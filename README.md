# About Bordeaux

Bordeaux is a media application server for IP telephony networks. Bordeaux
began with GNU Bayonne and ideas I had for creating real-time non-blocking
carrier scalable general purpose telephony servers using GNU/Linux. Many of the
architectural advances made in GNU Bayonne were ahead of common practices back
then, and some still remain so today. This is why I am introducing Bordeaux
using modern software practices as a micro-services capable Bayonne replacement
without support for legacy telephony networks.

Loadable driver modules are used to adopt Bordeaux to the needs of particular
VoIP phone systems. The generic sip driver can be used with most SIP based
phone services. This driver also integrates with a coventry call server. There
will be a driver made for H323 to use with GNU Gatekeeper, and a driver
specifically for IMS carrier uses. Classic telephony hardware, which GNU
Bayonne originally had extensive direct support for, will be used thru thru
network enabled VoIP gateways such as Bristol instead.

## Dependencies

Bordeaux is a C++17 application that requires CMake to build and requires
moderncli and libfmt to be installed. Bordeaux also requires OpenSSL,
libeXosip2 (5.3.0 or later), and supporting libraries for those. Bordeaux can
run on any platform with true Posix support, such as all Linux kernel based
systems, BSD's, and maybe even MacOS. Bordeaux can be compiled and ran using
GCC (9 or later), Clang (14? or later), or any other C++ runtime with thread
and filesystem support.

Bordeaux will build and run directly on most posix native systems like BSD (all
variants), Linux kernel based systems, and other common Unix systems. It
requires support for Unix domain datagram sockets to operate, and this alone
excludes it's use with Microsoft Windows, even if building with mingw.

Bordeaux also uses flite to synthesize text prompts for building application
phrasebooks. The entire default phrasebook can be rebuilt with this tool, so if
you want to change the voices to match something different, or for building
multi-lingual systems, you are able to.

Bordeaux may also optionally use libsystemd on GNU/Linux systems. This allows
building for optimized use on distros that use systemd. A systemd enabled
binary can also run on non-systemd distros, but it is recommended not to enable
systemd support for any GNU/Linux distro that does not default to systemd as
it increases the attack surface for potential vulnerabilities.

## Distributions

Distributions of this package are provided as detached source tarballs made
from a tagged release from our public gitlab repository or by building the dist
target. These stand-alone detached tarballs can be used to make packages for
many GNU/Linux systems, and for BSD ports. They may also be used to build and
install the software directly on a target platform.

We may offer ocx / docker images of Bordeaux in the future for supporting cloud
based telephony applications and pure IMS services. These would be built from
multi-stage docker files directly. Container images will also be hosted at our
public gitlab.

## Installation

**Installation should only be attempted with cmake release builds**. The
preferred cmake build type for live installations is `cmake -D
CMAKE_BUILD_TYPE=RelWithDebugInfo .`. This retains debug symbols in release
builds which many gnu/linux packaging systems then strip off and separately
generate a debug package for.

Many paths are set by cmake configuration for release builds, and these can be
overridden using `CMAKE -D... .` overrides. Some of these are listed with the
--version option, so you can always find what paths a given bordeaux binary has
been built for to use.

## Participation

This project is offered as free (as in freedom) software for public use and has
a public project page at https://www.gitlab.com/tychosoft/bordeaux which has an
issue tracker where people can submit public bug reports, a wiki for hosting
project documentation and architecture, and a public git repository. Patches
and merge requests may be submitted in the issue tracker or thru email. Support
requests and other kinds of inquiries may also be sent thru the tychosoft
gitlab help desktop service. Other details about participation may be found in
the Contributing page.

## Testing

When `cmake -DCMAKE_BUILD_TYPE=Debug .` is used to produce a Debug build it
can be executed directly for testing from the server build directory; no
further install is required. The debug build uses the config file found in the
test/ directory, and a test/custom.conf file can be added with configuration
overrides for local developer testing. Testing scripts, prompts, and libexec
telephony lambdas may also be placed in the test/ directory.

There is a `test` target which builds and executes a debug server.  The
server also includes a built-in regression test.  This can be executed with
just `server/bordeaux`, and verbose levels can be used.  Technically
this is the same as `server/bordeaux -g test`, and uses the special
test driver (`drivers/test/`) which has a suite of regression tests.

If you have coventry setup, it is easy to test and see Bordeaux operate using
debug level 4 on both servers. You might use a command like
`./server/bordeaux -d sip -vvvv` after building. The default test
script is named after the driver, so you can create a simple test/sip.scr
to get started like:

```
@ring
    sleep 8     # sleep 2 rings...
    answer
    sleep 8
    exit
```

And a test/custom.conf like:

```
[server]
media = 192.168.1.20

[sip]
route = localhost
extension = 89
password = XXX
dialplan = 10
port = 5054
```

