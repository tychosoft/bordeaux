/*
 * Copyright (C) 2022 Tycho Softworks.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "audiofile.hpp"
#include "paths.hpp"

#include <moderncli/args.hpp>
#include <moderncli/filesystem.hpp>
#include <moderncli/scan.hpp>

#include <csignal>
#include <memory>
#include <cmath>
#include <cstdint>

#include <sys/mman.h>
#include <fcntl.h>

#ifdef  FLITE
#include <flite/flite.h>
extern "C" cst_voice *register_cmu_us_slt();
#endif

namespace {
args::option opt_app('a',       "--app", "application script", "name");
args::option opt_country('c',   "--country", "country location", "country id");
args::option opt_lang('l',      "--lang", "language used", "language id");
args::option opt_mailbox('m',   "--mailbox", "mailbox selected", "number");
args::option opt_prefix('p',    "--prefix", "voice prefix", "path");
args::option opt_prompts(0,     "--prompts", "voice prompts", "path");
args::option opt_quiet('q',     "--quiet", "no output shown");
args::option opt_select('s',    "--select", "select tts voice", "name");
args::option opt_voice('v',     "--voice", "voice used", "name");
args::option helpflag('h',      "--help", "display this list");
args::option althelp('?',       nullptr, nullptr);

auto val_seg(int val) {
	int r = 0;
	val >>= 7;
	if (val & 0xf0) {
		val >>= 4;
		r += 4;
	}
	if (val & 0x0c) {
		val >>= 2;
		r += 2;
	}
	if (val & 0x02)
		r += 1;
	return r;
}

auto to_ulaw(int pcm_val) {
	int mask{};
	int seg{};
	unsigned char uval{};

	if (pcm_val < 0) {
		pcm_val = 0x84 - pcm_val;
		mask = 0x7f;
	} else {
		pcm_val += 0x84;
		mask = 0xff;
	}
	if (pcm_val > 0x7fff)
		pcm_val = 0x7fff;

	/* Convert the scaled magnitude to segment number. */
	seg = val_seg(pcm_val);

	/*
	 * Combine the sign, segment, quantization bits;
	 * and complement the code word.
	 */
	uval = (seg << 4) | ((pcm_val >> (seg + 3)) & 0x0f);
	return uval ^ mask;
}

void tone(uint8_t *samples, unsigned msec, short f1, short f2 = 0, unsigned rate = 8000, double volume = 10000.0) {
    const double freq1 = (f1 * M_PI * 2) / rate;
    const double freq2 = (f2 * M_PI * 2) / rate;
    double pos1{0}, pos2{0};

    if(f2 > 0)
        volume /= 2;

    msec *= (rate / 1000);
    while(msec--) {
        auto s1 = (int)(sin(pos1) * volume);
        auto s2 = (int)(sin(pos2) * volume);
        pos1 += freq1;
        pos2 += freq2;
        *(samples++) = to_ulaw(s1 + s2);
    }
}

auto path(const std::string& from, const audio_paths& paths) {
    char to[256];
    if(!audio_paths::playfile(from.c_str(), to, sizeof(to), &paths)) throw std::runtime_error(format("{}: invalid path", from));
    return std::string(to);
}

auto arg(int arg, const audio_paths& paths) {
    auto from = std::string(args::list()[arg]);
    if(from[0] == '/' || from[0] == '.' || ends_with(from, ".au")) {
        if(ends_with(from, ".au")) return from;
        return from + ".au";
    }
    return path(from, paths);
}
} // end namespace

auto main(int argc, const char **argv) -> int { // NOLINT
    try {
        auto count = args::parse(argc, argv);
        if(is(helpflag) || is(althelp) || !count) {
            args::help({"bordeauxsnd [options] command..."}, {
                "bordeauxsnd " PROJECT_VERSION,
                "David Sugar <tychosoft@gmail.com>",
                "Audio file operations"
            });
            return 0;
        }

#ifdef  FLITE
        flite_init();
        cst_voice* vox = nullptr;
#endif

        const auto& command = args::list()[0];
        audiofile audio;
        audio_paths paths;

        if(is(opt_lang))
            paths.lang = *opt_lang;

        if(is(opt_mailbox))
            paths.mailbox = *opt_mailbox;

        if(is(opt_app))
            paths.app = *opt_app;

        if(is(opt_prefix))
            paths.prefix = *opt_prefix;

        if(is(opt_prompts))
            paths.prompts = *opt_prompts;

        if(is(opt_voice))
            paths.voice = *opt_voice;

#ifdef  FLITE
        if(is(opt_select)) {
            if(!is(opt_voice))
                paths.voice = *opt_voice;
            vox = flite_voice_select(*opt_select);
            if(!vox) throw std::runtime_error(format("{}: tts selection not found", *opt_select));
        }

        if(!vox)
            vox = register_cmu_us_slt();
#else
        if(is(opt_select)) throw std::runtime_error("flite not supported");
#endif

        if(count == 2 && command == "path") {
            print("{}\n", path(std::string(args::list()[1]), paths));
            return 0;
        }

#ifdef  FLITE
        if(count == 1 && command == "make") {
            std::string specfile, app;
            auto lang = (*opt_lang ? *opt_lang : "en");
            auto vlib = (*opt_voice ? *opt_voice : "default");
            fsys::create_directory(std::string(paths.prefix) + "/" + lang);
            fsys::create_directory(std::string(paths.prefix) + "/" + lang + "/" + vlib);
            if(is(opt_app)) {
                app = *opt_app;
                fsys::create_directory(std::string(paths.prefix) + "/" + lang + "/" + vlib + "/" + app);
                specfile = std::string(paths.prefix) + "/" + lang + "/" + app + ".phr";
            }
            else
                specfile = std::string(paths.prefix) + "/" + lang + ".phr";

            auto country = (*opt_country ? *opt_country : "us");
            std::ifstream spec(specfile);
            if(!spec.is_open()) throw std::runtime_error(format("{}: no spec", specfile));
            scan_stream(spec, [&](std::string_view line) {
                auto pos = line.find_first_of("#;");
                if(pos != std::string::npos)
                    line.remove_suffix(line.size() - pos);
                line = strip(line);
                if(line.size() < 1) return true;
                pos = line.find_first_of(" \t");
                std::string file, text;
                if(pos < 1 || pos == std::string::npos) {
                    file = line;
                    text = line;
                }
                else {
                    file = strip(line.substr(0, pos));
                    text = strip(line.substr(++pos));
                }

                pos = file.find_first_of(':');
                if(pos != std::string::npos) {
                    if(line.substr(0, pos) != country) return true;
                    file = file.substr(++pos);
                }

                if(text.substr(0, 5) == "@tone") {
                    auto tokens = tokenize(text, " \t");
                    auto count = tokens.size();
                    if(count < 2 || count > 5) return true;
                    short f1{0}, f2{0};
                    auto length = unsigned(get_duration(tokens[1], true)) * 8U;
                    if(count > 2)
                        f1 = get_integer<short>(tokens[2]);
                    if(count > 3)
                        f2 = get_integer<short>(tokens[3]);
                    unsigned total{length};
                    if(count > 4)
                        total = length + unsigned(get_duration(tokens[4], true) * 8U);

                    auto samples = std::make_unique<uint8_t []>(total);
                    if(!is(opt_quiet))
                        print("{}: {} samples\n", file, total);

                    auto filename = path(file, paths);
                    auto note = "*" + file + "*";
                    memset(samples.get(), 0xff, sizeof(samples));
                    tone(samples.get(), length / 8, f1, f2);
                    fsys::remove(filename);
                    audio.make(filename.c_str(), 1, 8000, 1, 0664, note);
                    audio.put(samples.get(), total);
                    audio.close();
                    return true;
                }

                auto wave = flite_text_to_wave(text.c_str(), vox);
                if(!wave) throw std::runtime_error("flite conversion failed");
                if(wave->num_channels > 1) throw std::runtime_error("bordeaux requires mono voice");
                if(wave->sample_rate != 8000 && wave->sample_rate != 16000) throw std::runtime_error("bordeaux requires 8 or 16khz samples");
                auto samples = std::make_unique<uint8_t[]>(wave->num_samples);
                auto sp = wave->samples;
                auto up = &samples[0];

                if(wave->sample_rate == 16000)
                    wave->num_samples /= 2;

                if(!is(opt_quiet))
                    print("{}: {} samples\n", file, wave->num_samples);

                auto size = wave->num_samples;
                while(wave->num_samples--) {
                        *(up++) = to_ulaw(*(sp)++);
                        if(wave->sample_rate == 16000)
                            ++sp;
                }

                auto filename = path(file, paths);
                fsys::remove(filename);
                audio.make(filename.c_str(), 1, 8000, 1, 0664, text);
                audio.put(samples.get(), size);
                audio.close();
                delete_wave(wave);
                return true;
            });
            return 0;
        }

        if(count == 3 && command == "say") {
            auto text = std::string{args::list()[1]};
            auto filename = arg(2, paths);
            auto wave = flite_text_to_wave(text.c_str(), vox);

            if(!wave) throw std::runtime_error("flite conversion failed");
            if(wave->num_channels > 1) throw std::runtime_error("bordeaux requires mono voice");
            if(wave->sample_rate != 8000 && wave->sample_rate != 16000) throw std::runtime_error("bordeaux requires 8 or 16khz samples");
            auto samples = std::make_unique<uint8_t[]>(wave->num_samples);
            auto sp = wave->samples;
            auto up = &samples[0];

            if(wave->sample_rate == 16000)
                wave->num_samples /= 2;

            if(!is(opt_quiet))
                print("{} samples\n", wave->num_samples);

            auto size = wave->num_samples;
            while(wave->num_samples--) {
                    *(up++) = to_ulaw(*(sp)++);
                    if(wave->sample_rate == 16000)
                        ++sp;
            }

            fsys::remove(filename);
            audio.make(filename.c_str(), 1, 8000, 1, 0664, text);
            audio.put(samples.get(), size);
            audio.close();
            delete_wave(wave);
            return 0;
        }
#endif

        if(count > 2 && command == "tone") {
            auto filename = arg(1, paths);
            short f1{0}, f2{0};
            unsigned length{0};
            length = unsigned(get_duration(args::list()[2], true) * 8);
            if(count > 3)
                f1 = get_integer<short>(args::list()[3]);
            if(count > 4)
                f2 = get_integer<short>(args::list()[4]);
            unsigned total{length};
            if(count > 5)
                total = length + get_duration(args::list()[5], true) * 8U;
            auto samples = std::make_unique<uint8_t []>(total);
            memset(&samples[0], 0xff, total);
            tone(&samples[0], length / 8, f1, f2);
            if(!is(opt_quiet))
                print("{} samples\n", total);

            auto note = "*" + std::string{args::list()[1]} + "*";
            fsys::remove(filename);
            audio.make(filename.c_str(), 1, 8000, 1, 0664, note);
            audio.put(&samples[0], total);
            audio.close();
            return 0;
        }

        if(count == 1 && command == "sysinit") {
            uint8_t samples[8000];
            memset(samples, 0xff, sizeof(samples));
            auto fn_silence = path("sys/silence", paths);
            auto fn_pause = path("sys/pause", paths);
            auto fn_beep = path("sys/beep", paths);

            fsys::remove(fn_silence);
            audio.make(fn_silence.c_str(), 1, 8000, 1, 0664, "*silence*");
            audio.put(samples, 8000);
            audio.close();

            fsys::remove(fn_pause);
            audio.make(fn_pause.c_str(), 1, 8000, 1, 0664, "*pause*");
            audio.put(samples, 1600);
            audio.close();

            fsys::remove(fn_beep);
            tone(samples, 250, 852);
            audio.make(fn_beep.c_str(), 1, 8000, 1, 0664, "*beep*");
            audio.put(samples, 2000);
            audio.close();
            return 0;
        }

        if(count == 2 && command == "info") {
            auto filename = arg(1, paths);
            auto auformat = "unknown";
            auto bytesize = 0U;
            auto bitsize = 0U;
            if(!audio.play(filename.c_str())) throw std::runtime_error(format("{}: unknown or invalid file", filename));
            switch(audio.encoding()) {
            case 1:
                auformat = "g.711 u-law";
                bytesize = 1;
                break;
            case 2:
                auformat = "8-bit pcm";
                bytesize = 1;
                break;
            case 3:
                auformat = "16-bit pcm";
                bytesize = 2;
                break;
            case 4:
                auformat = "24-bit pcm";
                bytesize = 3;
                break;
            case 5:
                auformat = "32-bit pcm";
                bytesize = 4;
                break;
            case 6:
                auformat = "32-bit float";
                bytesize = 4;
                break;
            case 7:
                auformat = "64-bit float";
                bytesize = 8;
                break;
            case 8:
                auformat = "fragmented";
                bytesize = 0;
                break;
            case 9:
            case 21:
                auformat = "dsp image";
                bytesize = 0;
                break;
            case 10:
                auformat = "8-bit fixed";
                bytesize = 1;
                break;
            case 11:
                auformat = "16-bit fixed";
                bytesize = 2;
                break;
            case 12:
                auformat = "24-bit fixed";
                bytesize = 3;
                break;
            case 13:
                auformat = "32-bit fixed";
                bytesize = 4;
                break;
            case 18:
                auformat = "16-bit linear with emphasis";
                bytesize = 2;
                break;
            case 19:
                auformat = "16-bit linear compressed";
                bytesize = 2;
                break;
            case 20:
                auformat = "16-bit linear compressed with emphasis";
                bytesize = 2;
                break;
            case 23:
                auformat = "g.721 4-bit adpcm";
                bitsize = 4;
                break;
            case 24:
                auformat = "g.722 adpcm";
                bitsize = 4;
                break;
            case 25:
                auformat = "g.723 3-bit adpcm";
                bitsize = 3;
                break;
            case 26:
                auformat = "g.723 5-bit adpcm";
                bitsize = 5;
                break;
            case 27:
                auformat = "g.711 a-law";
                bytesize = 1;
                break;
            default:
                break;
            }
            if(bytesize)
                bitsize = bytesize * 8;
            bytesize *= audio.channels();
            print("path:        {}\n", filename);
            if(audio.size() == 0xffffffff)
                print("size:        unknown\n");
            else {
                print("size:        {}\n", audio.size());
                if(bitsize) {
                    auto samples = ((audio.size() * 8UL) / bitsize) / audio.channels();
                    samples *= 1000UL;
                    print("tine:        {} msec\n", samples / audio.sample_rate());
                }
            }
            print("encoding:    {}\n", auformat);
            print("channels:    {}\n", audio.channels());
            if(bitsize || bytesize)
                print("sample rate: {}\n", audio.sample_rate());
            if(bitsize)
                print("sample bits: {}\n", bitsize);
            if(!audio.annotation().empty())
                print("annotation:  {}\n", audio.annotation());
            return 0;
        }
        throw std::runtime_error(format("{}: unknown command or arguments"));
    }
    catch(const bad_arg& e) {
        die(-1, "Usage: bordeauxsnd [options] command...\nerror: {}\n", e.what());
    }
    catch(const std::exception& e) {
        die(-1, "error: {}\n", e.what());
    }
    catch(...) {
        die(-1, "error: unknown error\n");
    }
}
