/*
 * Copyright (C) 2021 Tycho Softworks.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "server.hpp"       // IWYU pragma: keep

#include "ipc.hpp"

#include <moderncli/process.hpp>
#include <moderncli/socket.hpp>
#include <moderncli/array.hpp>
#include <moderncli/args.hpp>
#include <moderncli/scan.hpp>

#include <sys/stat.h>
#include <fcntl.h>

namespace {
args::option helpflag('h',  "--help",   "display this list");
args::option quiet('q',     "--quiet",  "suppress most output");
args::option althelp('?',   nullptr, nullptr);

class ipc_error final : public std::exception {
public:
    explicit ipc_error(const char *text, int code) noexcept : 
    msg_(text), err_(code) {}

    auto what() const noexcept -> const char * override {
        return msg_;
    }

    auto exit() const noexcept {
        return err_;
    }

private:
    const char *msg_;
    int err_;
};

enum {STATUS, LEVEL, SESSIONS, RELOAD, INVALID};
auto command = INVALID;
auto exit_code = 0;
} // end namespace

auto main(int argc, const char **argv) -> int {
    using namespace process;
    try {
        auto count = args::parse(argc, argv);
        if(count == 1 && args::list()[0] == "status")
            command = STATUS;

        if(count == 1 && args::list()[0] == "reload")
            command = RELOAD;

        if(count == 1 && args::list()[0] == "list")
            command = SESSIONS;

        if(count == 2 && args::list()[0] == "level")
            command = LEVEL;

        if(is(helpflag) || is(althelp) || !count || command == INVALID) {
            args::help({"bordeauxctl [options] command..."}, {
                "bordeauxctl " PROJECT_VERSION,
                "David Sugar <tychosoft@gmail.com>",
                "Send command to server ipc"
            });
            return 0;
        }

        ipc_event event{.type = IPC_STATUS, .ver = IPC_VERSION};
        if(command == LEVEL) {
            event.type = IPC_LEVEL;
            event.body.level = get_unsigned(args::list()[1], 0U, 9U);
        }
        else if(command == RELOAD) {
            event.type = IPC_RELOAD;
        }

        auto ipc(Socket(AF_UNIX, SOCK_DGRAM));
        if(!ipc) throw ipc_error("control failed", 4);
        if(!send(ipc, event, address_t(SERVER_CONTROL_IPC))) throw ipc_error("send failed", 4);
        auto shm = shm_open(BORDEAUX_IPCPATH, O_RDONLY, 0660);
        if(shm < 0) throw ipc_error("mapping failed", 4);
        struct stat ino{};
        if(fstat(shm, &ino) < 0) throw ipc_error("stat failed", 4);
        auto max = (ino.st_size - sizeof(ipc_system)) / sizeof(ipc_session);
        auto mem = map_t(shm, ino.st_size, false);
        if(!is(mem)) throw ipc_error("mapping failed", 4);
        auto sys = static_cast<ipc_system*>(*mem);
        auto map = span<ipc_session>(void_ptr(*mem, sizeof(ipc_system)), max);
        auto used = sys->used.load();
        if(used > sys->limit)
            used = sys->limit;

        if(!sys->active && command == STATUS) throw ipc_error("offline", 10);
        time_t now{};
        time(&now);

        if(command == SESSIONS) {
            for(const auto& entry : map) {
                if(!entry.created) continue;
                char state = '?';
                char type = '?';
                switch(entry.state) {
                case ipc_session::ISC_RINGING:
                    state = 'R';
                    break;
                case ipc_session::ISC_CLOSING:
                    state = 'X';
                    break;
                case ipc_session::ISC_INVITING:
                    state = 'I';
                    break;
                case ipc_session::ISC_CONNECTED:
                    state = 'C';
                    break;
                default:
                    break;
                }

                switch(entry.type) {
                case ipc_session::ISC_INBOUND:
                    type = 'I';
                    break;
                case ipc_session::ISC_OUTBOUND:
                    type = 'O';
                    break;
                default:
                    break;
                }

                print("{} {}{} {} {} {}s\n", entry.sid, type, state, entry.caller, entry.dialed, now - entry.created);
            }
        }

        now -= sys->started;
        if(!is(quiet))
            print("used={}/{} uptime={}m log={}\n", used, sys->limit, now / 60, sys->level);

    }
    catch(const bad_arg& e) {
        die(-1, "Usage: bordeauxctl [options] command...\nerror: {}\n", e.what());
    }
    catch(const ipc_error& e) {
        if(is(quiet)) return e.exit();
        die(exit_code, "error: {}\n", e.what());
    }
    catch(const std::exception& e) {
        if(is(quiet)) return -1;
        die(-1, "error: {}\n", e.what());
    }
    catch(...) {
        if(is(quiet)) return -1;
        die(-1, "error: unknown error\n");
    }
    return exit_code;
}

