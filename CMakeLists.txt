# Copyright (C) 2020 Tycho Softworks.
#
# This file is free software; as a special exception the author gives
# unlimited permission to copy and/or distribute it, with or without
# modifications, as long as this notice is preserved.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

cmake_minimum_required(VERSION 3.16.0)
project(Bordeaux VERSION 0.9.5 LANGUAGES CXX)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_STANDARD 17)
cmake_policy(SET CMP0063 NEW)

file(
    GLOB LINT_SOURCES
    RELATIVE ${PROJECT_SOURCE_DIR}
    src/*.[ch]pp drivers/*.[ch]pp modules/*.[ch]pp utils/*.cpp)

include(cmake/custom.cmake OPTIONAL)
include(cmake/project.cmake)
include(cmake/tune.cmake)
include(cmake/features.cmake)
include(cmake/coverage.cmake)
include(cmake/linting.cmake)
include(cmake/paths.cmake)
include(cmake/deploy.cmake OPTIONAL)

file(GLOB testing test/*.conf test/*.scr)
file(GLOB optional .git[a-z]* *.json *.in *.yml .clang* cmake/*)
file(GLOB markdown *.md)

# Setup headers and config
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/src)

add_compile_definitions(PROJECT_VERSION="${PROJECT_VERSION}")
add_compile_definitions(SERVER_ID="${SERVER_ID}")
add_compile_definitions(SERVER_CONTROL_IPC="${SERVER_CONTROL_IPC}")
add_compile_definitions(SERVER_PREFIX_PATH="${SERVER_PREFIX_PATH}")
add_compile_definitions(SERVER_LOGGER_PATH="${SERVER_LOGGER_PATH}")
add_compile_definitions(SERVER_CONFIG_FILE="${SERVER_CONFIG_FILE}")

add_compile_definitions(BORDEAUX_VOICES_PATH="${BORDEAUX_VOICES_PATH}")
add_compile_definitions(BORDEAUX_PROMPTS_PATH="${BORDEAUX_PROMPTS_PATH}")
add_compile_definitions(BORDEAUX_SCRIPTS_PATH="${BORDEAUX_SCRIPTS_PATH}")
add_compile_definitions(BORDEAUX_MODULES_PATH="${BORDEAUX_MODULES_PATH}")
add_compile_definitions(BORDEAUX_DRIVERS_PATH="${BORDEAUX_DRIVERS_PATH}")
add_compile_definitions(BORDEAUX_PLUGINS_PATH="${BORDEAUX_PLUGINS_PATH}")
add_compile_definitions(BORDEAUX_DEFAULT_DRIVER="${BORDEAUX_DEFAULT_DRIVER}")
add_compile_definitions(BORDEAUX_IPCPATH="${BORDEAUX_IPCPATH}")
add_compile_definitions(BORDEAUX_VOICE_MODULES="${BORDEAUX_VOICE_MODULES}")

add_compile_definitions(TUNE_SYMBOLS_PAGER=${TUNE_SYMBOLS_PAGER})
add_compile_definitions(TUNE_COMPILE_PAGER=${TUNE_COMPILE_PAGER})
add_compile_definitions(TUNE_SYMBOLS_INDEX=${TUNE_SYMBOLS_INDEX})
add_compile_definitions(TUNE_SYMBOL_SIZE=${TUNE_SYMBOL_SIZE})
add_compile_definitions(TUNE_MAX_DIGITS=${TUNE_MAX_DIGITS})
add_compile_definitions(TUNE_MAX_LEVELS=${TUNE_MAX_LEVELS})
add_compile_definitions(TUNE_STEP_RATE=${TUNE_STEP_RATE})
add_compile_definitions(TUNE_STEP_COUNT=${TUNE_STEP_COUNT})

# Server build and install
add_subdirectory(src)
add_subdirectory(drivers)
add_subdirectory(plugins)
add_subdirectory(voices)
add_subdirectory(utils)

# Extras and IDE targets
add_custom_target(support-files SOURCES ${markdown} ${optional})
add_custom_target(testing-files SOURCES ${testing})
