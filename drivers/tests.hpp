/*
 * Copyright (C) 2020 Tycho Softworks.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "drivers.hpp"
#include "sessions.hpp"

namespace server {
class test_driver final : public server::Driver {
public:
    static test_driver instance;

    test_driver();

private:
    void startup(const keyfile& config) final;
    void restart(const keyfile& config) final;
    void shutdown() final;
    void drop(Session *session) final;

    // helper functions
    static void unit_tests(const keyfile& ini);
};

class test_session final : public server::Session
{
public:
    explicit test_session(script& attach);
};
} // end namespace

