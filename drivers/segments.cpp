/*
 * Copyright (C) 2022 Tycho Softworks.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "segments.hpp"
#include "ipc.hpp"

using namespace server;

Segment::~Segment() {
    Segment::disconnect();
}

auto Segment::sdp() -> std::string {
    if(!audio || !audio->local_port) return {};
    auto uid = syms.get_value("CALLED_TO");
    auto ip = ipv6 ? "IP6" : "IP4";
    if(!audio->dtmf) {
        return format(
            "v=0\r\n"
            "o={} 0 0 IN {} {}\r\n"
            "s={}\r\n"
            "c=IN {} {}\r\n"
            "t=0 0\r\n"
            "m=audio {} RTP/AVP {}\r\n"
            "a=rtpmap:{} pcmu/8000\r\n",
            uid, ip, audio->media,
            sid(),
            ip, audio->media,
            audio->local_port, audio->payload,
            audio->payload
        );
    }
    return format(
        "v=0\r\n"
        "o={} 0 0 IN {} {}\r\n"
        "s={}\r\n"
        "c=IN {} {}\r\n"
        "t=0 0\r\n"
        "m=audio {} RTP/AVP {} {}\r\n"
        "a=rtpmap:{} pcmu/8000\r\n"
        "a=rtpmap:{} telephone-event/8000\r\n",
        uid, ip, audio->media,
        sid(),
        ip, audio->media,
        audio->local_port, audio->payload, audio->dtmf,
        audio->payload,
        audio->dtmf
    );
}

auto Segment::answer(int status, std::function<void(exosip::msg_t)> fill) -> bool {
    if(tid < 1) return false;
    auto msg = exosip::msg_t(nullptr);
    const exosip::guard_t lock(context);
    eXosip_call_build_answer(context, tid, status, &msg);
    if(!msg) return false;
    fill(msg);
    eXosip_call_send_answer(context, tid, status, msg);
    if(status >= 200)
        tid = -1;

    return true;
}

void Segment::terminate() {
    if(tid > 0)
        answer(SIP_REQUEST_TERMINATED);
    else if(cid > 0 && did > 0) {
        const exosip::guard_t lock(context);
        eXosip_call_terminate(context, cid, did);
    }
    cid = did = tid = -1;
    connected = false;
}

void Segment::stream() {
    if(!audio || connected) return;
    Session::stream();
    audio->start();
}

void Segment::silence(bool shutdown) {
    if(!audio || (!shutdown && !media)) return;
    set_timed(audio->silent(shutdown));
    Session::silence(shutdown);
}

auto Segment::play_tone() -> bool {
    if(!audio) {
        error("not connected");
        return true;
    }

    media = true;
    audio->play_tone(playrec.handle(), state.tone.count);
    return false;
}

auto Segment::play_files() -> bool {
    if(!audio) {
        error("not connected");
        return true;
    }

    media = true;
    audio->play_file(playrec.handle());
    return false;
}

auto Segment::record_file() -> bool {
    if(!audio) {
        error("not connected");
        return true;
    }

    media = true;
    playrec.pos(state.prompt.pos);
    audio->record_file(playrec.handle());
    return false;
}

void Segment::apply_sdp(const std::string_view& sdp) const {
    if(audio)
        audio->apply_sdp(sdp);
}

void Segment::connect(std::string local_media, std::string_view& sdp, uint16_t port) {
    Segment::disconnect();
    audio = new rtp_audio(this, local_media, sdp, port);    // NOLINT
    if(shm)
        shm->audio = audio->source();
}

void Segment::disconnect() {
    if(audio) {
        audio->finish();
        delete audio;
        audio = nullptr;
    }
    if(shm)
        shm->audio = 0;
    Session::disconnect();
}

