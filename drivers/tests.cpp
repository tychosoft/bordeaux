/*
 * Copyright (C) 2020 Tycho Softworks.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "audiofile.hpp"
#include "tests.hpp"

#include <moderncli/strings.hpp>
#include <moderncli/keyfile.hpp>
#include <moderncli/filesystem.hpp>
#include <moderncli/process.hpp>

#include <thread>
#include <csignal>

#include <unistd.h>

using namespace server;
using namespace std::literals::string_literals;

namespace {
keyfile::keys test_keys;
}

test_driver test_driver::instance;

test_driver::test_driver() : Driver("test") {}

void test_driver::startup([[maybe_unused]] const keyfile& config) {
    kill(getpid(), SIGHUP);
}

void test_driver::restart(const keyfile& config) {
    unit_tests(config);
    process::exit(0);
}

void test_driver::shutdown() {}

void test_driver::drop(Session *session) {
    logger.debug(4, "sid({}): detected session drop", session->sid());
}

void test_driver::unit_tests(const keyfile& ini) {
    test_keys = ini.at("test");
    {   // config tests

        assert(!test_keys.empty());
        assert(test_keys["test1"] == "hello");
        print("Success: config test keys\n");
    }

    {   // audio file tests
        audiofile audio, audio2;
        fsys::remove("test.au");
        audio.make("test.au", 1, 8000, 1, 0664, "this is a note");
        uint32_t samples[32]{0};
        assert(audio.offset() == 40);
        audio.put(samples, 32);
        assert(audio.pos() == 32U);
        audio.close();

        audio2.play("test.au");
        assert(audio2.offset() == 40 && audio2.pos() == 0);
        audio2.pos(16U);
        assert(audio2.pos() == 16U);
        assert(audio2.get(samples, 32) == 16U);
        audio2.close();
        fsys::remove("test.au");
        print("Success: audio test\n");
    }

    {   // string tests

        const std::string text = "hi,bye,gone";
        auto list = split(text, ",");

        const std::string parse = "this is a 'quoted list'  of   words";
        auto tokens = tokenize(parse);

        assert(list.size() == 3);
        assert(list[0] == "hi");
        assert(list[1] == "bye");
        assert(list[2] == "gone");

        assert(tokens.size() == 6);
        assert(tokens[3] == "'quoted list'");
        assert(tokens[4] == "of");

        assert(symbols::is_number("23.7"));
        assert(!symbols::is_number("23b6"));

        assert(script::str_time24("8:30p") == "20:30");
        assert(script::str_months("11,mar,july") == "3,7,11");
        assert(script::str_days("weekend,tue") == "1,3,7");
        print("Success: string tests\n");
    }

    {   // compiler tests
        auto scr = script("testing.scr", ini);
        const session_t session = std::make_shared<test_session>(scr);
        Driver::attach(session);

        auto tid = session->get_symbol("TEST_ID");
        assert(Driver::sys_mapped->used == 1);
        assert(!scr.empty());
        assert(scr.name() == "testing");
        assert(session.use_count() == 2);
        assert(tid && eq(tid, "1"));

        {   // shared ptr behaviors...
            auto s1 = Driver::get(session->sid());
            assert(s1.use_count() == 3);
            auto s2(Driver::get(session->sid()));
            assert(s2.use_count() == 4);
        }
        assert(session.use_count() == 2);

        // Start and run long enough to hit engine exit...
        Session::begin(session);
        assert(eq(session->get_status(), "step"));
        std::this_thread::sleep_for(std::chrono::milliseconds(160));

        auto myconst = session->get_symbol("MYCONST");
        auto mymodule = session->get_symbol("MYMODULE");
        auto xyz = session->get_symbol("XYZ");

        assert(eq(myconst, "hello"));
        assert(eq(xyz, "test"));
        assert(eq(mymodule, "test module"));

        // We already hit script exit, so it should have released...
        if(session.use_count() > 1) {
            // release triggers timer thread, which makes temporary session copy,
            // so we wait for that to settle...
            logger.debug(4, "count above 1 on exit");
            Session::release(session);
            std::this_thread::sleep_for(std::chrono::milliseconds(50));

            assert(session.use_count() == 2);
            Driver::release(session->sid());
        }
        else
            logger.debug(4, "count at 1 on exit");

        assert(Driver::count() == 0);
        assert(sys_mapped->used == 0);
        assert(session.use_count() == 1);
        print("Success: engine tests\n");
    }
}

test_session::test_session(script& attach) :
Session(attach) {
	syms.make_const({{"TEST_ID", "1"}});
}

