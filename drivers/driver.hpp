/*
 * Copyright (C) 2020 Tycho Softworks.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PLUGIN_DRIVER_HPP_
#define PLUGIN_DRIVER_HPP_
#include "drivers.hpp"

namespace server {
class driver final : public server::Driver {
public:
    // public methods use driver internals & context lock

    static driver instance;
    static void notify(const char *to, const char *text, const char *subject);

private:
    driver();

    void configure(keyfile& config, unsigned verbose) final;
    void startup(const keyfile& config) final;
    void restart(const keyfile& config) final;
    void shutdown() final;
    void disconnect() final;
    void drop(Session *session) final;
    void answer(Session *session) final;

    // helper functions
    void dispatch(int priority);
    static void release_id();
};

extern driver& sip;

} // end namespace
#endif
