/*
 * Copyright (C) 2022 Tycho Softworks.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MEDIA_HPP_
#define MEDIA_HPP_

#include "driver.hpp"       // IWYU pragma: keep
#include "audiofile.hpp"

#include <moderncli/sync.hpp>
#include <moderncli/endian.hpp>
#include <moderncli/socket.hpp>

#include <atomic>

namespace server {
static const std::size_t rtp_min_header_size = 12;
static const std::size_t rtp_max_header_size = 12 + (15 * 4); // 15 csrc...
static const std::size_t max_audio_size = 640;

class Session;

class rtp_packet final {
public:
    explicit rtp_packet(uint8_t *p) noexcept :
    packet_(p), samples_(0) {
        memset(packet_, 0, rtp_min_header_size);
    }

    rtp_packet() = delete;
    rtp_packet(const rtp_packet&) = delete;

    auto version() const noexcept {  // default is 80/2
        return (packet_[0] & 0xc0) >> 6;
    }

    auto padding() const noexcept {
        return (packet_[0] & 0x20) != 0;
    }

    auto contrib() const noexcept {
        return (packet_[0] & 0x0f);
    }

    auto contrib4() const noexcept {
        return (packet_[0] & 0x0f) << 2;
    }

    auto marker() const noexcept {
        return (packet_[1] & 0x80) != 0;
    }

    auto payload() const noexcept {
        return (packet_[1] & 0x7f);
    }

    auto sequence() const noexcept {
        return be_get16(&packet_[2]);
    }

    auto timestamp() const noexcept {
        return be_get32(&packet_[4]);
    }

    auto source() const noexcept {
        return be_get32(&packet_[8]);
    }

    auto extends() const noexcept -> uint16_t {
        if(!(packet_[0] & 0x10)) return 0;
        return be_get16(&packet_[rtp_min_header_size + contrib4() + 2]) + 4;
    }

    auto profile_id() const noexcept -> uint16_t {
        if(!extends()) return 0;
        return be_get16(&packet_[rtp_min_header_size + contrib4()]);
    }

    auto profile_size() const noexcept -> uint16_t {
        if(!extends()) return 0;
        return be_get16(&packet_[rtp_min_header_size + contrib4() + 2]);
    }

    auto header_size() const noexcept {
        return  rtp_min_header_size + contrib4() + extends();
    }

    auto framing() const noexcept {
        return framing_;
    }

    auto samples() const noexcept {
        return samples_;
    }

    auto profile_data() const noexcept {
        return (extends() ? &packet_[rtp_min_header_size + contrib4() + 4] : nullptr);
    }

    auto data() const noexcept {
        return packet_ + rtp_min_header_size + contrib4() + extends();
    }

    // type for packet, samples per frame, and millisecond frame time. I
    // initially am doing 20ms/8khz ulaw, but other possibilities may exist in
    // the future.
    void set_payload(uint8_t value, uint32_t spf = 160, long ft = 20) noexcept {
        value |= (packet_[1] & 0x80);
        packet_[1] = value;
        samples_ = spf;
        framing_ = ft;
    }

    void set_version(uint8_t value = 2) noexcept {
        packet_[0] = (value << 6) | (packet_[0] & 0x3f);
    }

    void set_source(uint32_t value) noexcept {
        be_set32(&packet_[8], value);
    }

    // next frame...
    void inc_sequence(unsigned count = 1) noexcept {
        be_set16(&packet_[2], sequence() + count);
        be_set32(&packet_[4], timestamp() + samples_ * count);
    }

    // skipped silent frame...
    void inc_silence(unsigned count = 1) noexcept {
        be_set32(&packet_[4], timestamp() + samples_ * count);
    }

    auto adjust_offset(uint32_t offset) noexcept {
        be_set32(&packet_[4], timestamp() - offset);
        return timestamp();
    }

    auto send(int socket, const struct sockaddr *addr, std::size_t max) const noexcept {
        auto size = ::sendto(socket, packet_, max + header_size(), MSG_DONTWAIT, addr, inet_size(addr));
        if(size < 0) return size;
        if(size <= static_cast<ssize_t>(header_size())) return ssize_t(0);
        return ssize_t(size - header_size());
    }

    auto recv(int socket, struct sockaddr *addr, std::size_t max) noexcept {
        socklen_t len{sizeof(struct sockaddr_storage)};
        auto size = ::recvfrom(socket, packet_, max + header_size(), 0, addr, &len);
        if(size < 0) return size;
        if(size <= static_cast<ssize_t>(header_size())) return ssize_t(0);
        return ssize_t(size - header_size());
    }

private:
    uint8_t *packet_;
    uint32_t samples_;
    long framing_{20};
};

class rtp_audio final {
public:
    uint16_t local_port{0}, remote_port{0};
    uint8_t payload{0}, dtmf{100};
    std::string media, remote_media;

    rtp_audio() = delete;
    rtp_audio(const rtp_audio&) = delete;
    ~rtp_audio();

    rtp_audio(Session *s, std::string local_media, const std::string_view& sdp, uint16_t port = 0);

    operator bool() const noexcept {
        return active_;
    }

    auto operator!() const noexcept {
        return !active_;
    }

    void sync_with_origin(bool sync_flag) noexcept {
        sync_address = sync_flag;
    }

    auto address() {
        return *remote_to;
    }

    auto from() {
        if(!sync_address) return *remote_from;
        return address();
    }

    auto source() {
        return output_packet.source();
    }

    void apply_sdp(const std::string_view& sdp);
    void start();
    void finish();
    void ignore();
    void keepalive();
    auto silent(bool shutdown = false) -> long;
    void play_file(audiofile::handle_t handle);
    void play_tone(audiofile::handle_t handle, unsigned count);
    void record_file(audiofile::handle_t handle);

private:
    std::atomic<unsigned> sequence_{0U};
    std::thread thread_;        // exec thread
    std::mutex mutex_;          // lock guard
    Session *session_;          // session to post events to
    int socket_{-1};
    audiofile::handle_t audio_{audiofile::invalid};
    audiofile::offset_t first_{0};   // i/o offset
    unsigned current_{-1U};
    unsigned repeat_{};
    address_t remote_to, remote_from;
    bool sync_address{false};
    uint64_t sent_total{0}, recv_total{0}, fail_total{0}, skip_total{0};
    uint32_t samples_{160};     // samples per frame...
    long counted_{0};           // counted samples, milliseconds
    unsigned bytes_{1};
    uint8_t filler_{0xff};
    long framing_{20};
    bool active_{false};
    uint32_t offset_{0}, expect_{0};
    sync_timepoint timer_;

    auto (rtp_audio::*get_)(rtp_packet& rtp, uint32_t ts) noexcept -> std::size_t;
    auto (rtp_audio::*put_)(rtp_packet& rtp) noexcept -> std::size_t;

    // audio packet buffers...
    uint8_t input_buffer[rtp_max_header_size + max_audio_size];
    uint8_t output_buffer[rtp_max_header_size + max_audio_size];
    rtp_packet input_packet, output_packet;

    // i/o processing...
    void exec_thread();
    auto sync_timer() noexcept -> long;
    auto output() noexcept -> std::size_t;
    auto input() noexcept -> std::size_t;

    // file/packet ops...
    auto put_tonegen(rtp_packet& rtp) noexcept -> std::size_t;
    auto put_player(rtp_packet& rtp) noexcept -> std::size_t;
    auto put_silent(rtp_packet& rtp) noexcept -> std::size_t;
    auto put_ignore(rtp_packet& rtp) noexcept -> std::size_t;
    auto get_ignore(rtp_packet& rtp, uint32_t ts) noexcept -> std::size_t;
    auto get_record(rtp_packet& rtp, uint32_t ts) noexcept -> std::size_t;

    static auto bind(const char *media, uint16_t mport = 0) -> std::pair<int, uint16_t>;
};
} // end namespace server
#endif
