/*
 * Copyright (C) 2022 Tycho Softworks.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "media.hpp"
#include "sessions.hpp"
#include "utils.hpp"

#include <moderncli/strings.hpp>
#include <moderncli/process.hpp>
#include <moderncli/scan.hpp>

#include <poll.h>

using namespace server;

rtp_audio::rtp_audio(Session *s, std::string local_media, const std::string_view& sdp, uint16_t port) :   // NOLINT
media(std::move(local_media)), session_(s), input_buffer{}, output_buffer{}, input_packet(input_buffer), output_packet(output_buffer) {
    ignore();
    std::tie(socket_, local_port) = bind("*", port);
    if(!local_port)
        logger.error("failed to bind media to {}", media);
    else
        logger.debug(4, "rtp media mapped to {}:{}", media, local_port);

    apply_sdp(sdp);
    logger.debug(4, "media connected to {}:{}", remote_media, remote_port);
    output_packet.set_payload(payload, samples_, framing_);
    output_packet.set_version();

    // TODO: pick per-session random source id...
    output_packet.set_source(port ^ getpid());
    if(source() == 0L)   // make sure never 0
        output_packet.set_source(0xffffffff);
}

rtp_audio::~rtp_audio() {
    try {
        finish();
    } catch(...) {};
}

auto rtp_audio::bind(const char *media, uint16_t mport) -> std::pair<int, uint16_t> {
    auto socket = ::socket(ipv6 ? AF_INET6 : AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if(socket < 0) return {-1, 0};
    uint16_t port = 0;
    auto reuse = 1;
    ::setsockopt(socket, SOL_SOCKET, SO_REUSEADDR, reinterpret_cast<const char*>(&reuse), sizeof(reuse));
#ifdef SO_REUSEPORT
    ::setsockopt(socket, SOL_SOCKET, SO_REUSEPORT, reinterpret_cast<const char*>(&reuse), sizeof(reuse));
#endif

    struct sockaddr_storage origin{};
    if(ipv6) {
        auto addr = reinterpret_cast<struct sockaddr_in6*>(&origin);
        socklen_t alen = sizeof(struct sockaddr_in6);
        addr->sin6_family = AF_INET6;
        inet_pton(AF_INET6, media, &(addr->sin6_addr));
        addr->sin6_port = htons(mport);
        ::bind(socket, reinterpret_cast<const sockaddr *>(addr), alen);
        getsockname(socket, reinterpret_cast<sockaddr *>(addr), &alen);
        port = ntohs(addr->sin6_port);
    }
    else {
        auto addr = reinterpret_cast<struct sockaddr_in*>(&origin);
        socklen_t alen = sizeof(struct sockaddr_in);
        addr->sin_family = AF_INET;
        inet_pton(AF_INET, media, &(addr->sin_addr));
        addr->sin_port = htons(mport);
        ::bind(socket, reinterpret_cast<const sockaddr *>(addr), alen);
        getsockname(socket, reinterpret_cast<sockaddr *>(addr), &alen);
        port = ntohs(addr->sin_port);
    }

    if(!port) {
        ::close(socket);
        return {-1, 0};
    }
    return {socket, port};
}

void rtp_audio::apply_sdp(const std::string_view& sdp) {
    if(sdp.empty()) return;
    dtmf = payload = 0;     // clear before changing
    auto lines = tokenize(sdp, "\r\n");
    for(auto& line : lines) {
        if(line.size() < 16) continue;
        if(begins_case(line, "c=IN IP4 ") && !ipv6) {
            line.remove_prefix(9);
            remote_media = std::string{line};
            continue;
        }

        if(begins_case(line, "c=IN IP6 ") && ipv6) {
            line.remove_prefix(9);
            remote_media = std::string{line};
            continue;
        }

        if(begins_case(line, "m=audio ")) {
            auto items = tokenize(line, " ");
            if(items.size() < 4) continue;
            remote_port = get_unsigned_or<uint16_t>(items[1], 1024, 1);
            continue;
        }

        if(!begins_case(line, "a=rtpmap:")) continue;
        line.remove_prefix(9);
        auto items = tokenize(line, " ");
        if(items.size() != 2) continue;
        if(begins_case(items[1], "pcmu/8000"))
            payload = get_unsigned_or<uint8_t>(items[0], 1);
        else if(begins_case(items[1], "telephone-event/8000"))
            dtmf = get_unsigned_or<uint8_t>(items[0], 101);
    }
    remote_to.set(remote_media, remote_port);
}

void rtp_audio::start() {
    if(active_) return;
    silent();
    timer_ = sync_clock(framing_);
    active_ = true;
    thread_ = std::thread(&rtp_audio::exec_thread, this);
}

void rtp_audio::finish() {
    active_ = false;
    if(thread_.joinable())
        thread_.join();
    if(socket_ > -1) {
        ::close(socket_);
        socket_ = -1;
        logger.debug(4, "rtp media released from {}:{}", media, local_port);
    }
}

auto rtp_audio::sync_timer() noexcept -> long {
    for(;;) {
        auto now = sync_clock();
        if(now < timer_) return std::chrono::duration_cast<std::chrono::milliseconds>(timer_ - now).count() + 1;
        timer_ += std::chrono::milliseconds(framing_);
        if(now < timer_) return 0;
        ++skip_total;
        output_packet.inc_silence();
    }
}

void rtp_audio::exec_thread() {
    struct pollfd poller{};
    poller.fd = socket_;
    poller.events = POLLIN;

    logger.debug(3, "sid({}): starting media thread", session_->sid());
    this_thread::priority(2);
    while(running && active_) {
        auto timeout = sync_timer();
        if(!timeout) {
            output();
            continue;
        }
        poller.revents = 0;
        if(poll(&poller, 1, static_cast<int>(timeout)) == 1 && (poller.revents & POLLIN))
            input();
    }
    logger.debug(2, "sid({}): sent={}, recv={}, skips={}, failed={}", session_->sid(), sent_total, recv_total, skip_total, fail_total);
}

void rtp_audio::keepalive() {
    const std::lock_guard<std::mutex> lock(mutex_);
    if(address()->sa_family != AF_UNSPEC)
        ::sendto(socket_, nullptr, 0, 0, address(), inet_size(address()));
}

auto rtp_audio::output() noexcept -> std::size_t {
    std::unique_lock<std::mutex> lock(mutex_);
    auto fill_ = put_;
    lock.unlock();
    auto size = (this->*(fill_))(output_packet);
    lock.lock();
    if(address()->sa_family == AF_UNSPEC) return 0;
    if(size > 0) {
        if(output_packet.send(socket_, address(), size) < 0) {
            ++fail_total;
            if(errno == EAGAIN) {     // blocked we skip as if silent...
                output_packet.inc_silence();
                ++skip_total;
                return size;
            }
            return 0;
        }
        ++sent_total;
        output_packet.inc_sequence();
        return size;
    }
    ++skip_total;
    output_packet.inc_silence();
    return 0;
}

auto rtp_audio::input() noexcept -> std::size_t {
    auto size = input_packet.recv(socket_, from(), max_audio_size);
    if(size < 0) return 0;
    // if invalid packet, then we ignore...
    if(!sync_address) {
        auto alen = inet_size(from());
        size = memcmp(from(), address(), alen) == 0 ? size : 0;
    }
    if(size) {
        if(input_packet.version() != 2) return 0;
        if(input_packet.payload() != payload && input_packet.payload() != dtmf) return 0;
    }

    // get offset if not set...
    std::unique_lock<std::mutex> lock(mutex_);
    if(!offset_)
        offset_ = input_packet.timestamp();

    // loose first packet if starts at timestamp == 0
    if(!offset_) return 0;
    auto timestamp = input_packet.adjust_offset(offset_);
    ++recv_total;

    // special throw-away case, time reversal prior to offset...
    if(timestamp >= 0xf0000000) return 0;
    // see if we have audio...
    if(input_packet.payload() == payload) {
        if(static_cast<std::size_t>(size) != static_cast<std::size_t>(samples_) * bytes_) return 0;
        lock.unlock();
        auto result = (this->*(get_))(input_packet, timestamp);
        if(timestamp >= expect_)
            expect_ = timestamp + samples_;
        return result;
    }
    // TODO: dtmf support...
    lock.unlock();
    return 0;
}

void rtp_audio::ignore() {
    ++sequence_;
    const std::lock_guard<std::mutex> lock(mutex_);
    counted_ = 0;
    expect_ = 1;
    offset_ = 0;
    get_ = &rtp_audio::get_ignore;
    put_ = &rtp_audio::put_ignore;
}

void rtp_audio::play_tone(audiofile::handle_t handle, unsigned count) {
    ++sequence_;
    const std::lock_guard<std::mutex> lock(mutex_);
    get_ = &rtp_audio::get_ignore;
    put_ = &rtp_audio::put_tonegen;
    audio_ = handle;
    repeat_ = count;
    first_ = ::lseek(handle, 0L, SEEK_CUR);
    offset_ = expect_ = counted_ = 0;
    current_ = sequence_;
}

void rtp_audio::play_file(audiofile::handle_t handle) {
    ++sequence_;
    const std::lock_guard<std::mutex> lock(mutex_);
    get_ = &rtp_audio::get_ignore;
    put_ = &rtp_audio::put_player;
    audio_ = handle;
    first_ = ::lseek(handle, 0L, SEEK_CUR);
    offset_ = expect_ = counted_ = 0;
    current_ = sequence_;
}

void rtp_audio::record_file(audiofile::handle_t handle) {
    ++sequence_;
    const std::lock_guard<std::mutex> lock(mutex_);
    get_ = &rtp_audio::get_record;
    put_ = &rtp_audio::put_silent;
    audio_ = handle;
    first_ = ::lseek(handle, 0L, SEEK_CUR);
    offset_ = expect_ = counted_ = 0;
    current_ = sequence_;
}

auto rtp_audio::silent(bool shutdown) -> long {
    ++sequence_;
    if(shutdown)
        active_ = false;

    const std::lock_guard<std::mutex> lock(mutex_);
    auto dp = output_packet.data();
    memset(dp, filler_, static_cast<std::size_t>(samples_) * bytes_);
    output_packet.set_payload(payload, samples_, framing_);
    get_ = &rtp_audio::get_ignore;
    put_ = &rtp_audio::put_silent;
    auto timed = counted_;
    counted_ = 0L;
    offset_ = expect_ = 0;
    return timed;
}

auto rtp_audio::put_tonegen(rtp_packet& rtp) noexcept -> std::size_t {
    auto dp = rtp.data();
    auto total = samples_ * bytes_;
    auto count{0U};
    auto framing = framing_;
    for(;;) {
        std::unique_lock lock(session_->access_lock);
        auto handle = audio_;
        auto current = current_;
        auto repeat = repeat_;
        counted_ += framing;
        framing = 0;
        lock.unlock();
        if(handle == audiofile::invalid || current != sequence_) break;
        auto result = ::read(handle, dp + count, std::size_t(total - count)); // FlawFinder: ignore
        if(result + count == total) return std::size_t(samples_);
        auto s(Driver::get(session_->sid()));
        if(result < 0) {
            Session::event_t err{event_type::err};
            err.reason = "tone file error";
            Session::send(s, err);
            break;
        }
        if(repeat == 1) {
            const Session::event_t end{event_type::end};
            Session::send(s, end);
            break;
        }
        lock.lock();
        if(repeat_ > 1 && current == sequence_)
            --repeat_;
        lock.unlock();
        // rotate thru start of tone...
        count += result;
        ::lseek(handle, first_, SEEK_SET);
    }
    if(count < total)
        memset(dp + count, filler_, std::size_t(total - count));
    return std::size_t(samples_);
}

auto rtp_audio::put_player(rtp_packet& rtp) noexcept -> std::size_t {
    auto dp = rtp.data();
    auto total = samples_ * bytes_;
    auto count{0U};
    auto framing = framing_;
    for(;;) {
        std::unique_lock lock(session_->access_lock);
        auto handle = audio_;
        auto current = current_;
        counted_ += framing;
        framing = 0;
        lock.unlock();
            if(handle == audiofile::invalid || current != sequence_)
            break;
        auto result = ::read(handle, dp + count, std::size_t(total - count)); // FlawFinder: ignore
        if(result + count == total) return std::size_t(samples_);
        auto s(Driver::get(session_->sid()));
        if(result < 0) {
            Session::event_t err{event_type::err};
            err.reason = "player error";
            Session::send(s, err);
            break;
        } // if new file, chain read remainder of buffer...
        count += result;
        if(current == sequence_) {
            lock.lock();
            handle = audio_ = session_->get_phrase();
            lock.unlock();
        }
        else
            handle = audiofile::invalid;
        if(handle == audiofile::invalid) {
            const Session::event_t end{event_type::end};
            Session::send(s, end);
            break;
        }
    }
    if(count < total)
        memset(dp + count, filler_, std::size_t(total - count));
    return std::size_t(samples_);
}

auto rtp_audio::put_silent([[maybe_unused]] rtp_packet& rtp) noexcept -> std::size_t {   // NOLINT
    return std::size_t(samples_);
}

auto rtp_audio::put_ignore([[maybe_unused]] rtp_packet& rtp) noexcept -> std::size_t {   // NOLINT
    return std::size_t(0);
}

auto rtp_audio::get_ignore([[maybe_unused]] rtp_packet& rtp, [[maybe_unused]] uint32_t ts) noexcept -> std::size_t {   // NOLINT
    return std::size_t(0);
}

auto rtp_audio::get_record([[maybe_unused]] rtp_packet& rtp, [[maybe_unused]] uint32_t ts) noexcept -> std::size_t {   // NOLINT
    auto dp = rtp.data();
    auto total = samples_ * bytes_;
    auto framing = framing_;
    auto empty = std::make_unique<char []>(total);

    audiofile::handle_t handle{audiofile::invalid};
    memset(&empty[0], filler_, total);

    for(;;) {
        std::unique_lock lock(session_->access_lock);
        handle = audio_;
        if(handle == audiofile::invalid || current_ != sequence_) return std::size_t(0);
        if(!filler_ || ts <= expect_) break;
        auto pos = first_ + static_cast<std::size_t>(bytes_ * expect_);
        expect_ += samples_;
        counted_ += framing;
        framing = 0;
        lock.unlock();

        ::lseek(handle, static_cast<audiofile::offset_t>(pos), SEEK_SET);
        ::write(handle, &empty[0], total);
    }
    auto pos = first_ + static_cast<std::size_t>(bytes_ * ts);
    ::lseek(handle, static_cast<audiofile::offset_t>(pos), SEEK_SET);
    ::write(handle, dp, total);
    return std::size_t(samples_);
}
