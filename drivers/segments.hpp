/*
 * Copyright (C) 2022 Tycho Softworks.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sessions.hpp"
#include "media.hpp"
#include "exosip.hpp"

namespace server {
using segment_t = struct Segment final : public server::Session { // NOLINT
    Segment(script& attach, const exosip::event_t& event) :
    Session(attach), cid(event.cid()), did(event.did()), tid(event.tid()), context(event.context()) {}
    ~Segment() final;

    auto make_const(const symbols::init_t& list) {
        return syms.make_const(list);
    }

    auto ipc() const {
        return shm;
    }

    auto sdp() -> std::string;
    auto answer(int status, std::function<void(exosip::msg_t)> fill = [](auto msg){}) -> bool;
    void terminate();
    void apply_sdp(const std::string_view& sdp) const;
    void connect(std::string local_media, std::string_view& sdp, uint16_t port);

    void disconnect() final;
    void stream() final;
    void silence(bool shutdown) final;
    auto play_files() -> bool final;
    auto play_tone() -> bool final;
    auto record_file() -> bool final;

    int cid, did, tid;
    eXosip_t *context;
    rtp_audio *audio{nullptr};
};
}
