# Copyright (C) 2020 Tycho Softworks.
#
# This file is free software; as a special exception the author gives
# unlimited permission to copy and/or distribute it, with or without
# modifications, as long as this notice is preserved.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

add_library(driver_sip MODULE sip.cpp driver.hpp segments.hpp segments.cpp media.hpp media.cpp exosip.hpp)
target_link_libraries(driver_sip ${EXOSIP_LIBS})
set_target_properties(driver_sip PROPERTIES
    PREFIX "" CXX_VISIBILITY_PRESET hidden)

if(MALLOC_FOUND)
    target_link_libraries(driver_sip PkgConfig::MALLOC)
endif()

add_library(driver_test MODULE tests.hpp tests.cpp)
set_target_properties(driver_test PROPERTIES PREFIX "" CXX_VISIBILITY_PRESET hidden)

install(TARGETS driver_sip DESTINATION "${BORDEAUX_DRIVERS_PATH}")
