/*
 * Copyright (C) 2020 Tycho Softworks.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if !defined(EXOSIP_HPP_) && __has_include(<eXosip2/eXosip.h>)
#define EXOSIP_HPP_

#include <memory>
#include <osipparser2/osip_port.h>
#include <string>
#include <cstring>
#include <cstdlib>
#include <eXosip2/eXosip.h>
#include <sys/types.h>
#ifdef WIN32
#include <winsock2.h>
#else
#include <sys/socket.h>
#include <netinet/in.h>
#endif

namespace exosip {
enum class listen_type {
    udp, tcp, tls, dtls
};

enum class family_type : int {
    ipv4 = AF_INET, ipv6 = AF_INET6,
};

enum class tls_error: int {
    none = TLS_OK,
    random = TLS_ERR_NO_RAND,
    dhparam = TLS_ERR_NO_DH_PARAM,
    password = TLS_ERR_NO_PW,
    root = TLS_ERR_NO_ROOT_CA,
    cert = TLS_ERR_MISSING_AUTH_PART,
};

using msg_t = osip_message_t*;
using body_t = osip_body_t*;
using header_t = osip_header_t*;

inline void release(void *p) {
    if(p != nullptr) {
        if(osip_free_func)
            osip_free_func(p);
        else {
            ::free(p); // NOLINT
        }
    }
}

class certificate_t final {
public:
    certificate_t() { // NOLINT
        memset(&tls_, 0, sizeof(tls_));
    }

    auto operator*() const {
        return &tls_;
    }

    auto set_certificate(const std::string& crt_file) {
        snprintf(tls_.client.cert, sizeof(tls_.client.cert),
            "%s", crt_file.c_str());
    }

    auto set_authority(const std::string& ca_path) {
        snprintf(tls_.root_ca_cert, sizeof(tls_.root_ca_cert),
            "%s", ca_path.c_str());
    }

    auto set_keyfile(const std::string& key_file) {
        snprintf(tls_.client.priv_key, sizeof(tls_.client.priv_key),
            "%s", key_file.c_str());
    }

    auto set_password(const std::string& password) {
        snprintf(tls_.client.priv_key_pw, sizeof(tls_.client.priv_key_pw),
            "%s", password.c_str());
    }

private:
    eXosip_tls_ctx_t tls_;
};

class context_t final {
public:
    explicit operator eXosip_t *() const { return ctx_;}
    explicit operator bool() const { return ctx_ != nullptr;}
    auto operator!() const { return ctx_ == nullptr;}
    auto operator*() const { return ctx_;}

    class guard final {
    public:
        explicit guard(const context_t& from) : context_(*from) {
            eXosip_lock(context_);
        }

        explicit guard(eXosip_t *from) : context_(from) {
            eXosip_lock(context_);
        }

        ~guard() {
            eXosip_unlock(context_);
        }

    private:
        eXosip_t *context_;
    };

    explicit context_t(const char *id) noexcept : 
    ctx_(eXosip_malloc()) {
        eXosip_init(ctx_);
        eXosip_set_user_agent(ctx_, id);
    }

    ~context_t() {
        eXosip_quit(ctx_);
        release(ctx_);
    }

    context_t() = delete;
    context_t(const context_t& from) = delete;
    auto operator=(const context_t& from) -> context_t& = delete;

    auto family() const {
        return family_;
    }

    auto schema() const {
        return schema_;
    }

    auto active() const {
        return proto_ != 0;
    }

    auto tcp() const {
        return proto_ == IPPROTO_TCP;
    }

    auto port() const {
        return proto_ ? port_ : 0;
    }

    auto close() noexcept {
        if(ctx_ && proto_) {
            eXosip_quit(ctx_);
            proto_ = 0;
        }
    }

    auto find_port(int port = 5062, int proto = IPPROTO_UDP) const {
        for(;;) {
            port = eXosip_find_free_port(ctx_, port, proto);
            if(port & 0x01)
                ++port;
            else
                break;
        }
        return port;
    }

    // set_option uses different types, so we create type specific functions

    auto reuse_tcp_port(bool enable) {
        int value = enable ? 1 : 0;
        eXosip_set_option(ctx_, EXOSIP_OPT_ENABLE_REUSE_TCP_PORT, &value);
    }

    auto use_ephemeral_port(bool enable) {
        int value = enable ? 1 : 0;
        eXosip_set_option(ctx_, EXOSIP_OPT_ENABLE_USE_EPHEMERAL_PORT, &value);
    }

    auto set_family(family_type family) {
        int value = family == family_type::ipv6 ? 1 : 0;
        family_ = value ? AF_INET6 : AF_INET;
        eXosip_set_option(ctx_, EXOSIP_OPT_ENABLE_IPV6, &value);
    }

    auto use_rport(bool enable) {
        int value = enable ? 1 : 0;
        eXosip_set_option(ctx_, EXOSIP_OPT_USE_RPORT, &value);
    }

    auto srv_lookup(bool enable) {   // enabled by default
        int value = enable ? 2 : 0;
        eXosip_set_option(ctx_, EXOSIP_OPT_DNS_CAPABILITIES, &value);
    }

    auto set_keep_alive(int msecs) { // common value 17000
        if(proto_ == IPPROTO_UDP) {
            eXosip_set_option(ctx_, EXOSIP_OPT_UDP_KEEP_ALIVE, &msecs);
            return true;
        }
        return false;
    }

    auto set_user_agent(const std::string& name) {
        eXosip_set_user_agent(ctx_, name.c_str());
    }

    auto set_dscp(int value = 26) {
        eXosip_set_option(ctx_, EXOSIP_OPT_SET_DSCP, &value);
    }

    auto set_gateway(const std::string& host) {
        if(family_ == AF_INET6) return eXosip_set_option(ctx_, EXOSIP_OPT_SET_IPV6_FOR_GATEWAY, host.c_str()) == 0;
        return eXosip_set_option(ctx_, EXOSIP_OPT_SET_IPV4_FOR_GATEWAY, host.c_str()) == 0;
    }

    auto set_verify(bool enable) {
        int value = enable ? 1 : 0;
        eXosip_set_option(ctx_, EXOSIP_OPT_SET_TLS_VERIFY_CERTIFICATE, &value);
    }

    auto set_certificate(const certificate_t& cert) {
        return static_cast<tls_error>(eXosip_set_option(ctx_,
            EXOSIP_OPT_SET_TLS_CERTIFICATES_INFO, *cert));
    }

    auto listen(const std::string& host = "*", listen_type type = listen_type::udp, uint16_t port = 5060) {
        auto addr = host.c_str();
        auto secure = (type == listen_type::dtls || type == listen_type::tls) ? 1 : 0;
        port_ = port;
        proto_ = (type == listen_type::udp || type == listen_type::dtls) ? IPPROTO_UDP : IPPROTO_TCP;
        if(addr[0] == 0 || addr[0] == '*' || host == "::*" || host == "::")
            addr = nullptr;
        schema_ = secure ? "sips" : "sip";
        if(!eXosip_listen_addr(ctx_, proto_, addr, port,  family_, secure)) return true;
        proto_ = 0;
        return false;
    }

    auto automatic() {
        const guard lock(ctx_);
        eXosip_automatic_action(ctx_);
    }

private:
    uint16_t port_{0};
    int proto_{0};
    int family_{AF_INET};
    std::string schema_{"sip"};
    eXosip_t *ctx_{nullptr};
};

using guard_t = context_t::guard;

class event_t {
public:
    explicit event_t(const context_t& ctx, int timeout = 0) noexcept :
    ctx_(*ctx), ptr_(std::make_shared<data>(*ctx, timeout)) {
        evt_ = ptr_->event;
    }

    event_t() noexcept :
    ptr_(nullptr) {}

    explicit operator const eXosip_event_t *() const { return evt_;}
    explicit operator bool() const { return evt_ != nullptr;}
    auto operator!() const { return evt_ == nullptr;}
    auto operator*() const { return evt_;}
    auto operator->() const { return evt_;}

    auto operator==(const event_t& rhs) {
        return evt_ == rhs.evt_;
    }

    auto operator!=(const event_t& rhs) {
        return evt_ != rhs.evt_;
    }

    auto context() const {
        return ctx_;
    }

    auto type() const {
        return evt_ ? evt_->type : -1;
    }

    auto text() const {
        return evt_ ? evt_->textinfo : "timeout";
    }

    auto request() const {
        return evt_ ? evt_->request : nullptr;
    }

    auto response() const {
        return evt_ ? evt_->response : nullptr;
    }

    auto ack() const {
        return evt_ ? evt_->ack : nullptr;
    }

    auto method() const {
        return request() ? evt_->request->sip_method : "NONE";
    }

    auto status() const {
        return response() ? evt_->response->status_code : 407;
    }

    auto timeout() const {
        return evt_ == nullptr;
    }

    auto tid() const {
        return evt_ ? evt_->tid : -1;
    }

    auto cid() const {
        return evt_ ? evt_->cid : -1;
    }

    auto did() const {
        return evt_ ? evt_->did : -1;
    }

    auto automatic() const {
        const guard_t lock(ctx_);
        return eXosip_default_action(ctx_, evt_) == 0;
    }

protected:
    struct data {
        eXosip_event_t *event;

        explicit data(eXosip_t *ctx, int timeout) noexcept : event(eXosip_event_wait(ctx, timeout / 1000,  timeout % 1000)) {}

        ~data() {
            if(event)
                eXosip_event_free(event);
        }

        data(const data&) = delete;
        auto operator=(const data&) -> data& = delete;
    };

    eXosip_event_t *evt_{};
    eXosip_t *ctx_{};
    std::shared_ptr<const data> ptr_;
};
} // end namespace
#endif
