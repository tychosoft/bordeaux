/*
 * Copyright (C) 2020 Tycho Softworks.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sessions.hpp"
#include "segments.hpp"
#include "utils.hpp"

#include <moderncli/strings.hpp>
#include <moderncli/keyfile.hpp>
#include <moderncli/process.hpp>
#include <moderncli/sync.hpp>
#include <moderncli/scan.hpp>
#include <webforcpp/json.hpp>

#include <unordered_set>
#include <unordered_map>
#include <atomic>

using namespace server;
using namespace exosip;
using namespace std::literals::chrono_literals;

using map_t = std::unordered_map<int, segment_t *>;
using maplist_t = struct {
    map_t calls;
    map_t dialogs;
};

namespace server {
struct extend final : public Session {
    auto scr_notify() noexcept -> bool;
};
} // end namespace

namespace server::checks {
auto chk_notify(const script::context_t& ctx) -> const char *;
} // end namespace

namespace {
const char *DIVERT_HEADER = "Diversion";
const char *DIGITS_HEADER = "X-Digits";
const char *FEATURE_SCRIPT = "X-Feature-Script";
const char *FEATURE_MODE = "X-Feature-Mode";
const char *AGENT_ID = SERVER_ID "/" PROJECT_VERSION;
const Session::event_t drop_event{event_type::drop};
const Session::event_t ack_event{event_type::answer};
std::string lowest_extension = "10", highest_extension = "89";
std::string site_name = "Unknown Server";
std::string site_url = "http://localhost:80/";

uint16_t media_base = 0;
context_t listener(AGENT_ID);
unique_sync<maplist_t> maplist;
std::string remote_uri, route_uri, media_address;
std::thread event_thread;
std::atomic<int> active_rid = -1;
std::unordered_set<std::string> local_hosts = {"localhost", "localhost.localdomain"};

[[maybe_unused]] auto is_local(osip_uri *uri) {
    if(!uri || !uri->host) return false;
    return std::any_of(local_hosts.begin(), local_hosts.end(), [uri](const std::string_view& localhost){
        return localhost == uri->host;
    });
}

[[maybe_unused]] auto msg_to_str(exosip::msg_t msg) -> std::string {
    char *text = nullptr;
    std::size_t tlen = 0;

    osip_message_to_str(msg, &text, &tlen);
    if(!text) return {};
    std::string result(text, tlen);
    exosip::release(text);
    return result;
}

auto subject(exosip::msg_t msg) {
    osip_header_t *header{nullptr};
    if(msg)
        osip_message_get_subject(msg, 0, &header);
    return (header && header->hvalue) ? std::string(header->hvalue) : "";
}

void fail_call(const event_t& event, int code) {
    const exosip::guard_t lock(event.context());
    eXosip_call_send_answer(event.context(), event.tid(), code, nullptr);
}

void fail_message(const event_t& event, int code) {
    const exosip::guard_t lock(event.context());
    eXosip_message_send_answer(event.context(), event.tid(), code, nullptr);
}

auto call_info(int cid, const osip_message_t *request) -> int {
    const segment_t *leg = nullptr;
    sync_ptr maps(maplist);
    try {
        leg = maps->calls.at(cid);
    }
    catch(...) {
        logger.debug(5, "no info call leg for cid={}", cid);
        return 488;
    }

    auto sid = leg->sid();
    maps.unlock();

    auto ctype = osip_message_get_content_type(request);
    if(!ctype || !ctype->type || !ctype->subtype) return SIP_406_NOT_ACCEPTABLE;
    if(!eq(ctype->type, "application") || !eq(ctype->subtype, "dtmf-relay")) return SIP_406_NOT_ACCEPTABLE;
    osip_body_t *body = nullptr;
    osip_message_get_body(request, 0, &body);
    if(!body || body->length < 1) return SIP_406_NOT_ACCEPTABLE;
    const std::string_view dtmf(body->body, body->length);
    auto pos = dtmf.find("Signal=");
    if(pos == std::string_view::npos) return SIP_406_NOT_ACCEPTABLE;
    Session::event_t dtmf_event{event_type::digit};
    dtmf_event.digit[0] = dtmf[pos + 7];
    dtmf_event.digit[1] = 0;
    try {
        auto session(Driver::get(sid));
        Session::send(session, dtmf_event);
        return SIP_OK;
    }
    catch(...) {
        return 488;
    }
}

void allow_options(exosip::msg_t msg) {
    osip_message_set_header(msg, ALLOW, "OPTIONS,INVITE,BYE,CANCEL,ACK,INFO");
    osip_message_set_header(msg, ACCEPT, "application/sdp,application/dtmf-relay,text/plain");
    osip_message_set_header(msg, ACCEPT_ENCODING, "text/plain");
    osip_message_set_header(msg, ACCEPT_LANGUAGE, "en");    // TODO: Phrasebook
    osip_message_set_header(msg, SUPPORTED, "100rel");
}

// hidden event handlers
void do_receipt(const event_t& event) {
    using namespace web::json;

    auto response = event.response();
    auto ctype = osip_message_get_content_type(response);

    osip_body_t *data{nullptr};
    osip_message_get_body(response, 0, &data);
    if(!data || !data->body || !ctype || !eq(ctype->subtype, "json")) return;

    const ast_t ast;
    const auto& json = parse(*ast, mutable_string_view(data->length, data->body));
    if(!is(json)) {
        logger.error("invalid json receipt format");
        return;
    }

    auto root = *json;
    if(!root.is_object()) {
        logger.error("invalid json receipt format");
        return;
    }

    auto name = root("n");
    if(name.is_string())
        site_name = name.as_string();

    auto url = root("url");
    if(url.is_string())
        site_url = url.as_string();
}

void do_close(const event_t& event) {
    auto cid = event.cid();
    segment_t *leg = nullptr;
    sync_ptr maps(maplist);
    try {
        leg = maps->calls.at(cid);
    }
    catch(...) {
        logger.debug(5, "no call leg for cid={}", cid);
        return;
    }

    // if no active call, bail...
    if(leg->cid < 1) return;
    logger.debug(4, "closing call for cid={}", cid);
    auto sid = leg->sid();
    maps->calls.erase(leg->cid);
    if(leg->did > 0)
        maps->dialogs.erase(leg->did);
    leg->cid = leg->did = leg->tid = 0;
    maps.unlock();

    try {
        auto session(Driver::get(sid));
        Session::send(session, drop_event);
    }
    catch(...) {}
}

auto do_ack(const event_t& event) {
    auto request = event.request();
    auto cid = event.cid();
    auto did = event.did();
    segment_t *leg = nullptr;
    sync_ptr maps(maplist);
    try {
        leg = maps->calls.at(cid);
    }
    catch(...) {
        logger.debug(5, "no call leg for cid={}", cid);
        return;
    }

    // if no rtp or call, we can ignore
    if(leg->cid < 1) return;
    logger.debug(4, "ack call for cid={}", cid);
    auto sid = leg->sid();
    if(did > 0) {
        if(did != leg->did) {
            if(leg->did > 0)
                maps->dialogs.erase(leg->did);
            maps->dialogs[did] = leg;
        }
        leg->did = did;
        leg->tid = -1;
    }

    std::string_view sdp;
    osip_body_t *body{nullptr};
    osip_message_get_body(request, 0, &body);
    if(body && body->body && body->length)
        sdp = std::string_view(body->body, body->length);

    maps.unlock();
    leg->apply_sdp(sdp);
    // TODO: start rtp session...

    try {
        auto session(Driver::get(sid));
        Session::send(session, ack_event);
    }
    catch(...) {}
}

auto do_invite(const event_t& event) {
    auto request = event.request();
    auto from = request->from;
    auto to = request->to;
    auto caller_from = std::string(from->url->username ? from->url->username : "unknown");
    auto caller_id = unquote<std::string>(from->displayname ? from->displayname : "unknown");
    auto dialed_to = std::string(to->url->username ? to->url->username : "unknown");
    auto start = "main";
    auto remote = from->url->host;
    if(eq(remote, to->url->host))
        remote = nullptr;

    script scr{};   // NOLINT
    osip_header_t *feature_script{nullptr};
    osip_message_header_get_byname(request, FEATURE_SCRIPT, 0, &feature_script);

    // integrated direct call to feature script
    if(feature_script && feature_script->hvalue) {
        dialed_to = feature_script->hvalue;
        try {
            scr = Driver::get_script(feature_script->hvalue);
        }
        catch(...) {
            logger.error("cannot find feature script {} for call from {}", dialed_to, caller_from);
            return SIP_NOT_FOUND;
        }
    }
    else {
        try {
            if(remote) {
                scr = Driver::get_script("remote.scr");
                scr = Driver::get_script(remote + ".scr"s);
            }
            else
                scr = Driver::get_script("default.scr");
        }
        catch(...) {}

        try {
            if(!scr)
                scr = Driver::get_script("sip.scr");
        }
        catch(...) {}

        try {
            if(dialed_to != util::uri_userid(remote_uri))
                scr = Driver::get_script(dialed_to + ".scr");
            scr = Driver::get_script(caller_from + ".scr");
        }
        catch(...) {}
    }

    if(scr.empty()) {
        logger.error("cannot find script for call from {} to {}", caller_from, dialed_to);
        return SIP_NOT_FOUND;
    }

    // ignite new call leg
    logger.notice("call from \"{}\" at {} to {}\n", caller_id, caller_from, dialed_to);
    const auto session(std::make_shared<segment_t>(scr, event));
    auto leg = dynamic_cast<segment_t*>(session.get());
    leg->make_const({
        {"SUBJECT", subject(request)},
        {"CALLER", caller_from},
        {"DIALED", dialed_to},
        {"FROM", caller_from},
        {"TO", dialed_to},
        {"CALLER_ID", caller_id},
        {"SITE_NAME", site_name},
        {"SITE_URL", site_url},
        {"CALLED_TO", dialed_to},
    });

    osip_header_t *divert{nullptr}, *digits{nullptr}, *fmode{nullptr};
    osip_message_header_get_byname(request, DIVERT_HEADER, 0, &divert);
    osip_message_header_get_byname(request, DIGITS_HEADER, 0, &digits);    osip_message_header_get_byname(request, FEATURE_SCRIPT, 0, &feature_script);
    osip_message_header_get_byname(request, FEATURE_MODE, 0, &fmode);

    if(feature_script && feature_script->hvalue) {
        leg->make_const({{"FEATURE_SCRIPT", feature_script->hvalue}});
        if(digits && digits->hvalue)
            leg->make_const({{"EXTRA_DIGITS", digits->hvalue}});

        if(fmode && fmode->hvalue)
            leg->make_const({{"FEATURE_MODE", fmode->hvalue}});
    }

    if(divert && divert->hvalue) {
        osip_from_t *fwd{nullptr};
        osip_from_init(&fwd);
        osip_from_parse(fwd, divert->hvalue);
        if(fwd->displayname)
            leg->make_const({{"DIVERT_ID", fwd->displayname}});
        if(fwd->url && fwd->url->username)
            leg->make_const({{"DIVERT_FROM", fwd->url->username}});

        osip_uri_param_t *reason{nullptr};
        osip_from_param_get_byname(fwd, const_cast<char *>("reason"), &reason);
        if(reason && reason->gvalue)
            leg->make_const({{"DIVERT_REASON", reason->gvalue}});

        osip_uri_param_t *privacy{nullptr};
        osip_from_param_get_byname(fwd, const_cast<char *>("privacy"), &privacy);
        if(privacy && privacy->gvalue)
            leg->make_const({{"DIVERT_PRIVACY", privacy->gvalue}});

        osip_uri_param_t *screen{nullptr};
        osip_from_param_get_byname(fwd, const_cast<char *>("screen"), &screen);
        if(privacy && privacy->gvalue)
            leg->make_const({{"DIVERT_SCREEN", screen->gvalue}});

        osip_from_free(fwd);
    }

    if(remote)
        leg->make_const({{"REMOTE", remote}});

    if(!Driver::attach(session)) return SIP_TEMPORARILY_UNAVAILABLE;
    auto port = media_base + (2 * leg->slot());
    auto ipc = leg->ipc();
    std::string_view sdp{}; // NOLINT
    osip_body_t *body{nullptr};
    osip_message_get_body(request, 0, &body);
    if(body && body->body && body->length)
        sdp = std::string_view(body->body, body->length);

    str_copy(ipc->caller, sizeof(ipc->caller), caller_from);
    str_copy(ipc->dialed, sizeof(ipc->dialed), dialed_to);
    leg->ipc()->state = ipc_session::ISC_INVITING;
    leg->ipc()->type = ipc_session::ISC_INBOUND;
    leg->connect(media_address, sdp, port);
    if(leg->is_label("ring")) {
        start = "ring";
        if(!leg->answer(SIP_RINGING)) return SIP_TEMPORARILY_UNAVAILABLE;
        leg->ipc()->state = ipc_session::ISC_RINGING;
    }
    else if(leg->is_label("main")) {
        if(!leg->answer(SIP_OK, [leg](auto msg) {
            auto sdp = leg->sdp();
            if(!sdp.empty()) {
                osip_message_set_body(msg, sdp.data(), sdp.size());
                osip_message_set_content_type(msg, "application/sdp");
            }
        }))
            return SIP_TEMPORARILY_UNAVAILABLE;
        leg->stream();
    }
    else
        return SIP_TEMPORARILY_UNAVAILABLE;

    guard_ptr maps(maplist);    // NOLINT
    maps->calls[leg->cid] = leg;
    if(leg->did > 0)
        maps->dialogs[leg->did] = leg;

    Session::begin(session, start);
    return SIP_OK;
}

void do_options(const event_t& event) {
    auto context = event.context();
    auto user = event.request()->to->url->username; // to->url proven earlier
    auto tid = event.tid();

    if(!user || eq(user, "system")) {   // Ping!
        auto msg = exosip::msg_t(nullptr);
        const exosip::guard_t lock(context);
        eXosip_options_build_answer(context, tid, SIP_OK, &msg);
        allow_options(msg);
        eXosip_options_send_answer(context, tid, SIP_OK, msg);
    }
    else {
        const exosip::guard_t lock(context);
        eXosip_options_send_answer(context, tid, SIP_FORBIDDEN, nullptr);
    }
}
} // end anon namespace

driver driver::instance;
driver& server::sip = driver::instance;

driver::driver() : Driver("sip") {
    keywords.insert(
        {"notify", {
            static_cast<method_t>(&extend::scr_notify),
            static_cast<check_t>(&checks::chk_notify)}}
    );
}

void driver::configure(keyfile& config, unsigned verbose) {
    const auto& server = config["server"];
    const auto& hostname = server.at("hostname");
    auto& keys = config["sip"];
    auto& address = keys["address"];
    auto& authorize = keys["authorize"];
    auto& extension = keys["extension"];
    auto& route = keys["route"];
    auto& type = keys["type"];
    auto& expires = keys["expires"];

    if(route.empty())
        route = util::make_uri("sip:", hostname, 5060);
    else if(route == "static")
        route = util::make_uri("sip:", util::uri_address(hostname), 5060);
    else if(!begins_with(route, "sip:"))
        route = "sip:" + route;

    if(address.empty())
        address = "*";

    if(extension.empty())
        extension = "89";

    if(authorize.empty())
        authorize = extension;

    if(type.empty())
        type = "udp";

    if(expires.empty())     // 5 minute default
        expires = "300";

    Driver::configure(config, verbose);
}

void driver::restart(const keyfile& config) {
    const auto& keys = config["sip"];
    auto route = keys.at("route").c_str();
    auto extension = keys.at("extension").c_str();
    auto authorize = keys.at("authorize").c_str();
    auto server_host = util::uri_host(route);
    auto identity = util::make_uri("sip:", extension, server_host);
    auto expires = int(get_unsigned_or(keys.at("expires"), 300U, 60U, 3600U));
    const char *password = nullptr;
    try {
        password = keys.at("password").c_str();
    }
    catch(...) {
        logger.warn("registration password not set");
    }

    logger.info("remote identity {}", identity);
    logger.info("remote routing {}", route);

    const exosip::guard_t lock(listener); // lock events while updating
    route_uri = route;
    remote_uri = identity;
    Driver::restart(config);        // kick off scripts

    release_id();
    eXosip_clear_authentication_info(*listener);
    if(!password || !*password || !extension || !*extension) return;
    eXosip_add_authentication_info(*listener, extension, authorize, password, nullptr, nullptr);

    auto msg = exosip::msg_t(nullptr);
    active_rid = eXosip_register_build_initial_register(*listener, identity.c_str(), route, nullptr, expires, &msg);
    if(active_rid < 0) {
        logger.error("failed to register {}", remote_uri);
        return;
    }

    logger.info("starting registration {}", remote_uri);
    allow_options(msg);
    eXosip_register_send_register(*listener, active_rid, msg);
}

void driver::startup(const keyfile& config) {
    const auto& server = config["server"];
    const auto& keys = config["sip"];
    const auto& hostname = server.at("hostname");
    auto bind(keys.at("address"));
    auto type = listen_type::udp;
    auto port = get_unsigned_or<uint16_t>(keys.at("port"), 0, 1);

    if(keys.at("type") == "tcp")
        type = listen_type::tcp;
    else if(keys.at("type") == "tls")
        type = listen_type::tls;

    auto addr = inet_bind(bind, type == listen_type::tls ? "sips" : "sip", ipv6 ? AF_INET6 : AF_INET, type == listen_type::udp ? SOCK_DGRAM : SOCK_STREAM);
    if(addr.empty())
        logger.crit(98, "{}: driver address invalid", bind);

    if(addr.is_any())
        bind = "*";
    else
        bind = addr.host();

    if(!port)
        port = listener.find_port(5062, IPPROTO_UDP);

    listener.set_family(ipv6 ? family_type::ipv6 : family_type::ipv4);
    listener.srv_lookup(false);

    if(!listener.listen(bind, type, port))
        logger.crit(98, "cannot bind to {}:{}", bind, port);

    logger.info("starting {}:{}", bind, port);


    media_address = inet_find(server.at("media"), "", ipv6 ? AF_INET6 : AF_INET).host();
    media_base = port + 2;
    local_hosts.emplace(ipv6 ? "[::1]" : "127.0.0.1");
    local_hosts.emplace(media_address);
    local_hosts.emplace(hostname);
    local_hosts.emplace(util::uri_address(hostname));
    local_hosts.emplace(system_hostname());
    local_hosts.emplace(util::uri_address(system_hostname()));
    logger.info("media address {}", media_address);

    restart(config);   // kick off scripts, etc
    event_thread = std::thread(&driver::dispatch, this, 1);
}

void driver::disconnect() {
    if(listener.active() && active_rid != -1) {
        const exosip::guard_t lock(*listener);
        release_id();
    }
    if(listener.active()) {
        Driver::disconnect();
        std::this_thread::sleep_for(std::chrono::milliseconds(960));
    }
    else
        Driver::disconnect();
}

void driver::shutdown() {
    guard_ptr maps(maplist);
    for(auto& [cid, leg] : maps->calls) {
        leg->terminate();
    }

    Driver::shutdown();

    if(listener.active()) {
        listener.close();
        event_thread.join();
    }
}

void driver::answer(Session *session) {
    auto leg = dynamic_cast<segment_t*>(session);
    leg->answer(SIP_OK, [leg](auto msg) {
        auto sdp = leg->sdp();
        if(!sdp.empty()) {
            osip_message_set_body(msg, sdp.data(), sdp.size());
                osip_message_set_content_type(msg, "application/sdp");
        }
    });
}

void driver::drop(Session *session) {
    auto leg = dynamic_cast<segment_t*>(session);
    leg->disconnect();

    guard_ptr maps(maplist);
    if(leg->did > 0)
        maps->dialogs.erase(leg->did);
    if(leg->cid > 0)
        maps->calls.erase(leg->cid);
    leg->terminate();
    logger.debug(4, "map counts {}, {}\n", maps->dialogs.size(), maps->calls.size());
}

void driver::release_id() {
    if(active_rid == -1) return;
    logger.debug(3, "releasing registration");

    // Locked before we get here...
    auto msg = exosip::msg_t(nullptr);
    auto res = eXosip_register_build_register(*listener, active_rid, 0, &msg);
    if(res > -1)
        eXosip_register_send_register(*listener, active_rid, msg);
    else
        logger.error("unregister failed");
    active_rid = -1;
    Driver::notify(false);
    sys_mapped->active = false;
}

// event dispatch listener thread
void driver::dispatch(int priority) {
    if(!this_thread::priority(priority))
        priority = 0;

    logger.info("starting listener thread; pri={}", priority);

    auto prior = sync_clock();
    for(;;) {
        const exosip::event_t event(listener, 500);
        if(!running) break;
        // breakdown events to dispatch handlers with minimal stack frames
        if(is(event)) {
            logger.debug(5, "event: {}", event.text());

            auto request = event.request();
            auto response = event.response();
            auto status_code = response ? response->status_code : 409;

            switch(event.type()) {
            case EXOSIP_MESSAGE_NEW:
                if(!request)
                    fail_message(event, SIP_BAD_REQUEST);
                else if(MSG_IS_OPTIONS(request))
                    do_options(event);
                else if(MSG_IS_MESSAGE(request))
                    logger.debug(4, "message event...");
                else
                    fail_message(event, SIP_METHOD_NOT_ALLOWED);
                break;
            case EXOSIP_CALL_INVITE:
                if(!request || event.cid() < 1) {
                    logger.debug(2, "bad invite request");
                    fail_call(event, SIP_BAD_REQUEST);
                }
                else if(sys_mapped->used >= limit_) {
                    logger.warn("declined call, all sessions busy");
                    fail_call(event, SIP_BUSY_HERE);
                }
                else {
                    status_code = do_invite(event);
                    if(status_code != SIP_OK) {
                        logger.error("incoming call failed; code={}", status_code);
                        fail_call(event, status_code);
                    }
                }
                break;
            case EXOSIP_CALL_MESSAGE_NEW:
                if(!request || event.cid() < 1) {
                    logger.debug(2, "bad invite request");
                    fail_call(event, SIP_BAD_REQUEST);
                }
                else if(MSG_IS_BYE(request)) {
                    do_close(event);
                }
                else if(MSG_IS_INFO(request)) {
                    auto tid = event.tid();
                    auto status = call_info(event.cid(), request);
                    osip_message_t *msg = nullptr;
                    const exosip::guard_t lock(*listener);
                    eXosip_call_build_answer(*listener, tid, status, &msg);
                    eXosip_call_send_answer(*listener, tid, status, msg);
                }
                else {
                    logger.debug(4, "method not allowed {}", request->sip_method);
                    fail_call(event, SIP_METHOD_NOT_ALLOWED);
                }
                break;
            case EXOSIP_REGISTRATION_FAILURE:
                if(active_rid != -1 && (status_code == SIP_UNAUTHORIZED || status_code == SIP_PROXY_AUTHENTICATION_REQUIRED)) {
                    logger.debug(1, "send credentials");
                    event.automatic();
                }
                else {
                    Driver::notify(false);
                    sys_mapped->active = false;
                    logger.warn("registration failed; code={}", status_code);
                }
                break;
            case EXOSIP_REGISTRATION_SUCCESS:
                if(active_rid == -1)
                    logger.info("released registration");
                else if(!online) {
                    Driver::notify(true);
                    sys_mapped->active = true;
                    logger.info("registered with server");
                }
                else
                    logger.debug(1, "registration confirmed");
                do_receipt(event);
                break;
            case EXOSIP_CALL_RELEASED:
            case EXOSIP_CALL_CLOSED:
                do_close(event);
                break;
            case EXOSIP_CALL_ACK:
                do_ack(event);
                break;
            default:
                event.automatic();
            }
        }

        auto current = sync_clock();
        if(!event || current > prior + 500ms) { // timeout
            logger.debug(6, "listener automatic; online={}", online.load());
            prior = current;
            listener.automatic();
            continue;
        }
    }
    logger.info("stopping listener thread");
}

void driver::notify(const char *to, const char *text, const char *subject) {
    auto target = util::uri_setuser(route_uri, to);

    osip_message_t *msg = nullptr;
    const exosip::guard_t lock(*listener);
    eXosip_message_build_request(*listener, &msg, "MESSAGE", target.c_str(), remote_uri.c_str(), route_uri.c_str());
    if(msg) {
        if(subject && *subject)
            osip_message_set_subject(msg, subject);
        osip_message_set_body(msg, text, str_size(text, 160));
        osip_message_set_content_type(msg, "text/plain");
        allow_options(msg);
        eXosip_message_send_request(*listener, msg);
    }
    else
        logger.error("failed to send message to {}", to);
}

auto extend::scr_notify() noexcept -> bool {
    auto to = syms.get_value("CALLER");
    auto subject = "";
    while(auto opt = option()) {
        auto val = value();
        if(!val) return error("missing value");
        if(eq(opt, "to"))
            to = val;
        else if(eq(opt, "subject"))
            subject = val;
    }

    try {
        char buffer[160]{0};
        buffer[0] = 0;
        while(auto v = value())
            mem_append(buffer, sizeof(buffer), v);

        driver::notify(to, buffer, subject);
    } catch(...) {
        return error("failed notify");
    }
    skip();
    return false;
}

auto checks::chk_notify(const script::context_t& ctx) -> const char * {
    if(ctx.header && eq(ctx.header->id, "_init_")) return "notify cannot be used to initialize";
    unsigned index = 0;
    const char *cp = nullptr;
    while(nullptr != (cp = ctx.line->argv[index])) {
        if(*cp != '=') break;
        cp = ctx.line->argv[++index];
        if(!cp) return "notify option missing";
        ++index;
    }

    if(!ctx.line->argv[index]) return "notify text missing";
    return nullptr;
}
