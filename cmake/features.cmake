# Copyright (C) 2020 Tycho Softworks.
#
# This file is free software; as a special exception the author gives
# unlimited permission to copy and/or distribute it, with or without
# modifications, as long as this notice is preserved.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

include(CheckCXXSourceCompiles)
include(CheckIncludeFileCXX)
include(CheckFunctionExists)
include(FindPkgConfig)

check_include_file_cxx(flite/flite.h FLITE_FOUND)
if(FLITE_FOUND)
    add_compile_definitions(FLITE=1)
    set(FLITE_LIBRARIES flite_${FLITE_VOICE} flite_${FLITE_LANGUAGE} flite_cmulex flite)
endif()

set(CMAKE_THREAD_PREFER_PTHREAD TRUE)
set(THREADS_PREFER_PTHREAD_FLAG TRUE)

find_package(Threads REQUIRED)
find_package(OpenSSL REQUIRED)
find_package(fmt CONFIG REQUIRED)

pkg_check_modules(MODERNCLI moderncli>=1.2.3 REQUIRED)
include_directories(${MODERNCLI_PREFIX}/include ${MODERNCLI_INCLUDEDIR})

pkg_check_modules(WEBFORCPP webforcpp>=0.1.0 REQUIRED)
include_directories(${WEBFORCPP_PREFIX}/include ${WEBFORCPP_INCLUDEDIR})

pkg_check_modules(OSIP2 libosip2>=5.3.0 REQUIRED)
include_directories(${OSIP2_INCLUDE_DIRS})
link_directories(${OSIP2_LIBRARY_DIRS})

if(BUILD_SYSTEMD)
    pkg_check_modules(SYSTEMD libsystemd REQUIRED)
    include_directories(${SYSTEMD_INCLUDE_DIRS})
    link_directories(${SYSTEMD_LIBRARY_DIRS})
    add_compile_definitions(SYSTEMD_SERVICE=1)
endif()

set(EXOSIP_LIBS eXosip2 osip2 osipparser2)

if(CMAKE_BUILD_TYPE MATCHES "Debug")
    set(BUILD_DEBUG true)
    add_compile_definitions(DEBUG=1)
else()
    add_compile_definitions(NDEBUG=1)
    pkg_search_module(MALLOC jemalloc IMPORTED_TARGET)
endif()

