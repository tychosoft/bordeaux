# Copyright (C) 2020 Tycho Softworks.
#
# This file is free software; as a special exception the author gives
# unlimited permission to copy and/or distribute it, with or without
# modifications, as long as this notice is preserved.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

# various compile-time tunable parameters and their defaults
if(NOT DEFINED TUNE_COMPILE_PAGER)
    set(TUNE_COMPILE_PAGER 4096)
endif()

if(NOT DEFINED TUNE_SYMBOLS_PAGER)
    set(TUNE_SYMBOLS_PAGER 4096)
endif()

if(NOT DEFINED TUNE_SYMBOLS_INDEX)
    set(TUNE_SYMBOLS_INDEX 123)
endif()

if(NOT DEFINED TUNE_SYMBOL_SIZE)
    set(TUNE_SYMBOL_SIZE 64)
endif()

if(NOT DEFINED_TUNE_MAX_DIGITS)
    set(TUNE_MAX_DIGITS 32)
endif()

if(NOT DEFINED TUNE_MAX_LEVELS)
    set(TUNE_MAX_LEVELS 20)
endif()

if(NOT DEFINED TUNE_STEP_RATE)
    set(TUNE_STEP_RATE 50)
endif()

if(NOT DEFINED TUNE_STEP_COUNT)
    set(TUNE_STEP_COUNT 5)
endif()

if(NOT DEFINED FLITE_LANGUAGE)
    set(FLITE_LANGUAGE usenglish)
endif()

if(NOT DEFINED FLITE_VOICE)
    set(FLITE_VOICE cmu_us_slt)
endif()
