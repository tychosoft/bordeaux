# Copyright (C) 2020 Tycho Softworks.
#
# This file is free software; as a special exception the author gives
# unlimited permission to copy and/or distribute it, with or without
# modifications, as long as this notice is preserved.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

set(SERVER_ID "bordeaux")
if(CMAKE_BUILD_TYPE MATCHES "Debug")
    set(SERVER_CONTROL_IPC "${CMAKE_CURRENT_SOURCE_DIR}/test/${SERVER_ID}.ipc")
    set(SERVER_PREFIX_PATH "${CMAKE_CURRENT_SOURCE_DIR}/test")
    set(SERVER_LOGGER_PATH "${CMAKE_CURRENT_SOURCE_DIR}/test")
    set(SERVER_CONFIG_FILE "${CMAKE_CURRENT_SOURCE_DIR}/test/${SERVER_ID}.conf")
    set(BORDEAUX_VOICES_PATH "${CMAKE_CURRENT_SOURCE_DIR}/voices")
    set(BORDEAUX_PROMPTS_PATH "${CMAKE_CURRENT_SOURCE_DIR}/test")
    set(BORDEAUX_SCRIPTS_PATH "${CMAKE_CURRENT_SOURCE_DIR}/test")
    set(BORDEAUX_MODULES_PATH "${CMAKE_CURRENT_SOURCE_DIR}/test")
    set(BORDEAUX_DRIVERS_PATH "${CMAKE_CURRENT_BINARY_DIR}/drivers")
    set(BORDEAUX_PLUGINS_PATH "${CMAKE_CURRENT_BINARY_DIR}/plugins")
    set(BORDEAUX_DEFAULT_DRIVER "test")
    set(BORDEAUX_IPCPATH "/bordeaux-$ENV{USER}")
else()
    if(NOT DEFINED SERVER_PREFIX_PATH)
        set(SERVER_PREFIX_PATH "${CMAKE_INSTALL_FULL_LOCALSTATEDIR}/lib/${SERVER_ID}")
    endif()
    if(NOT DEFINED SERVER_CONFIG_FILE)
        set(SERVER_CONFIG_FILE "${CMAKE_INSTALL_FULL_SYSCONFDIR}/${SERVER_ID}.conf")
    endif()
    if(NOT DEFINED SERVER_CONTROL_IPC)
        set(SERVER_CONTROL_IPC "${CMAKE_INSTALL_FULL_LOCALSTATEDIR}/run/${SERVER_ID}.ipc")
    endif()
    if(NOT DEFINED SERVER_LOGGER_PATH)
        set(SERVER_LOGGER_PATH "${CMAKE_INSTALL_FULL_LOCALSTATEDIR}/log")
    endif()
    if(NOT DEFINED BORDEAUX_VOICES_PATH)
        set(BORDEAUX_VOICES_PATH "${CMAKE_INSTALL_FULL_DATADIR}/${SERVER_ID}/voices")
    endif()
    if(NOT DEFINED BORDEAUX_PROMPTS_PATH)
        set(BORDEAUX_PROMPTS_PATH "${CMAKE_INSTALL_FULL_LOCALSTATEDIR}/lib/${SERVER_ID}")
    endif()
    if(NOT DEFINED BORDEAUX_SCRIPTS_PATH)
        set(BORDEAUX_SCRIPTS_PATH "${CMAKE_INSTALL_FULL_DATADIR}/${SERVER_ID}/scripts")
    endif()
    if(NOT DEFINED BORDEAUX_MODULES_PATH)
        set(BORDEAUX_MODULES_PATH "${CMAKE_INSTALL_FULL_DATADIR}/${SERVER_ID}/modules")
    endif()
    if(NOT DEFINED BORDEAUX_DRIVERS_PATH)
        set(BORDEAUX_DRIVERS_PATH "${CMAKE_INSTALL_FULL_LIBDIR}/${SERVER_ID}")
    endif()
    if(NOT DEFINED BORDEAUX_PLUGINS_PATH)
        set(BORDEAUX_PLUGINS_PATH "${CMAKE_INSTALL_FULL_LIBDIR}/${SERVER_ID}")
    endif()

    set(BORDEAUX_DEFAULT_DRIVER "generic")
    set(BORDEAUX_IPCPATH "/bordeaux-system")
endif()
