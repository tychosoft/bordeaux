/*
 * Copyright (C) 2020 Tycho Softworks.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RUNTIME_HPP_
#define RUNTIME_HPP_

#include "sessions.hpp"

namespace server {
struct methods final : public Session {
    auto scr_pause() noexcept -> bool;
    auto scr_exit() noexcept -> bool;
    auto scr_clear() noexcept -> bool;
    auto scr_data() noexcept -> bool;
    auto scr_read() noexcept -> bool;
    auto scr_rand() noexcept -> bool;
    auto scr_reset() noexcept -> bool;
    auto scr_var() noexcept -> bool;
    auto scr_set() noexcept -> bool;
    auto scr_do() noexcept -> bool;
    auto scr_tone() noexcept -> bool;
    auto scr_play() noexcept -> bool;
    auto scr_record() noexcept -> bool;
    auto scr_append() noexcept -> bool;
    auto scr_erase() noexcept -> bool;
    auto scr_info() noexcept -> bool;
    auto scr_copy() noexcept -> bool;
    auto scr_move() noexcept -> bool;
    auto scr_speak() noexcept -> bool;
    auto scr_event() noexcept -> bool;
    auto scr_const() noexcept -> bool;
    auto scr_repeat() noexcept -> bool;
    auto scr_break() noexcept -> bool;
    auto scr_end() noexcept -> bool;
    auto scr_debug() noexcept -> bool;
    auto scr_sleep() noexcept -> bool;
    auto scr_until() noexcept -> bool;
    auto scr_input() noexcept -> bool;
    auto scr_collect() noexcept -> bool;
    auto scr_answer() noexcept -> bool;
    auto scr_switch() noexcept -> bool;
    auto scr_case() noexcept -> bool;
    auto scr_otherwise() noexcept -> bool;
    auto scr_keystore() noexcept -> bool;
    auto scr_store() noexcept -> bool;
    auto scr_here() noexcept -> bool;
};
} // end namespace

namespace server::checks {
auto chk_keyword(const script::context_t& ctx) -> const char *;
auto chk_event(const script::context_t& ctx) -> const char *;
auto chk_do(const script::context_t& ctx) -> const char *;
auto chk_data(const script::context_t& ctx) -> const char *;
auto chk_key(const script::context_t& ctx) -> const char *;
auto chk_const(const script::context_t& ctx) -> const char *;
auto chk_var(const script::context_t& ctx) -> const char *;
auto chk_set(const script::context_t& ctx) -> const char *;
auto chk_tone(const script::context_t& ctx) -> const char *;
auto chk_play(const script::context_t& ctx) -> const char *;
auto chk_info(const script::context_t& ctx) -> const char *;
auto chk_copy(const script::context_t& ctx) -> const char *;
auto chk_move(const script::context_t& ctx) -> const char *;
auto chk_erase(const script::context_t& ctx) -> const char *;
auto chk_record(const script::context_t& ctx) -> const char *;
auto chk_append(const script::context_t& ctx) -> const char *;
auto chk_keystore(const script::context_t& ctx) -> const char *;
auto chk_store(const script::context_t& ctx) -> const char *;
auto chk_debug(const script::context_t& ctx) -> const char *;
auto chk_speak(const script::context_t& ctx) -> const char *;
auto chk_sleep(const script::context_t& ctx) -> const char *;
auto chk_clear(const script::context_t& ctx) -> const char *;
auto chk_collect(const script::context_t& ctx) -> const char *;
auto chk_conditional(const script::context_t& ctx) -> const char *;
auto chk_case(const script::context_t& ctx) -> const char *;
auto chk_otherwise(const script::context_t& ctx) -> const char *;
auto chk_here(const script::context_t& ctx) -> const char *;
} // end namespace
#endif
