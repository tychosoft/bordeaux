/*
 * Copyright (C) 2020 Tycho Softworks.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SERVER_HPP_
#define SERVER_HPP_

#ifdef __clang__
#pragma clang diagnostic ignored "-Wpadded"
#pragma clang diagnostic ignored "-Wswitch-enum"
#pragma clang diagnostic ignored "-Wnested-anon-types"
#pragma clang diagnostic ignored "-Wgnu-anonymous-struct"
#pragma clang diagnostic ignored "-Wmissing-field-initializers"
#pragma clang diagnostic ignored "-Wweak-vtables"
#pragma clang diagnostic ignored "-Wunused-member-function"
#endif

#ifdef __GNUC__
#pragma GCC diagnostic ignored "-Wunused-result"
#pragma GCC diagnostic ignored "-Wzero-as-null-pointer-constant"
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"
#endif

#if(__cplusplus < 201703L)
#error C++17 compliant compiler required
#endif

#include <moderncli/templates.hpp>
#include <moderncli/print.hpp>

#include <cassert>

#define NO_EXPORT __attribute__ ((visibility("hidden")))    // NOLINT

using namespace tycho;

// Linting defines
#ifndef PROJECT_VERSION
#define PROJECT_VERSION "unknown"                           // NOLINT
#define PROJECT_SOURCE_DIR "."                              // NOLINT
#define SERVER_ID "bordeaux"                                // NOLINT
#define SERVER_CONTROL_IPC "/bordeaux-system"               // NOLINT
#define SERVER_CONFIG_FILE "/etc/bordeaux.conf"             // NOLINT
#define SERVER_LOGGER_PATH "/var/log"                       // NOLINT
#define SERVER_PREFIX_PATH "/var/lib/bordeaux"              // NOLINT
#define BORDEAUX_VOICES_PATH "/usr/share/bordeaux/voices"   // NOLINT
#define BORDEAUX_PROMPTS_PATH "/usr/share/bordeaux/prompts" // NOLINT
#define BORDEAUX_SCRIPTS_PATH "/usr/share/bordeaux/scripts" // NOLINT
#define BORDEAUX_MODULES_PATH "/usr/lib/bordeaux"           // NOLINT
#define BORDEAUX_DRIVERS_PATH "/usr/lib/bordeaux"           // NOLINT
#define BORDEAUX_PLUGINS_PATH "/usr/lib/bordeaux"           // NOLINT
#define BORDEAUX_DEFAULT_DRIVER "testing"                   // NOLINT
#define BORDEAUX_IPCPATH "/bordeaux-system"                 // NOLINT
#define BORDEAUX_VOICE_MODULES "/usr/share/bordeaux/voices" // NOLINT
#define TUNE_COMPILE_PAGER  4096                            // NOLINT
#define TUNE_SYMBOLS_PAGER  4096                            // NOLINT
#define TUNE_SYMBOLS_INDEX  123                             // NOLINT
#define TUNE_SYMBOL_SIZE   64                               // NOLINT
#define TUNE_MAX_DIGITS     32                              // NOLINT
#define TUNE_MAX_LEVELS     20                              // NOLINT
#define TUNE_STEP_RATE      50                              // NOLINT
#define TUNE_STEP_COUNT     5                               // NOLINT
#endif

#endif
