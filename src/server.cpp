/*
 * Copyright (C) 2020 Tycho Softworks.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "server.hpp"       // IWYU pragma: keep
#include "utils.hpp"
#include "ipc.hpp"
#include "drivers.hpp"
#include "sessions.hpp"

#include <moderncli/args.hpp>
#include <moderncli/keyfile.hpp>
#include <moderncli/socket.hpp>
#include <moderncli/filesystem.hpp>
#include <moderncli/process.hpp>
#include <moderncli/datetime.hpp>

#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>

#ifdef  SYSTEMD_SERVICE
#include <systemd/sd-daemon.h>
#else
inline auto sd_notify(int, const char *) {  // NOLINT
    return 0;
}
#endif

using namespace server;

std::atomic<bool> server::running = false;  // need not be atomic...but...
system_logger server::logger;
bool server::ipv6 = false;

namespace {
auto driver_name = BORDEAUX_DEFAULT_DRIVER;
fsys::path config_file = SERVER_CONFIG_FILE;
fsys::path prefix_path = SERVER_PREFIX_PATH;
std::ofstream logfile;
std::mutex loglock;
Socket ipc;

args::option opt_ipv6('6',  "--ipv6", "enable ipv6");
args::option opt_config(0,  "--config", "configuration file", "file");
args::option opt_driver('d',"--driver", "driver name to load", "path");
args::option helpflag('h',  "--help", "display this list");
args::option paths(0,       "--paths", "display paths used");
args::option opt_prefix(0,  "--prefix", "prefix directory", "path");
args::option verbose('v',   "--verbose", "verbosity, repeat for debug", true);
args::option version(0,     "--version", "display version information");
args::option althelp('?',   nullptr, nullptr);
unsigned logging{};

auto is_root() {
    return getuid() == 0 || getgid() == 0;
}

void log_reset() {
    const std::lock_guard<std::mutex> lock(loglock);
    if(logfile.is_open())
        logfile.close();
    logfile.open(SERVER_LOGGER_PATH "/bordeaux.log", std::ios_base::app); // FlawFinder: ignore
}

void logger_notify(const std::string& msg, const char *type) {
    if(eq(type, "debug") || !logfile.is_open()) return;
    auto timestamp = system_clock();
    const struct tm now = local_time(timestamp);
    const std::lock_guard<std::mutex> lock(loglock);
    logfile << std::put_time(&now, ISO_DATETIME) << " " << msg << std::endl;
}

auto reload() {
    auto config = keyfile::create()
        .load("consts", {
        })
        .load("paths", {
            {"scripts", BORDEAUX_SCRIPTS_PATH},
            {"voices", BORDEAUX_VOICES_PATH},
            {"prompts", BORDEAUX_PROMPTS_PATH},
            {"modules", BORDEAUX_MODULES_PATH},
            {"prefix", SERVER_PREFIX_PATH},
            {"logger", SERVER_LOGGER_PATH},
        })
        .load("server", {
            {"hostname", system_hostname()},
            {"media", system_hostname()},
            {"limit", "20"},
        });

    config.load(config_file);
    config.load("custom.conf");

    auto& language = config["server"]["language"];
    auto& location = config["server"]["location"];
    auto& voice = config["server"]["voice"];
    auto& ring_timer = config["timers"]["ring"];
    auto& answer_timer = config["timers"]["answer"];
    auto family = lower_case(config["server"]["family"]);
    if(family == "af_inet6" || family == "inet6" || family == "ipv6" || family == "6")
        ipv6 = true;

    if(language.empty()) {
        auto env = process::env("BORDEAUX_LANGUAGE", 16);
        if(!env || (*env).empty())
            env = process::env("LANGUAGE", 16);

        if(!env || (*env).empty())
            language = "en_US";
        else
            language = *env;
    }

    if(location.empty()) {
        auto env = process::env("BORDEAUX_LOCATION", 64);
        if(!env || (*env).empty())
            env = process::env("LOCATION", 64);

        if(env)
            location = *env;
        else
            location = "unknown";
    }

    if(voice.empty()) {
        auto env = process::env("BORDEAUX_VOICE", 64);
        if(!env || (*env).empty())
            env = process::env("VOICE", 64);

        if(!env || (*env).empty())
            env = "default";
        else
            voice = *env;
    }

    process::env("LANGUAGE", language);
    process::env("LOCATION", location);
    process::env("VOICE", voice);

    if(ring_timer.empty())
        ring_timer = "4";

    if(answer_timer.empty())
        answer_timer = "30";

    // Support for testability... will be extended over time
    if(!is_root() && is(opt_prefix) && fsys::exists(prefix_path / "bordeaux.conf"s)) {
        config["paths"]["scripts"] = prefix_path;
        config["paths"]["modules"] = prefix_path;
        config["paths"]["voices"] = fsys::canonical(prefix_path / ".." / "voices");
        config["paths"]["prompts"] = prefix_path / "prompts";
        config["paths"]["prefix"] = prefix_path;
        config["paths"]["logger"] = prefix_path;
    }

    Plugin::config_all(config);
    return Driver::config_instance(config, logging);
}

auto stop() {
    ipc.release();
    sd_notify(1, "STOPPING=1");
    running = false;
    std::this_thread::sleep_for(std::chrono::milliseconds(50));

    Driver::drop_instance();
    Plugin::stop_all();
    Session::stop_all();
    Driver::stop_instance();
    fsys::remove(SERVER_CONTROL_IPC);
}

void control() {
    ipc_event event{};
    fsys::remove(SERVER_CONTROL_IPC);
    ipc.bind(address_t(SERVER_CONTROL_IPC), SOCK_DGRAM);
    if(ipc)
        logger.info("ipc control attached {}", SERVER_CONTROL_IPC);
    else {
        logger.error("ipc control failed; err={}", ipc.err());
        return;
    }

    for(;;) {
        if(!recv(ipc, event)) {
            logger.info("ipc control exiting; reason={}", ipc.err());
            return;
        }
        switch(event.type) {
        case IPC_STATUS:
            break;
        case IPC_LEVEL:
            logging = event.body.level;
            logger.notice("logging level changed to {}", logging);
            break;
        case IPC_RELOAD:
            logger.debug(3, "Reload from ipc");
            kill(getpid(), SIGHUP);
            break;
        default:
            logger.debug(4, "received unknown message {}", static_cast<int>(event.type));
        }
    }
}

auto loader(const std::string& prefix, const std::string& name, const std::string& type) {
    auto module(type);
    module += name;
    module += process::dso_suffix;
    auto dsopath = prefix + "/" + module;
    if(name[0] == '/' && !is_root())
        dsopath = name + process::dso_suffix;
    auto ptr = process::load(dsopath);

    if(ptr)
        logger.info("loading {}", dsopath);
    else
        logger.error("{}: failed to load", name);
    return ptr != nullptr;
}

void cleanup() {
    running = false;
    shm_unlink(BORDEAUX_IPCPATH);
}
} // anon namespace

auto main(int argc, const char **argv) -> int { // NOLINT
    auto exit_code = 0;
    log_reset();
    on_crit(cleanup);

    try {
        auto count = args::parse(argc, argv);
        if(is(helpflag) || is(althelp) || count) {
            args::help({SERVER_ID " [options]"}, {
                SERVER_ID " " PROJECT_VERSION,
                "David Sugar <tychosoft@gmail.com>",
                "Start Bordeaux Service"
            });
            return 0;
        }

        if(is(opt_config))
            config_file = *opt_config;

        if(is(opt_prefix))
            prefix_path = fsys::canonical(*opt_prefix);

        if(is(version)) {
            print("Version: {}\n", PROJECT_VERSION);
            return 0;
        }

        if(is(paths)) {
            print("Config:  {}\nPrefix:  {}\nDrivers: {}\nPlugins: {}\nModules: {}\nScripts: {}\nPrompts: {}\nVoices:  {}\n",
                config_file, std::string{prefix_path.u8string()},
                BORDEAUX_DRIVERS_PATH, BORDEAUX_PLUGINS_PATH,
                BORDEAUX_MODULES_PATH, BORDEAUX_SCRIPTS_PATH,
                BORDEAUX_PROMPTS_PATH, BORDEAUX_VOICES_PATH);
            return 0;
        }

        // set miscellaneous configs
        if(is(opt_ipv6))
            ipv6 = true;

        if(is(opt_driver))
            driver_name = *opt_driver;

        umask(007);    // FlawFinder: group access even as root...

        // create default prompt workspaces, clean old session recordings....
        fsys::current_path(prefix_path);
        process::env("PWD", prefix_path);
        fsys::remove_all("temp");
        if(fsys::create_directory("prompts"))
            fsys::permissions("prompts", fsys::perms::owner_all | fsys::perms::group_read | fsys::perms::group_write | fsys::perms::group_exec);
        if(fsys::create_directory("temp"))
            fsys::permissions("temp", fsys::perms::owner_all);

        // initial logging
        logging = verbose.count();
        logger.set(verbose.count(), logger_notify);
        system_logger::open(SERVER_ID, LOG_INFO);   // FlawFinder: ignore

        // handlers for new and exit
        std::set_new_handler([]() {
            crit(9, "error: out of memory\n");
        });

        // signal masks, we could add SIGUSR1..2 for special things
        sigset_t sigs;
        sigemptyset(&sigs);
        sigaddset(&sigs, SIGALRM);
        sigaddset(&sigs, SIGINT);
        sigaddset(&sigs, SIGHUP);
        sigaddset(&sigs, SIGKILL);
        sigaddset(&sigs, SIGCHLD);
        sigaddset(&sigs, SIGTERM);
        pthread_sigmask(SIG_BLOCK, &sigs, nullptr);

        // bind driver
        auto driver_path = fsys::path(BORDEAUX_DRIVERS_PATH);
        auto dp = driver_name;
        if(!is_root() && strchr(driver_name, '/') && driver_name[0] != '/') {
            driver_path = fsys::canonical(prefix_path / ".." / fsys::path(driver_name).parent_path());
            dp = strrchr(driver_name, '/');
            if(dp) {
                ++dp;
                auto cp = strchr(driver_name, '_');
                if(cp)
                    dp = ++cp;
            }
            else
                dp = driver_name;
        }
        if(!loader(driver_path, dp, "driver_"))
            crit(-1, "error: driver {} failed: {}\n", driver_name, dlerror());

        // load plugins if not absolute driver for build testing
        auto plugins_path = fsys::path(BORDEAUX_PLUGINS_PATH);
        if(!is_root() && strchr(driver_name, '/')) {
            if(driver_name[0] == '/')
                plugins_path = fsys::path(driver_name).parent_path();
            else
                plugins_path = prefix_path / ".." / fsys::path(driver_name).parent_path();
            if(fsys::is_directory(plugins_path / "../plugins"))
                plugins_path = fsys::canonical(plugins_path / "../plugins");
            else
                plugins_path = fsys::canonical(plugins_path / "../../plugins" / plugins_path.filename());
        }

        if(fsys::is_directory(plugins_path))
            scan_directory(plugins_path, [plugins_path](const auto& entry) {
                if(!entry.is_regular_file()) return false;
                auto filename = entry.path().filename().string();
                if(begins_with(filename, "plugin_") && ends_with(filename, ".so")) {
                    filename.erase(filename.size() - 3, 3);
                    filename.erase(0, 7);
                    loader(plugins_path, filename, "plugin_");
                    return true;
                }
                return false;
            });

        // start running service
        auto config = reload();
        running = true;

        Driver::start_instance(config);
        Plugin::start_all(config);

        // create ipc info
        fsys::remove("ipc.json");
        std::ofstream ipc("ipc.json");
        ipc << R"({"ipc_path":")" << BORDEAUX_IPCPATH << R"(",)" << std::endl
            << R"( "control_path":")" << SERVER_CONTROL_IPC << R"(",)" << std::endl
            << R"( "event_size":)" << sizeof(ipc_event) << "," << std::endl
            << R"( "system_size":)" << sizeof(ipc_system) << "," << std::endl
            << R"( "session_size":)" << sizeof(ipc_session) << "," << std::endl
            << R"( "session_count":)" << Driver::instance()->limit() << std::endl
            << "}" << std::endl;
        ipc.close();

        // create timers and other threads
        std::thread session_timers(&Session::start, 2);
        std::thread control_events(control);
        std::this_thread::sleep_for(std::chrono::milliseconds(50));

        // handle signals while up
        auto timer = sync_clock() + std::chrono::seconds(60);
        if(!eq(driver_name, "test"))
            alarm(60);

        logger.info("started server");
        sd_notify(0, "READY=1");
        while(running) {
            int sig{};
            if(sigwait(&sigs, &sig))
                crit(-1, "Broken sigwait");

            switch(sig) {
            case SIGCHLD:
                for(;;) {
                    auto status = 0;
                    auto pid = waitpid(-1, &status, WNOHANG);
                    if(pid == -1) break;
                }
                break;
            case SIGINT:
                std::cout << '\n';
                stop();
                break;
            case SIGALRM:
                // abort if testing..
                if(eq(driver_name, "test")) {
                    exit_code = 5;
                    stop();
                }
                // sync alarm every 60 seconds, used for stats...
                sd_notify(0, "WATCHDOG=1");
                logger.debug(3, "minute timer wakeup");
                timer += std::chrono::seconds(60);
                alarm(std::chrono::duration_cast<std::chrono::seconds>(timer - sync_clock()).count());
                break;
            case SIGTERM:
            case SIGQUIT:
                stop();
                break;
            case SIGHUP:
                sd_notify(0, "RELOADING=1");
                log_reset();
                logger.info("reloading server");
                Session::sync_store();
                try {
                    config = reload();
                    Plugin::reload_all(config);
                    Driver::reload_instance(config);
                }
                catch(const std::exception& e) {
                    logger.error("reload: ", e.what());
                }
                catch(...) {
                    logger.error("reload: unknown error");
                }
                sd_notify(0, "READY=1");
                break;
            default:
                break;
            }
        }

        // normal tear-down and exit
        session_timers.join();
        control_events.join();
    }
    catch(const bad_arg& e) {
        crit(-1, "Usage: {} [options]\nerror: {}\n", SERVER_ID, e.what());
    }
    catch(const std::exception& e) {
        if(running)
            logger.crit(-1, "{}", e.what());
        else
            crit(-1, "error: {}\n", e.what());
    }
    catch(...) {
        if(running)
            logger.crit(-1, "unknown failure");
        else
            crit(-1, "error: unknown failure\n");
    }

    logger.info("stopped server; reason={}", exit_code);
    cleanup();
    return exit_code;
}
