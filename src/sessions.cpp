#include "sessions.hpp"
/*
 * Copyright (C) 2020 Tycho Softworks.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "drivers.hpp"
#include "utils.hpp"

#include <moderncli/keyfile.hpp>
#include <moderncli/random.hpp>
#include <moderncli/filesystem.hpp>
#include <moderncli/process.hpp>
#include <moderncli/datetime.hpp>
#include <moderncli/scan.hpp>

#include <list>

using namespace server;

namespace {
std::atomic<sid_t> next_sid{1};
Session::runlist_t runlist;
std::mutex runlock;
std::condition_variable cvar;
unique_sync<keyfile> storage;
std::atomic<bool> key_updates{false};
std::random_device rd;
std::mt19937 rand_gen(rd());

using close_t = struct handles {
    audiofile::handle_t handle;
    sync_timepoint expires;

    handles(audiofile::handle_t file, sync_timepoint when) :
    handle(file), expires(when) {}
};
std::list<close_t> closing;

const Session::event_t entry_event{event_type::entry};
const Session::event_t leave_event{event_type::leave};
const Session::event_t timeout_event{event_type::timeout};
const Session::event_t exit_event{event_type::exit};

} // end anon namespace

Session::Session(const script& attach) noexcept :
language(nullptr), handler(&Session::dummy_handler), expires(started), image(attach), shm(nullptr), index(0), level(0), run_(runlist.cend()) {
    auto seq = next_sid.fetch_add(1, std::memory_order_relaxed);
    uint8_t sid64[6];

    crypto::rand(sid64, 4);
    sid64[4] = seq & 0xff;
    sid64[5] = seq & 0xff00 >> 8;

    var_lang = syms.make_storage("LANGUAGE", 16, "en_US");
    var_voice = syms.make_storage("VOICE", 64, "default");
    var_location = syms.make_storage("LOCATION", 64, "unknown");
    var_mailbox = syms.make_storage("MAILBOX", 32, "none"); // NOLINT

    auto env = process::env("LANGUAGE", 16);
    if(env)
        syms.set_value("LANGUAGE", (*env).c_str());

    env = process::env("VOICE", 64);
    if(env)
        syms.set_value("VOICE", (*env).c_str());

    env = process::env("LOCATION", 64);
    if(env)
        syms.set_value("LOCATION", (*env).c_str());

    set_lang(syms.get_value("LANGUAGE"));
    syms.make_storage("result_code", 7);
    syms.set_value("result_code", "0");

    auto iso = to_string(local_time(time(nullptr)), ISO_DATETIME);
    syms.make_const({
        {"SESSION_ID", crypto::to_b64(sid64, sizeof(sid64))},
        {"SCRIPT_NAME", attach.name()},
        {"SESSION_DATE", iso.substr(0, 10)},
        {"SESSION_TIME", iso.substr(11, 8)},
    });

    app_ = syms.get_value("SCRIPT_NAME");   // NOLINT
    sid_ = syms.get_value("SESSION_ID");    // NOLINT

    const auto& config_paths = attach.config().at("paths");
    sys_audio.app = app_;
    sys_audio.lang = var_lang;
    sys_audio.mailbox = var_mailbox;
    sys_audio.voice = var_voice;
    sys_audio.prefix = config_paths.at("voices").c_str();
    sys_audio.prompts = config_paths.at("prompts").c_str();
    sys_audio.record = "prompts";
}

Session::~Session() {
    // clean up any session recording
    audio_release();
    auto filename = "temp/"s + sid_ + ".au";
    fsys::remove(filename);
}

void Session::begin(const session_t& s, const char *name) {
    s->begin(name);
    send(s, entry_event);
}

void Session::begin(const char *name) {
    error_ = syms.make_storage("error", symbols::var_size);
    timed_ = syms.make_storage("timed", 18);    // timed media
    notes_ = syms.make_storage("notes", 128);   // annotations
    digits = syms.make_storage("digits", max_digits);
    index = level = 0;
    items = nullptr;

    syms.set_value("error", "none");
    syms.set_value("timed", "0");
    syms.set_value("notes", "-");

    // run init segment...
    header = image.init();
    line = header->first;
    data = reset();
    while(line)
        execute_method(line->method);

    label(name);
    set_timeout(step_rate);
    set_handler("step", &Session::script_handler);
}

auto Session::set_handler(const char *text, handler_t new_handler) -> bool {
    if(handler == new_handler) return false;
    process_event(leave_event);
    handler = new_handler;
    mem_copy(shm->status, sizeof(shm->status), text);
#ifdef  DEBUG
    logger.debug(4, "sid({}): state={}", sid_, text);
#endif
    silence();
    return process_event(entry_event);
}

void Session::stream() {
    shm->state = ipc_session::ISC_CONNECTED;
    connected = true;
}

auto Session::play_tone() -> bool {
    return error("no tonegen");
}

auto Session::play_files() -> bool {
    return error("no audio");
}

auto Session::record_file() -> bool {
    return error("no audio");
}

void Session::silence(bool shutdown) {  // NOLINT
    audio_release();
    media = false;
}

void Session::disconnect() {
    audio_release();
}

void Session::clear_timeout() {
    expires = started;
}

void Session::audio_release(long offset) {
    auto handle = playrec.detach();
    if(handle == audiofile::invalid) return;
    const std::lock_guard<std::mutex> lock(runlock);
    closing.emplace_back(handle, sync_clock(offset));
}

void Session::set_timed(long value, std::string_view note) {
    if(value)
        snprintf(timed_, 18, "%ld", value / 1000L);

    if(!note.empty())
        str_copy(notes_, 128, note);
}

void Session::set_timeout(const sync_timepoint& until) {
    expires = until;
}

void Session::set_timeout(long offset) {
    expires = sync_clock(offset);
}

void Session::bump_timeout(long offset) {
    expires += std::chrono::milliseconds(offset);
}

void Session::sync_store() {
    auto flag = key_updates.exchange(false);
    if(!flag) return;
    guard_ptr keys(storage);
    keys->write("stored.keys");
    key_updates = false;
}

void Session::set_lang(const char* from) {
    if(!from || *from == 0)
        str_copy(var_lang, 16, "en_US");
    else
        str_copy(var_lang, 16, from);

    char *cp = strchr(var_lang, '.');
    if(cp && *cp == '.')
        *cp = 0;

    language = Plugin::find_locale(var_lang);
    cp = var_lang;
    while(*cp) {
        *cp = static_cast<char>(tolower(*cp));
        ++cp;
    }

    auto path = image.config().at("paths").at("voices") + "/";
    path += from;
    cp = strchr(var_lang, '_');
    if(fsys::is_directory(path) && cp  && *cp == '_')
        *cp = '-';
    else if(cp && *cp == '_')
        *cp = 0;
}

void Session::get_store(const char *id) {
    guard_ptr store(storage);
    const auto& keys = store->keyset(id);
    const auto& common = store->keyset();

    // load common default keys if any
    for(const auto& [key, val] : common) {
        snprintf(temp, sizeof(temp), "stored_%s", key.c_str());
        syms.set_value(temp, val.c_str());
    }

    // load target storage keys
    for(const auto& [key, val] : keys) {
        snprintf(temp, sizeof(temp), "stored_%s", key.c_str());
        syms.set_value(temp, val.c_str());
    }
}

void Session::put_store(const char *id) {
    guard_ptr store(storage);
    auto& keys = store->keyset(id);
    auto key{""};
    while(nullptr != (key = symbol())) {
        if(!strncmp(key, "stored_", 7))
            key += 7;
        snprintf(temp, sizeof(temp), "stored_%s", key);
        auto val = syms.get_value(temp);
        keys.insert_or_assign(key, val);
    }
    key_updates = true;
}

void Session::release(const session_t& s) {
    if(s->expires == s->started && s->run_ == runlist.cend()) return;
    std::unique_lock<std::mutex> lock(runlock);
    s->clear_timeout();
    if(s->run_ != runlist.cend())
        runlist.erase(s->run_);
    s->run_ = runlist.cend();

    lock.unlock();
    cvar.notify_one();
}

void Session::start(int priority) {
    sync_ptr store(storage);
    store->load("stored.keys");
    store.unlock();
    if(!this_thread::priority(priority))
        priority = 0;

    logger.info("starting session timers; pri={}", priority);
    while(running) {
        auto now = sync_clock();
        std::unique_lock<std::mutex> lock(runlock);
        closing.remove_if([&now](close_t closer) {
            if(now >= closer.expires) {
                ::close(closer.handle);
                return true;
            }
            return false;
        });
        if(runlist.empty()) {
#ifdef  DEBUG
            logger.debug(5, "timers idle");
#endif
            cvar.wait_until(lock, sync_clock(1000));
        }
        else {
#ifdef  DEBUG
            logger.debug(5, "timer triggered; runlist={}", runlist.size());
#endif
            auto s(*runlist.begin());
            auto expires = s->expires;
            if(now < expires) {
                // never more than a second...
                if(expires > sync_clock(1000))
                    expires = sync_clock(1000);
                cvar.wait_until(lock, expires);
            }
        }
        if(!running) break;
        now = sync_clock();
        while(!runlist.empty()) {
            // get earliest event remaining in list
            auto it = runlist.begin();
            auto s(*it);
            auto first = s->expires;

            // skip all future events after present...
            if(now < first) break;
            // timeout and remove this event
            runlist.erase(it);
            s->run_ = runlist.cend();
            lock.unlock();
            Session::send(s, timeout_event);
            lock.lock();
        }
    }
    logger.info("stopping session timers");
    if(key_updates) {
        store.lock();
        store->write("stored.keys");
    }
}

void Session::stop_all() {
    cvar.notify_all();
    std::this_thread::sleep_for(std::chrono::milliseconds(20));
    const std::lock_guard<std::mutex> lock(runlock);
    runlist.clear();
}

auto Session::send(const session_t& s, const event_t &event) -> bool {
    std::unique_lock<std::mutex> session_lock(s->access_lock);
    auto result = s->process_event(event);
    if(s->started == s->expires && s->run_ == runlist.cend()) return result;

    session_lock.unlock();
    const std::unique_lock<std::mutex> runlist_lock(runlock);

    if(s->run_ != runlist.cend())
        runlist.erase(s->run_);

    if(s->started != s->expires) {
        auto [it, flag] = runlist.insert(s);
        s->run_ = it;
    }
    else
        s->run_ = runlist.cend();

    cvar.notify_one();
    return result;
}

auto Session::common_handler(const event_t &event) -> bool {
    std::size_t count = 0;
    switch(event.type) {
    case event_type::err:
        if(media)
            silence();
        set_error(event.reason);
        if(!Session::event("error"))
            skip();
        return this_step();
    case event_type::end:
        if(!media) return false;
        silence();
        if(!Session::event("media"))
            skip();
        return this_step();
    case event_type::drop:
        if(exiting) return true;    // can ignore call drops entirely if already exiting
    case event_type::exit:
        silence(true);
        shm->state = ipc_session::ISC_CLOSING;
        exiting = true;
        if(!trap && Session::event("exit")) return true;
        if(!eq(header->id, "exit") && label("exit")) return this_step();
        set_handler("down", &Session::shutdown_handler);
        return false;
    case event_type::answer:
        stream();
        return true;
    case event_type::timeout:
        if(media)
            silence();
        if(!Session::event("timeout"))
            skip();
        return this_step();
    case event_type::flash:
        if(Session::event("flash") || label("flash")) {
            if(media)
                silence();
            return this_step();
        }
        return true;
    case event_type::digit:
        if(Session::event(&event.digit[0]) || Session::event("digits")) {
            if(media)
                silence();
            digits[0] = event.digit[0];
            digits[1] = 0;
            return this_step();
        }

        // trap handlers buffer for collect...
        count = mem_size(digits);
        if(count >= max_digits) {
            set_error("overflow");
            if(Session::event("overflow")) {
                if(media)
                    silence();
                return this_step();
            }
            return true;
        }
        digits[count++] = event.digit[0];
        digits[count] = 0;
        return false;
    case event_type::entry:
    case event_type::leave:
        return true;
    default:
        return false;
    }
}

auto Session::play_handler(const event_t &event) -> bool {
    switch(event.type) {
    case event_type::entry:
        if(state.prompt.timeout)
            set_timeout(state.prompt.timeout);
        else
            clear_timeout();
        return play_files();
    case event_type::timeout:
        if(media)
            silence();
        skip();
        return this_step();
    default:
        return common_handler(event);
    }
}

auto Session::record_handler(const event_t &event) -> bool {
    switch(event.type) {
    case event_type::entry:
        if(state.prompt.timeout)
            set_timeout(state.prompt.timeout);
        else
            clear_timeout();
        return record_file();
    case event_type::timeout:
        if(media)
            silence();
        skip();
        return this_step();
    default:
        return common_handler(event);
    }
}

auto Session::speak_handler(const event_t &event) -> bool {
    switch(event.type) {
    case event_type::entry:
        clear_timeout();
        return play_files();
    case event_type::end:
        if(media)
            silence();
        if(pause) {
            set_timeout(pause);
            return true;
        }
        return common_handler(event);
    case event_type::timeout:
        skip();
        return this_step();
    default:
        return common_handler(event);
    }
}

auto Session::collect_handler(const event_t &event) -> bool {
    auto store = state.collect.digits;
    auto count = mem_size(store);

    switch(event.type) {
    case event_type::digit:
        store[count++] = event.digit[0];
        store[count] = 0;
    case event_type::entry:
        if(count && strchr(state.collect.terminators, store[count - 1])) {
            store[--count] = 0;
            return next_step();
        }
        if(count >= state.collect.max_digits)
            return next_step();
        if(count < state.collect.min_digits)
            set_timeout(state.collect.wait_until);
        else
            set_timeout(state.collect.interdigit);
        return common_handler(event);
    case event_type::leave:
        clear_timeout();
    default:
        return common_handler(event);
    }
}

auto Session::answer_handler(const event_t& event) -> bool {
    switch(event.type) {
    case event_type::timeout:
        return common_handler(exit_event);
    case event_type::answer:
        stream();
        return next_step();
    case event_type::drop:
    case event_type::exit:
        return common_handler(event);
    default:    // ignore most secondary events while waiting for answer
        return true;
    }
}

auto Session::shutdown_handler(const event_t &event) -> bool {
    switch(event.type) {
    case event_type::entry:
        set_timeout(50);    // long enough to idle media...
        return true;
    case event_type::timeout:
        disconnect();
        set_handler("idle", &Session::dummy_handler);
        clear_timeout();
        Driver::release(sid_);
        return false;
    default:
        return false;
    }
}

auto Session::tone_handler(const event_t& event) -> bool {
    switch(event.type) {
    case event_type::entry:
        if(play_tone()) return true;
        if(state.tone.timeout)
            set_timeout(state.tone.timeout);
        else
            clear_timeout();
        return false;
    case event_type::timeout:
        if(media)
            silence();
        return next_step();
    default:
        return common_handler(event);
    }
}

auto Session::sleep_handler(const event_t& event) -> bool {
    switch(event.type) {
    case event_type::timeout:
        return next_step();
    default:
        return common_handler(event);
    }
}

auto Session::script_handler(const event_t& event) -> bool {
    auto count = step_count;

    switch(event.type) {
    case event_type::entry:
    case event_type::timeout:
        if(!line) return common_handler(exit_event);
        set_timeout(step_rate);
        ++shm->steps;
        while(line && count--) {
            ++shm->lines;
            if(!execute_method(line->method)) return false;
        }

        // if we really used up the entire interval, still sleep...
        if(sync_remains(expires) < std::chrono::milliseconds(1))
            set_timeout(step_rate);

        return true;
    default:
        return common_handler(event);
    }
}

auto Session::dummy_handler(const event_t &event) -> bool {
    switch(event.type) {
    case event_type::entry:
        clear_timeout();
        return true;
    default:
        return false;
    }
}

auto Session::argv(unsigned short offset) -> const char * {
    if(offset >= line->argc) return nullptr;
    return line->argv[offset];
}

auto Session::output() -> const char * {
    if(index >= line->argc) return nullptr;
    const char *cp = line->argv[index];
    if(*cp == '&') return ++cp;
    auto prior = index;
    auto older = items;
    cp = value();
    index = prior;
    items = older;

    if(!cp) return "invalid";
    if(!*cp) return "empty";
    auto len = mem_size(cp);
    auto dots = false;
    auto phone = false;

    if(len == 5 && cp[2] == ':' && get_integer_or(cp, -1, 0, 23) != -1 && get_integer_or(cp, -1, 0, 60) != -1) return "time";
    if(strchr(cp, ',')) return "list";
    if(*cp == '-')
        ++cp;
    else if(*cp == '+') {
        phone = true;
        ++cp;
    }

    while(*cp) {
        if(*cp == '.') {
            if(!dots && !phone)
                dots = true;
            else
                break;
        }
        if(!isdigit(*cp) || (phone && isspace(*cp)))
            break;
        ++cp;
    }

    if(!*cp && dots) return "decimal";
    if(!*cp && phone) return "phone";
    if(!*cp) return "number";
    return "text";
}

auto Session::option() -> const char * {
    if(index >= line->argc) return nullptr;
    auto cp = line->argv[index];
    if(*cp == '=') return ++cp;
    return nullptr;
}

auto Session::symbol() -> const char * {
    while(index < line->argc) {
        auto cp = line->argv[index];
        if(*cp == '=') {
            ++index;
            continue;
        }
        if(*cp == '%') {
            ++index;
            return ++cp;
        }
        if(*cp == '$' || *cp == '#') return nullptr;
        ++index;
        return cp;
    }
    return nullptr;
}

auto Session::get_phrase() -> audiofile::handle_t {
    if(!eq(line->cmd, "speak")) return -1;
    char path[128];
    const char *cp{nullptr};
    while(nullptr != (cp = fetch())) {
        if(!audio_paths::playfile(cp, path, sizeof(path), &paths)) continue;
        if(!playrec.play(path)) continue;
        return playrec.handle();
    }
    return -1;
}

auto Session::funcs(const char *cp) -> const char * {
    auto speak = eq(line->cmd, "speak");
    const char *lp = nullptr;

    if(eq(cp, "&count")) {
        unsigned count = 0;
        const char *s = symbol();
        if(s) {
            snprintf(temp, sizeof(temp), "%u", syms.get_count(s));
            return temp;
        }

        const char *v = arg();
        if(!v) return "invalid";
        cp = v;
        while((cp = strchr(cp, ',')) != nullptr) {
            ++count;
            lp = ++cp;
        }

        if(lp && *lp)
            ++count;

        v = temp;
        if(count)
            mem_value(temp, sizeof(temp), count);
        else
            mem_value(temp, sizeof(temp), mem_size(v));

        if(!speak) return v;
        if(!language || !language->speak(temp2, sizeof(temp2), "&order", v)) return "invalid";
        items = temp2;
        return itemize();
    }

    // nth value for ordering
    if(eq(cp, "&order")) {
        const char *v = arg();
        if(!v || *v == '0') return "invalid";
        cp = v;
        while(*cp && isdigit(*cp))
            ++cp;
        if(*cp) return "invalid";
        if(!speak) return v;
        if(!language || !language->speak(temp, sizeof(temp), "&order", v)) return "invalid";
        items = temp;
        return itemize();
    }

    // simple number...
    if(eq(cp, "&number")) {
        const char *v = arg();
        if(!v) return "invalid";
        cp = v;
        if(*cp == '-')
            ++cp;

        while(*cp && isdigit(*cp))
            ++cp;

        if(*cp) return "invalid";
        if(!speak) return v;
        if(!language || !language->speak(temp, sizeof(temp), "&number", v)) return "invalid";
        items = temp;
        return itemize();
    }

    // decimal number
    if(eq(cp, "&decimal")) {
        const char *v = arg();
        if(!v) return "invalid";
        bool dots = false;
        cp = v;
        if(*cp == '-')
            ++cp;

        while(*cp) {
            if(isdigit(*cp)) {
                ++cp;
                continue;
            }

            if(*cp == '.' && !dots) {
                dots = true;
                ++cp;
                continue;
            }

            return "invalid";
        }

        if(!dots) {
            snprintf(temp, sizeof(temp), "%s.", v);
            v = temp;
        }

        if(!speak) return v;

        if(!language || !language->speak(temp2, sizeof(temp2), "&decimal", v)) return "invalid";

        items = temp2;
        return itemize();
    }

    if(eq(cp, "&spell")) {
        const char *v = arg();
        if(!language || !v || strchr(v, ',')) return "invalid";
        if(!language->speak(temp, sizeof(temp), cp, v)) return "invalid";
        if(!speak) return temp;
        items = temp;
        return itemize();
    }

    if(eq(cp, "&path")) {
        cp = arg();
        if(!cp) return "/nonexists";
        if(!audio_paths::playfile(cp, temp, sizeof(temp), &sys_audio)) return "/nonexists";
        return temp;
    }

    if(eq(cp, "&items")) {
        cp = arg();
        if(!cp) return "invalid";
        items = cp;
        cp = itemize();
        if(!cp) return "invalid";
        return cp;
    }

    if(eq(cp, "&list")) {
        const char *v = arg();
        if(!v) return ",";

        if(!strchr(v, ',')) {                       // single element must be xxx,
            snprintf(temp, sizeof(temp), "%s,", v);
            v = temp;
            if(!strchr(v, ',')) return "invalid";   // element too long...
        }
        return v;
    }

    if(eq(cp, "&time")) {
        const char *v = arg();
        if(!v) return "invalid";
        snprintf(temp, sizeof(temp), "%s", script::str_time24(v).c_str());
        return temp;
    }
    return "invalid";
}

auto Session::itemize() -> const char * {
    if(items) {
        while(*items && !isalnum(*items) && *items != '/' && *items != '.' && *items != '-')
            ++items;
        if(*items) {
            auto offset = 0U;
            while((*items == '.' || *items == '/' || *items == '-' || isalnum(*items)) && offset < (sizeof(item) - 1))
                item[offset++] = *(items++);
            item[offset] = 0;
            return item;
        }
        items = nullptr;
    }
    return nullptr;
}

auto Session::fetch() -> const char * {
    const char *cp{nullptr};
    if(items) {
        cp = itemize();
        if(cp) return cp;
    }

    cp = value();
    if(!cp) return nullptr;
    if(!items && strchr(cp, ',')) {
        mem_copy(temp, sizeof(temp), cp);
        items = temp;
        return itemize();
    }
    return cp;
}

auto Session::value() -> const char * {
    // skip empty items, itemize if from list
    auto cp = itemize();
    if(cp) return cp;

    // parse from argument
    while(index < line->argc) {
        cp = line->argv[index++];
        if(*cp == '=') continue;
        if(*cp == '$' || *cp == '#' || *cp == '@' || *cp == '^') return ++cp;   // literal...
        if(*cp == '%') return syms.get_value(++cp); // symbol...
        if(*cp == '&') return funcs(cp);
        return cp;
    }
    return nullptr;
}

auto Session::arg() -> const char * {
    while(index < line->argc) {
        auto cp = line->argv[index++];
        if(*cp == '$' || *cp == '#' || *cp == '@' || *cp == '^') return ++cp;   // literal...
        if(*cp == '%' || *cp == '&') return syms.get_value(++cp);   // symbol...
        return cp;
    }
    return nullptr;
}

auto Session::is_label(const char *id) -> bool {
    assert(id != nullptr);

    if(*id == '@')
        ++id;

    auto section = image.init();
    while(section && !eq(id, section->id)) {
        section = section->next;
    }

    return section;
}

auto Session::goto_end() -> bool {
    index = 0;
    items = nullptr;

    if(!line->top) {
        line = nullptr;
        return false;
    }

    auto top = line->top;
    while(line) {
        line = line->next;
        if(line && line->top == top && eq(line->cmd, "end")) return true;
    }

    return false;;
}

auto Session::conditional() -> bool {
    if(index >= line->argc) return true;
    auto result = false;
    goto cont;

join:
    if(index >= line->argc) return result;
    if(eq(line->argv[index], "and") && !result) return false;
    if(!eq(line->argv[index], "or")) return false;
    ++index;
    goto cont;

cont:
    if(index >= line->argc) return result;
    auto cp = line->argv[index];
    if(eq(cp, "-defined")) {
        ++index;
        cp = value();
        result = (cp && *cp);
        goto join;
    }

    if(eq(cp, "-undefined")) {
        ++index;
        cp = value();
        result = !cp || !*cp;
        goto join;
    }

    auto v1 = value();
    auto op = line->argv[index++];
    auto v2 = value();

    if(!v1)
        v1 = "";

    if(!v2)
        v2 = "";

    if(!op)
        op = "?ne";

    if(eq(op, "?in")) {
        result = strstr(v2, v1) != nullptr;
        goto join;
    }

    if(eq(op, "?eq")) {
        result = eq(v1, v2);
        goto join;
    }

    if(eq(op, "?ne")) {
        result = !eq(v1, v2);
        goto join;
    }

    if(eq(op, "-eq")) {
        result = get_integer_or(v1, 0) == get_integer_or(v2, 0);
        goto join;
    }

    if(eq(op, "-ne")) {
        result = get_integer_or(v1, 0) != get_integer_or(v2, 0);
        goto join;
    }

    if(eq(op, "-gt")) {
        result = get_integer_or(v1, 0) > get_integer_or(v2, 0);
        goto join;
    }

    if(eq(op, "-ge")) {
        result = get_integer_or(v1, 0) >= get_integer_or(v2, 0);
        goto join;
    }

    if(eq(op, "-lt")) {
        result = get_integer_or(v1, 0) < get_integer_or(v2, 0);
        goto join;
    }

    if(eq(op, "-le")) {
        result = get_integer_or(v1, 0) <= get_integer_or(v2, 0);
        goto join;
    }

    result = false;
    goto join;
}

auto Session::get_status() -> const char * {
    if(shm) return shm->status;
    return "-";
}

auto Session::label(const char *id) -> bool {
    assert(id != nullptr);

    if(*id == '@')
        ++id;

    auto section = image.init();
    while(section && !eq(id, section->id)) {
        section = section->next;
    }

    if(!section) return false;
    header = section;
    entry();
    return true;
}

void Session::entry() {
    assert(header != nullptr);

    set_error("none");
    line = header->first;
    trap = nullptr;
    digits[0] = 0;
    index = level = 0;
    items = nullptr;
}

auto Session::reset() -> script::line_t * {
    auto reader = header->traps;
    while(reader) {
        if(eq(reader->id, "data")) return reader->first;
        reader = reader->next;
    }
    return nullptr;
}

auto Session::event(const char *id) -> bool {
    assert(id != nullptr);
    if(*id == '^')
        ++id;

    if(eq(header->id, "_init_") || eq(header->id, "exit")) return false;
    auto jump = header->traps;
    while(jump && !eq(jump->id, id))
        jump = jump->next;

    if(jump && (!trap || jump->group != trap->group)) {
        trap = jump;
        line = trap->first;
        digits[0] = 0;
        if(!eq(jump->id, "error"))
            set_error("none");
        return true;
    }

    return false;
}

auto Session::error(const char *reason) -> bool {
    if(getppid() > 1)
        print(stderr, "{}:{}:{}\n", image.name(), line->lnum, reason);

    set_error(reason);
    if(!eq(header->id, "_init_")) {
        if(event("error")) return false;
    }
    skip();
    return true;
}

auto Session::randomize(int min, int max, int sum) -> int {
    auto val = 0;
    std::uniform_int_distribution<> dist(min, max);
    while(sum-- > 0)
        val += dist(rand_gen);
    return val;
}
