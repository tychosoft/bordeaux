/*
 * Copyright (C) 2020 Tycho Softworks.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SCRIPT_HPP_
#define SCRIPT_HPP_

#include "server.hpp"       // IWYU pragma: keep

#include <moderncli/keyfile.hpp>

constexpr inline const char *SCRIPT_EXT = ".scr";

namespace server {
class Session;
using namespace std::string_literals;
using session_t = std::shared_ptr<Session>;
using session_ref = const std::weak_ptr<Session>;
using method_t = bool (Session::*)() noexcept;
using sid_t = long;

class script final {
public:
    static constexpr unsigned pager_size = TUNE_COMPILE_PAGER;
    static constexpr unsigned max_levels = TUNE_MAX_LEVELS;
    static constexpr unsigned max_imports = 10;

    struct line_t {
        line_t *next;
        line_t *top;
        const char *cmd;
        unsigned argc, lnum, mask;
        method_t method;
        const char *argv[1];
    };

    struct when_t {
        when_t *next;
        const char *argv[1];
    };

    struct trap_t {             // ^xx event trap...
        trap_t *next;
        line_t *first;
        const char *group;      // for common event groups
        char id[1];
    };

    struct header_t {
        header_t *next;
        trap_t *traps;
        line_t *first;
        when_t *when;           // answer when list...
        char id[1];
    };

    // context object for compiler session
    struct context_t {
        header_t *header, *first;
        line_t *line;
        when_t *when;
        trap_t *trap, *last{nullptr};
        std::string path, name, base;
        unsigned level;
        unsigned errors;
        unsigned lnum;
        mutable bool keystore;
    };

    explicit script(const std::string& path, const keyfile& config) noexcept;
    script() = default;

    auto operator!() const {
        return !ptr_;
    }

    auto name() const {
        return name_;
    }

    auto config() const -> const keyfile& {
        return config_;
    }

    auto init() const -> const header_t *;
    auto empty() const -> bool;
    auto count() const -> unsigned;

    static auto is_variable(const char *cp) -> bool;
    static auto is_constant(const char *cp) -> bool;
    static auto is_scoped(const char *cp) -> bool;

    static auto str_time24(const std::string& value) -> std::string;
    static auto str_months(const std::string& value) -> std::string;
    static auto str_days(const std::string& value) -> std::string;

private:
    friend class Driver;
    friend class Session;
    class data;

    const header_t *sections{};
    std::shared_ptr<data> ptr_;
    std::string name_;
    keyfile config_;
};
} // namespace
#endif
