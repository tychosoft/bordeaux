/*
 * Copyright (C) 2020 Tycho Softworks.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UTILS_HPP_
#define UTILS_HPP_

#include "server.hpp"       // IWYU pragma: keep

namespace server::util {
auto uri_schema(const std::string& id) -> std::string;
auto uri_userid(const std::string& id) -> std::string;
auto uri_server(const std::string& id) -> std::string;
auto uri_port(const std::string& id) -> uint16_t;
auto uri_host(const std::string& id) -> std::string;
auto uri_address(const std::string& id) -> std::string;
auto uri_setuser(const std::string& uri, const std::string& user) -> std::string;
auto make_uri(const std::string& schema, const std::string& host, uint16_t port = 0) -> std::string;
auto make_uri(const std::string& schema, const std::string& user, const std::string& host, uint16_t port = 0) -> std::string;
} // end namespace server utils
#endif
