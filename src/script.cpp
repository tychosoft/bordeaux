/*
 * Copyright (C) 2020 Tycho Softworks.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "script.hpp"
#include "symbols.hpp"
#include "drivers.hpp"

#include <moderncli/strings.hpp>
#include <moderncli/filesystem.hpp>

#include <list>
#include <unordered_set>

using namespace server;

namespace {
std::unordered_map<std::string_view, int> months = {
    {"1", 1},   {"jan", 1},     {"january", 1},
    {"2", 2},   {"feb", 2},     {"february", 2},
    {"3", 3},   {"mar", 3},     {"march", 3},
    {"4", 4},   {"apr", 4},     {"april", 4},
    {"5", 5},   {"may", 5},     {"may", 5},
    {"6", 6},   {"jun", 6},     {"june", 6},
    {"7", 7},   {"jul", 7},     {"july", 7},
    {"8", 8},   {"aug", 8},     {"august", 8},
    {"9", 9},   {"sep", 9},     {"september", 9},
    {"10", 10}, {"oct", 10},    {"october", 10},
    {"11", 11}, {"nov", 11},    {"november", 11},
    {"12", 12}, {"dec", 12},    {"december", 12},
};

std::unordered_map<std::string_view, int> days = {
    {"1", 1},   {"sun", 1}, {"sunday", 1},
    {"2", 2},   {"mon", 2}, {"monday", 2},
    {"3", 3},   {"tue", 3}, {"tuesday", 3},
    {"4", 4},   {"wed", 4}, {"wednesday", 4},
    {"5", 5},   {"thu", 5}, {"thursday", 5},
    {"6", 6},   {"fri", 6}, {"friday", 6},
    {"7", 7},   {"sat", 7}, {"saturday", 7},
};

std::list<std::string_view> when_options = {
    "before", "after", "until", "day", "year", "month", "state", "mon", "type"
};

std::list<std::string_view> func_options = {
    "&count", "&order", "&list", "&decimal", "&number", "&time", "&items", "&path", "&spell"
};

std::list<std::string_view> cond_options = {
    "-defined", "-undefined", "-eq", "-ne", "-gt", "-ge", "-lt", "-le"
};

std::list<std::string_view> expr_options = {
    "?eq", "?ne", "?in"
};

auto make_arg(const std::string& token, const script::header_t *scope) -> std::string {
    if(token[0] == '@' || token[0] == '^') return token;
    if(token[0] == '\"' || token[0] == '\'' || token[0] == '{') return "$" + unquote(token);
    if(token[0] == '%') return token;
    if(token[0] == '$' && eq(scope->id, "_init_")) return "";
    if(token[0] == '$' && token.find(':') != std::string::npos) return "%" + token.substr(1);
    if(token[0] == '$') return "%"s + scope->id + ":" + token.substr(1);
    if(symbols::is_number(token)) return "#" + token;
    if(symbols::is_path(token)) return "$" + token;
    if(symbols::is_symbol(token)) return token;
    return "";
}
} // end anon namespace

class script::data final : private mempager {
public:
    data() : mempager(pager_size) {}
    data(data const&) = delete;
    auto operator=(const data&) = delete;
    ~data() final;

    auto empty() const -> bool {
        return mempager::empty();
    }

    auto compile(const std::string& path, const keyfile& config) -> header_t *;
    auto make_header(const char *id, context_t& ctx) -> header_t *;
    auto make_line(context_t& ctx, const std::string& cmd, const std::list<std::string>& list, std::list<line_t*>& stack) -> bool;
    auto make_when(context_t& ctx, const std::vector<std::string>& tokens) -> bool;
    auto make_trap(context_t& ctx, const char *id) -> bool;

private:
};

script::data::~data() = default;

auto script::data::make_when(context_t& ctx, const std::vector<std::string>& tokens) -> bool {
    auto header = ctx.header;
    auto count = tokens.size() - 1;
    if(eq(header->id, "_init_")) {
        ++ctx.errors;
        logger.warn("{}({}): when: cannot be in init", ctx.name, ctx.lnum);
        return false;
    }

    if(header->first) {
        ++ctx.errors;
        logger.warn("{}({}): when: must be immediately after label", ctx.name, ctx.lnum);
        return false;
    }

    auto when = make<when_t>(sizeof(void *) * count);
    for(auto pos = 1UL; pos <= count; ++pos) {
        auto option(tokens[pos]);
        auto pair = split(option, "=");
        if(pair.size() != 2 || std::find(when_options.begin(), when_options.end(), pair[0]) == when_options.end()) {
            ++ctx.errors;
            logger.warn("{}({}): when: {}: unrecognized option", ctx.name, ctx.lnum, tokens[pos]);
            return false;
        }
        if(pair[0] == "after") {
            pair[0] = "a"; pair[1] = str_time24(pair[1]);
        }
        else if(pair[0] == "before") {
            pair[0] = "b"; pair[1] = str_time24(pair[1]);
        }
        else if(pair[0] == "until") {
            pair[0] = "u"; pair[1] = str_time24(pair[1]);
        }
        else if(pair[0] == "type")
            pair[0] = "t";
        else if(pair[0] == "state")
            pair[0] = "s";
        else if(pair[0] == "year")
            pair[0] = "y";
        else if(pair[0] == "day" || pair[0] == "dow") {
            pair[0] = "d"; pair[1] = str_days(pair[1]);
        }
        else if(pair[0] == "month" || pair[0] == "mon") {
            pair[0] = "m"; pair[1] = str_months(pair[1]);
        }
        if(pair[1].empty()) {
            ++ctx.errors;
            logger.warn("{}({}): when: {}: invalid value", ctx.name, ctx.lnum, tokens[pos]);
            return false;
        }
        when->argv[pos - 1] = dup(pair[0] + ":" + pair[1]);
    }
    when->argv[count] = nullptr;
    when->next = nullptr;

    if(!header->when)
        header->when = when;
    else if(ctx.when)
        ctx.when->next = when;
    ctx.when = when;
    return true;
}

auto script::data::make_trap(context_t& ctx, const char *id) -> bool {
    logger.debug(4, "{}({}): make trap ^{}", ctx.name, ctx.lnum, id);
    std::size_t id_size = mem_size(id, TUNE_SYMBOL_SIZE);
    auto trap = make<trap_t>(id_size);
    mem_copy(&trap->id[0], ++id_size, id);
    trap->next = ctx.trap;
    trap->first = nullptr;
    trap->group = Driver::event_group(trap->id);
    ctx.trap = ctx.last = trap;
    ctx.line = nullptr;
    return true;
}

auto script::data::make_line(context_t& ctx, const std::string& cmd, const std::list<std::string>& list, std::list<line_t *>& stack) -> bool {
    auto keyword = Driver::keyword(cmd);
    if(keyword.method == nullptr) {
        ++ctx.errors;
        logger.warn("{}({}): {}: unknown keyword", ctx.name, ctx.lnum, cmd);
        return false;
    }

    auto current = ctx.line;
    auto header = ctx.header;
    auto argc = static_cast<unsigned short>(list.size());
    auto line = make<line_t>(sizeof(void *) * argc);
    line->method = keyword.method;
    line->cmd = dup(cmd);
    line->argc = argc;
    line->argv[argc] = nullptr;
    line->next = nullptr;
    line->top = stack.front();

    if(cmd == "switch") {
        stack.push_front(line);
    }
    else if(cmd == "end") {
        if(stack.size() < 2) {
            ++ctx.errors;
            logger.warn("{}({}): end without block", ctx.name, ctx.lnum);
            return false;
        }
        stack.pop_front();
    }

    unsigned pos = 0;
    for(auto const& arg : list)
        line->argv[pos++] = dup(arg);

    ctx.line = line;
    if(keyword.check) {
        auto err = server::Driver::check(keyword.check, ctx);
        if(err) {
            ++ctx.errors;
            logger.warn("{}({}): {}: {}", ctx.name, ctx.lnum, cmd, err);
            ctx.line = current;
            return false;
        }
    }

    // set first line of any pending traps...
    auto trap = ctx.trap;
    while(trap) {
        trap->first = line;
        trap = trap->next;
    }

    // integrate pending trap list with script
    if(ctx.trap) {
        if(header->traps)
            ctx.trap->next = header->traps;
        header->traps = ctx.trap;
        ctx.trap = nullptr;
    }

    if(!header->first)
        header->first = line;
    if(current)
        current->next = line;
    return true;
}

auto script::data::make_header(const char *id, context_t& ctx) -> header_t * {
    if(eq(id, "_init_") && ctx.first != nullptr) {
        logger.debug(4, "{}({}): @{}: join header", ctx.name, ctx.lnum, id);
        ctx.header = ctx.first;
        ctx.line = ctx.first->first;
        while(ctx.line && ctx.line->next)
            ctx.line = ctx.line->next;
    }
    else {
        logger.debug(4, "{}({}): @{}: make header", ctx.name, ctx.lnum, id);
        auto size = mem_size(id, TUNE_SYMBOL_SIZE);
        auto prior = ctx.header;
        auto header = make<header_t>(size);
        mem_copy(&header->id[0], ++size, id);
        header->next = nullptr;
        header->when = nullptr;
        header->first = nullptr;
        header->traps = nullptr;
        if(prior)
            prior->next = header;
        ctx.header = header;
        ctx.line = nullptr;
        if(!ctx.first)
            ctx.first = header;
    }
    ctx.trap = nullptr;
    ctx.when = nullptr;
    ctx.errors = 0;
    return ctx.header;
}

// script compiler...
auto script::data::compile(const std::string& path, const keyfile& config) -> header_t * {
    std::unordered_set<std::string> imports;
    std::list<std::ifstream> files;
    std::list<std::string> paths;
    std::list<unsigned> lines;
    std::list<line_t *> stack;
    std::unordered_set<std::string> traps;
    std::unordered_set<std::string> labels;
    context_t ctx;
    ctx.keystore = false;
    ctx.level = 0;
    ctx.first = nullptr;
    ctx.base = fsys::path(path).stem();
    std::string buffer;
    auto total_sections = 0;

    if(!fsys::exists(path)) {
        mempager::clear();
        return nullptr;
    }

    lines.emplace_back(0);
    paths.emplace_back(path);
    files.emplace_back(path);
    if(!files.back().is_open()) {
        mempager::clear();
        return nullptr;
    }

import:
    buffer = "@_init_";
    ctx.path = paths.back();
    ctx.name = fsys::path(paths.back()).filename();
    ctx.header = nullptr;
    ctx.trap = nullptr;
    ctx.lnum = lines.back();
    auto count = 0;
    imports.insert(fsys::path(ctx.name).stem());

compile:
    count = 0;
    traps.clear();
    labels.insert(buffer);
    stack.clear();
    stack.emplace_front(nullptr);
    make_header(buffer.substr(1).c_str(), ctx);

    auto id = ctx.header->id;
    while(std::getline(files.back(), buffer)) {
        buffer = trim(buffer);
        ++ctx.lnum;

        if(buffer[0] == '#') continue;
        auto pos = buffer.find(" # ");
        if(pos == std::string::npos)
            pos = buffer.find("%% ");

        if(pos != std::string::npos)
            buffer.erase(pos);

        if(strip(buffer).empty()) continue;
        if(isspace(buffer[0])) {
            buffer = strip(buffer);
            if(buffer[0] == '#') continue;
            auto tokens = tokenize(buffer);
            if(tokens[0] == "plugin") {
                if(!eq(ctx.header->id, "_init_")) {
                    ++ctx.errors;
                    logger.warn("{}({}): plugin must be in _init_", ctx.name, ctx.lnum);
                    continue;
                }

                if(tokens.size() < 2) {
                    ++ctx.errors;
                    logger.warn("{}({}): plugin without ids", ctx.name, ctx.lnum);
                    continue;
                }
                auto enable = false;
                for(std::string_view token : tokens) {
                    if(token[0] == '!') {
                        token.remove_prefix(1);
                        if(Plugin::find(token)) {
                            enable = false;
                            break;
                        }
                    }
                    if(Plugin::find(token)) {
                        enable = true;
                        break;
                    }
                }
                if(enable) continue;
                logger.info("{}({}): required plugin missing", ctx.name, ctx.lnum);
                goto finish;
            }

            if(tokens[0] == "driver") {
                if(!eq(ctx.header->id, "_init_")) {
                    ++ctx.errors;
                    logger.warn("{}({}): driver must be in _init_", ctx.name, ctx.lnum);
                    continue;
                }

                if(tokens.size() < 2) {
                    ++ctx.errors;
                    logger.warn("{}({}): driver without ids", ctx.name, ctx.lnum);
                    continue;
                }
                auto enable = false;
                auto drid = Driver::instance()->name();
                for(std::string_view token : tokens) {
                    if(token[0] == '!') {
                        token.remove_prefix(1);
                        if(token == drid) {
                            enable = false;
                            break;
                        }
                    }
                    if(token == drid) {
                        enable = true;
                        break;
                    }
                }
                if(enable) continue;
                logger.info("{}({}): {}: wrong driver", ctx.name, ctx.lnum, drid);
                goto finish;
            }

            if(tokens[0] == "method") {
                if(!eq(ctx.header->id, "_init_")) {
                    ++ctx.errors;
                    logger.warn("{}({}): method must be in _init_", ctx.name, ctx.lnum);
                    continue;
                }

                if(tokens.size() < 2) {
                    ++ctx.errors;
                    logger.warn("{}({}): method without ids", ctx.name, ctx.lnum);
                    continue;
                }

                auto enable = false;
                for(std::string_view token : tokens) {
                    if(token[0] == '!') {
                        token.remove_prefix(1);
                        if(Driver::keyword(std::string{token}).method != nullptr) {
                            enable = false;
                            break;
                        }
                    }
                    if(Driver::keyword(std::string{token}).method != nullptr) {
                        enable = true;
                        break;
                    }
                }
                if(enable) continue;
                logger.info("{}({}): required method missing", ctx.name, ctx.lnum);
                goto finish;
            }


            if(tokens[0] == "import") {
                if(tokens.size() < 2) {
                    ++ctx.errors;
                    logger.warn("{}({}): import without modules", ctx.name, ctx.lnum);
                    continue;
                }
                if(!eq(ctx.header->id, "_init_")) {
                    ++ctx.errors;
                    logger.warn("{}({}): import must be in _init_", ctx.name, ctx.lnum);
                    continue;
                }
                if(count)
                    logger.debug(3, "{}({}): @_init_: {} line(s) compiled", ctx.name, ctx.lnum, count);
                tokens.erase(tokens.begin());
                lines.back() = ctx.lnum;
                for(auto const& inc_path : tokens) {
                    auto file_name = fsys::path(inc_path + SCRIPT_EXT).filename();
                    auto file_path = fsys::path(config.at("paths").at("modules")) / file_name;
                    if(imports.find(file_name) != imports.end()) continue;
                    if(!fsys::exists(file_path)) {
                        ++ctx.errors;
                        logger.warn("{}({}): {}: import path missing", ctx.name, ctx.lnum, file_path.string());
                        continue;
                    }
                    if(ctx.level > max_imports) {
                        ++ctx.errors;
                        logger.error("{}({}): {}: imports too deep", ctx.name, ctx.lnum, file_path.u8string());
                        continue;
                    }
                    ++ctx.level;
                    lines.emplace_back(0);
                    paths.emplace_back(file_path);
                    files.emplace_back(file_path);
                }
                goto import;
            }

            ++count;

            if(tokens[0] == "when") {
                make_when(ctx, tokens);
                continue;
            }

            std::list<std::string> args;
            if(!isalpha(tokens[0][0])) {
                ++ctx.errors;
                logger.warn("{}({}): {}: malformed keyword", ctx.name, ctx.lnum, tokens[0]);
                continue;
            }

            for(auto index = 1UL; index < tokens.size(); ++index) {
                auto& token = tokens[index];
                if(token[0] == '=')
                    args.push_back("$" + token);
                else if(token.find_first_of('=') != std::string::npos) {
                    // keyword options get pushed to front...
                    auto pair = split(token, "=");
                    if(pair[0].find_first_of(" =,:+-&#$@") != std::string::npos) goto normal;
                    if(pair.size() != 2) {
bad:					++ctx.errors;
                        logger.warn("{}({}): {}: malformed option", ctx.name, ctx.lnum, token);
                        continue;
                    }
                    token = make_arg(pair[1], ctx.header);
                    if(token.empty()) goto bad;
                    args.push_front(token);
                    args.push_front("=" + pair[0]);
                }
                else {
normal:
                    // arguments get pushed to back
                    if(token[0] == '&') {           // function tokens
                        if(std::find(func_options.begin(), func_options.end(), token) == func_options.end()) {
                            ++ctx.errors;
                            logger.warn("{}({}): {}: unknown function", ctx.name, ctx.lnum, token);
                            continue;
                        }
                        args.push_back(token);
                        continue;
                    }
                    if(token[0] == '-' && std::find(cond_options.begin(), cond_options.end(), token) != cond_options.end()) {
                        args.push_back(token);
                        continue;
                    }
                    if(token[0] == '?' && std::find(expr_options.begin(), expr_options.end(), token) != expr_options.end()) {
                        args.push_back(token);
                        continue;
                    }

                    auto arg_token = make_arg(token, ctx.header);
                    if(arg_token.empty()) {
                        ++ctx.errors;
                        logger.warn("{}({}): {}: malformed token", ctx.name, ctx.lnum, token);
                        continue;
                    }
                    args.push_back(arg_token);
                }
            }
            make_line(ctx, tokens[0], args, stack);
        }
        else {
            if(buffer[0] == '@') {
                if(labels.find(buffer) != labels.end()) {
                    ++ctx.errors;
                    logger.error("{}({}): {}: duplicate label", ctx.name, ctx.lnum, buffer);
                    goto finish;
                }
                else if(stack.size() > 1) {
                    ++ctx.errors;
                    logger.error("{}({}): {}: unclosed blocks(s)!", ctx.name, ctx.lnum, buffer);
                    goto finish;
                }
                else
                    goto closure;
            }
            else if(buffer[0] == '^') {
                if(stack.size() > 1) {
                    ++ctx.errors;
                    logger.error("{}({}): {}: unclosed blocks(s)!", ctx.name, ctx.lnum, buffer);
                    goto finish;
                }
                if(traps.find(buffer) == traps.end()) {
                    traps.insert(buffer);
                    make_trap(ctx, buffer.c_str() + 1);
                }
                else {
                    ++ctx.errors;
                    logger.error("{}({}): {}: duplicate trap", ctx.name, ctx.lnum, buffer);
                    goto finish;
                }
            }
        }
    }

closure:
    if(!eq(id, "_init_"))
        ++total_sections;
    if(ctx.errors)
        logger.error("{}({}): @{}: {} error(s)", ctx.name, ctx.lnum, id, ctx.errors);
    logger.debug(3, "{}({}): @{}: {} line(s) compiled", ctx.name, ctx.lnum, id, count);
    if(!files.back().eof()) goto compile;
finish:
    --ctx.level;
    paths.pop_back();
    files.pop_back();
    lines.pop_back();
    if(!files.empty()) goto import;
    logger.info("{}: {} total sections(s) created", ctx.name, total_sections);
    return ctx.first;
}

script::script(const std::string& path, const keyfile& cfg) noexcept : // NOLINT
ptr_(std::make_shared<script::data>()), name_(fsys::path(path).stem()), config_(cfg) {
    sections = ptr_->compile(path, cfg);
}

auto script::count() const -> unsigned {
    if(!ptr_) return 0;
    unsigned total = 0;
    auto section = sections;
    while(section) {
        ++total;
        section = section->next;
    }
    return total;
}

auto script::empty() const -> bool {
    return !ptr_ || ptr_->empty();
}

auto script::init() const -> const header_t * {
    if(empty()) return nullptr;
    if(!eq(sections->id, "_init_")) return nullptr;
    return sections;
}

auto script::is_scoped(const char *cp) -> bool {
    const char *p1 = strchr(cp, ':');
    const char *p2 = strrchr(cp, ':');
    return !p1 || p1 == p2;
}

auto script::is_variable(const char *cp) -> bool {
    if(!cp || !*cp) return false;
    if(!islower(*(cp)) || !is_scoped(cp)) return false;
    ++cp;
    while(*cp && (isdigit(*cp) || *cp == '_' || *cp == ':' || islower(*cp)))
        ++cp;

    return *cp == 0;
}

auto script::is_constant(const char *cp) -> bool {
    if(!cp || !*cp) return false;
    if(!isupper(*(cp++))) return false;
    while(*cp && (isdigit(*cp) || *cp == '_' || isupper(*cp)))
        ++cp;

    return *cp == 0;
}

auto script::str_time24(const std::string& value) -> std::string {
    auto text(value);
    if(text[1] == ':')
        text = "0" + text;

    int hour = 0, min = 0;
    if(text.length() < 2) return "";
    if(isdigit(text[0]) && isdigit(text[1])) {
        hour = (text[0] - '0') * 10 + (text[1] - '0');
        if(hour < 0 || hour > 23) return "";
    }

    if(text.length() < 3) goto done;
    if(tolower(text[2]) == 'a') goto am;
    if(tolower(text[2]) == 'p') goto pm;
    if(isdigit(text[2]))
        text = text.substr(2);
    else if(text[2] == ':')
        text = text.substr(3);
    else
        return "";

    std::string::size_type end; // NOLINT
    min = std::stoi(text, &end);
    if(end >= text.length()) goto done;
    if(tolower(text[end]) == 'a') goto am;
    if(tolower(text[end]) == 'p') goto pm;
    return "";

am:
    if(hour == 0) return "";
    if(hour == 12)
        hour = 0;
    if(hour > 12) return "";
    goto done;

pm:
    if(hour > 12 || hour == 0) return "";
    if(hour < 12)
        hour += 12;
    goto done;

done:
    if(hour < 0 || hour > 23) return "";
    if(min < 0 || min > 59) return "";
    return format("{:02}:{:02}", hour, min);
}

auto script::str_months(const std::string& value) -> std::string {
    std::set<int> set;
    auto list = split(value, ",");
    for(auto const& month : list) {
        auto current = strip(lower_case(month));
        auto num = months[current];
        if(num < 1) return "";
        set.insert(num);
    }
    return join(set);
}

auto script::str_days(const std::string& value) -> std::string {
    std::set<int> set;
    auto list = split(value, ",");
    for(auto const& day : list) {
        auto current = strip(lower_case(day));
        if(current == "weekday") {
            set.insert({2, 3, 4, 5, 6});
            continue;
        }
        if(current == "weekend") {
            set.insert({1, 7});
            continue;
        }
        auto num = days[current];
        if(num < 1) return "";
        set.insert(num);
    }
    return join(set);
}
