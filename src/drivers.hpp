/*
 * Copyright (C) 2020 Tycho Softworks.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DRIVERS_HPP_
#define DRIVERS_HPP_

#include "server.hpp"
#include "script.hpp"
#include "ipc.hpp"

namespace server {
class Driver {
friend class Session;
friend class script;
friend struct methods;
public:
    using context_t = script::context_t;
    using header_t = script::header_t;
    using check_t = const char *(*)(const script::context_t& scr);
    using line_t = script::line_t;

    struct keyword_t final {
        method_t method{nullptr};
        check_t check{nullptr};

        keyword_t() = default;

        keyword_t(method_t m, check_t c) : 
        method(m), check(c) {}

        operator bool() const {
            return method != nullptr;
        }

        auto operator!() const {
            return method == nullptr;
        }
    };

    Driver(const Driver&) = delete;
    auto operator=(const Driver&) -> Driver& = delete;

    virtual ~Driver();

    auto name() const noexcept {
        return name_;
    }

    auto limit() const noexcept {
        return limit_;
    }

    static auto instance() noexcept {
        return driver_;
    }

    static auto ring_timer() noexcept {
        return ring_timer_;
    }

    static auto answer_timer() noexcept {
        return answer_timer_;
    }

    static auto extend_runtime(const std::string& id, const keyword_t& keyword) {
        keywords[id] = keyword;
    }

    static auto extend_runtime(const std::initializer_list<std::pair<std::string, keyword_t>>& list) {
        for(auto const& [key, value] : list)
            keywords[key] = value;
    }

    NO_EXPORT static auto config_instance(keyfile& cfg, unsigned verbose) -> keyfile;
    NO_EXPORT static void reload_instance(const keyfile& cfg);
    NO_EXPORT static void start_instance(const keyfile& ini);
    NO_EXPORT static void drop_instance();
    NO_EXPORT static void stop_instance();

    static auto is_online() noexcept {
        return online.load();
    }

    static auto count() -> std::size_t;
    static auto get(std::string_view sid) -> session_t&;
    static auto get_script(const std::string& name) -> script&;
    static auto attach(const session_t& s) -> bool;
    static void release(const std::string_view& sid);
    static auto control(const std::vector<std::string_view>& list) -> bool;

protected:
    static std::unordered_map<std::string_view, keyword_t> keywords;
    static std::unordered_map<std::string_view, const char *> groups;

    explicit Driver(const std::string& id) noexcept;

    virtual void startup(const keyfile& config) = 0;
    virtual void restart(const keyfile& config);
    virtual void configure(keyfile& config, unsigned verbose);
    virtual void shutdown();
    virtual void disconnect();

    virtual void drop(Session *session);
    virtual void answer(Session *session);

    // check and coding related functions

    static auto keyword(const std::string& key) {
        return keywords[key];
    }

    static auto check(check_t handler, const context_t& ctx) {
        return (*(handler))(ctx);
    }

    static auto event_group(const char *id) {
        auto result = groups[id];
        return result ? result : id;
    }

    static void notify(bool set);

    unsigned limit_{0};

    static ipc_system *sys_mapped;
    static std::atomic<bool> online;

private:
    std::string name_;

    static Driver *driver_;
    static unsigned ring_timer_;
    static unsigned answer_timer_;
};

class Plugin {
public:
    Plugin(const Plugin&) = delete;
    auto operator=(const Plugin&) -> Plugin& = delete;

    virtual ~Plugin();

    auto name() const noexcept {
        return name_;
    }

    virtual auto speak(char *out, std::size_t size, const char *request, const char *value) -> bool;

    NO_EXPORT static void config_all(keyfile& cfg);
    NO_EXPORT static void reload_all(const keyfile& cfg);
    NO_EXPORT static void start_all(const keyfile& ini);
    NO_EXPORT static void stop_all();
    NO_EXPORT static auto find(const std::string_view& from) -> Plugin *;
    NO_EXPORT static auto find_locale(std::string_view from) -> Plugin *;
    NO_EXPORT static auto get_formatted(Session *s, const char *keyword) -> const char *;

protected:
    explicit Plugin(const std::string& id) noexcept;

    virtual void startup(const keyfile& config);
    virtual void restart(const keyfile& config);
    virtual void configure(keyfile& config);
    virtual void shutdown();
    virtual auto format(Session *s, const char *keyword) -> const char *;

private:
    friend class Session;

    std::string name_;
    Plugin *next_{nullptr};

    static Plugin *plugins_;
};

extern system_logger logger;            // global logging
extern std::atomic<bool> running;		// global server run flag
extern bool ipv6;					    // global ipv6 setting
} // end namespace
#endif
