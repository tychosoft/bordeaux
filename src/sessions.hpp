/*
 * Copyright (C) 2020 Tycho Softworks.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SESSIONS_HPP_
#define SESSIONS_HPP_

#include "audiofile.hpp"
#include "symbols.hpp"
#include "script.hpp"
#include "paths.hpp"

#include <moderncli/sync.hpp>

struct ipc_session;
struct audio_paths;

namespace server {
enum event_type {
    timeout,        // timer expires...
    err,            // error event
    end,            // end of media event
    response,       // response event
    digit,          // dtmf digit..
    flash,          // flash event
    entry,          // entering new state...
    leave,          // leaving old state...
    exit,           // script exiting
    drop,           // connection dropped
    answer,         // answer acknowledged
    shutdown,       // system shutdown notification
};

extern bool ipv6;

class Session {
public:
    static const long step_rate = TUNE_STEP_RATE;
    static const unsigned step_count = TUNE_STEP_COUNT;
    static const std::size_t max_digits = TUNE_MAX_DIGITS;

    // derived response object carried in shared ptr
    struct response_t {
        char sid[10];
        unsigned step;
    };

    struct event_t {
        event_type type;
        std::shared_ptr<const response_t> response;
        union {
            void *native_event;
            const char *reason;
            int result;
            unsigned timed;
            char digit[2];
            std::string_view sid;
        };
    };

    // Make sure if same shared pointer, always !<(l,r) && !(r,l)
    // If same time, we use memory order for uniqueness...
    // Otherwise ordered by time
    struct timer_compare {
        auto operator()(const session_t& lhs, const session_t& rhs) const -> bool {
            if(lhs.get() == rhs.get()) return false;
            if(lhs->expires == rhs->expires) return lhs.get() < rhs.get();
            return lhs->expires < rhs->expires;
        }
    };

    using runlist_t = std::set<session_t, timer_compare>;
    using run_t = runlist_t::iterator;

    Session() = delete;
    Session(const Session&) = delete;
    virtual ~Session();

    operator bool() const = delete;
    auto operator!() const -> bool = delete;

    auto sid() const {
        return sid_;
    }

    auto get_symbol(const char *id) {
        const std::lock_guard<std::mutex> lock(access_lock);
        return syms.get_value(id);
    }

    auto set_symbol(const char *id, const char *value) {
        const std::lock_guard<std::mutex> lock(access_lock);
        return syms.set_value(id, value);
    }

    auto stats() const {
        return shm;
    }

    auto slot() const {
        return slot_;
    }

    virtual void disconnect();
    virtual void stream();
    virtual void silence(bool shutdown = false);
    virtual auto play_files() -> bool;
    virtual auto play_tone() -> bool;
    virtual auto record_file() -> bool;

    auto is_label(const char *id) -> bool;
    auto get_phrase() -> audiofile::handle_t;
    auto get_status() -> const char *;

    static void sync_store();
    static void release(const session_t& session);
    static void begin(const session_t& session, const char *name = "main");
    static auto send(const session_t& session, const event_t &event) -> bool;
    static void start(int priority);
    static void stop_all();

protected:
    friend class Driver;
    friend class Plugin;

    using handler_t = bool (Session::*)(const event_t&);

    class Plugin *language;
    handler_t handler;
    sync_timepoint started, expires;
    std::mutex access_lock;
    symbols syms;
    script image;
    audiofile playrec;
    char *digits{nullptr};      // set from storage
    ipc_session *shm;           // segment mapped for bordeauxctl

    union {
        struct collect_t {
            std::size_t max_digits, min_digits;
            char *terminators;
            char *digits;
            long wait_until;
            long interdigit;
        } collect;
        struct tone_t {
            long timeout;
            unsigned count;
        } tone;
        struct prompt_t {
            uint32_t pos;
            long timeout;
            bool created;
        } prompt;
        unsigned timeout{0U};
    } state{};

    audio_paths paths, sys_audio;
    script::line_t *begins[script::max_levels]{};
    char temp[256]{};
    char item[128]{};
    char temp2[128]{};
    const char *items{};
    const script::header_t *header{};
    const script::trap_t *trap{};
    const script::line_t *line{};
    const script::line_t *data{};
    unsigned index, level;
    bool connected{false}, exiting{false}, media{false};
    unsigned pause{0U};

    // consts we manage in code...
    char *var_lang{nullptr};
    char *var_voice{nullptr};
    char *var_location{nullptr};
    char *var_mailbox{nullptr};

    explicit Session(const script& attach) noexcept;

    auto process_event(const event_t& event) noexcept {
        try {
            return (this->*(handler))(event);
        }
        catch(const std::exception& e) {
            set_error(e.what());
            return false;
        }
    }

    auto execute_method(method_t method) noexcept {
        return (this->*(method))();
    }

    auto argv() const {
        return line->argv;
    }

    void skip() {
        if(line)
            line = line->next;
        index = 0;
        items = nullptr;
    }

    auto this_step() {
        return set_handler("script", &Session::script_handler);
    }

    auto next_step() {
        skip();
        return set_handler("script", &Session::script_handler);
    }

    void set_error(const char *text) {
        mem_copy(error_, symbols::var_size, text);
    }

    void set_timed(long value, std::string_view note = std::string_view());
    auto set_handler(const char *text, handler_t handler) -> bool;
    void set_timeout(const sync_timepoint& until);
    void set_timeout(long offset);
    void bump_timeout(long offset);
    void clear_timeout();
    void audio_release(long offset = 500);
    void set_lang(const char *from);

    auto argv(unsigned short offset) -> const char *;
    auto output() -> const char *;  // test if currently a &xxx function
    auto option() -> const char *;  // test if currently in a =xx option
    auto symbol() -> const char *;  // get next symbol (var symname ...)
    auto itemize() -> const char *; // get next itemized value until nullptr
    auto value() -> const char *;   // get next argument value until nullptr
    auto fetch() -> const char *;   // get next itemized value until nullptr
    auto arg() -> const char *;     // version of value for funcs
    void get_store(const char *id); // get keys from keystore
    void put_store(const char *id); // put keys into keystore

    void entry();   // entry into new state, figure out event masks, etc...
    auto error(const char *reason) -> bool;
    auto event(const char *id) -> bool;
    auto label(const char *id) -> bool;
    auto reset() -> script::line_t *;
    void begin(const char *name = "main");
    auto conditional() -> bool;
    auto goto_end() -> bool;

    auto common_handler(const event_t &event) -> bool;
    auto collect_handler(const event_t &event) -> bool;
    auto sleep_handler(const event_t &event) -> bool;
    auto answer_handler(const event_t &event) -> bool;
    auto script_handler(const event_t &event) -> bool;
    auto dummy_handler(const event_t &event) -> bool;
    auto shutdown_handler(const event_t &event) -> bool;
    auto speak_handler(const event_t &event) -> bool;
    auto tone_handler(const event_t &event) -> bool;
    auto play_handler(const event_t &event) -> bool;
    auto record_handler(const event_t &event) -> bool;

    static auto randomize(int min, int max, int sum = 1) -> int;

private:
    friend class rtp_audio;

    run_t run_;
    char *error_{}, *timed_{}, *notes_{};
    unsigned slot_{};
    const char *app_{};
    const char *sid_{};

    auto funcs(const char *cp) -> const char *;       // ?xxx query ops...
};
} // end namespace
#endif
