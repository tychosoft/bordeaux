/*
 * Copyright (C) 2023 Tycho Softworks.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef	PATHS_HPP_
#define	PATHS_HPP_

#include "server.hpp"       // IWYU pragma: keep

#include <moderncli/strings.hpp>

struct audio_paths final {
    const char *mailbox{"none"};
    const char *app{"none"};
    const char *lang{"en"};
    const char *voice{"default"};
    const char *prefix{BORDEAUX_VOICES_PATH};
    const char *prompts{BORDEAUX_PROMPTS_PATH};
    const char *record{"prompts"};

    static auto savefile(const char *from, char *to, std::size_t size, const audio_paths *paths) {
        if(!from || !*from || !to || size < 2) return false;
        if(strstr(from, "..") || strstr(from, "/.") || *from == '.' || *from == '/') return false;
        if(eq(from, "greet/", 6)) {
            if(eq(paths->mailbox, "none")) return false;
            snprintf(to, size, "%s/greet/%s/%s%s", paths->prompts, paths->mailbox, from + 6, ".au");
        }
        if(eq(from, "msg/", 4)) {
            if(eq(paths->mailbox, "none")) return false;
            snprintf(to, size, "%s/msg/%s/%s%s", paths->prompts, paths->mailbox, from + 6, ".au");
        }
        else if(eq(from, "sys/", 4) || eq(from, "app/", 4))
            return false;
        else if(strchr(from, '/'))
            snprintf(to, size, "%s/%s%s", paths->prompts, from, ".au");
        else
            snprintf(to, size, "%s/%s/%s%s", paths->prompts, paths->record, from, ".au");
        while(to && *to) {
            *to = static_cast<char>(tolower(*to));
            ++to;
        }
        return true;
    }

    static auto playfile(const char *from, char *to, std::size_t size, const audio_paths *paths) {
        if(!from || !*from || !to || size < 2) return false;
        if(strstr(from, "..") || strstr(from, "/.") || *from == '.' || *from == '/') return false;
        if(eq(from, "greet/", 6)) {
            if(eq(paths->mailbox, "none")) return false;
            snprintf(to, size, "%s/greet/%s/%s%s", paths->prompts, paths->mailbox, from + 6, ".au");
        }
        if(eq(from, "msg/", 4)) {
            if(eq(paths->mailbox, "none")) return false;
            snprintf(to, size, "%s/msg/%s/%s%s", paths->prompts, paths->mailbox, from + 6, ".au");
        }
        else if(eq(from, "sys/", 4))
            snprintf(to, size, "%s/sys/%s%s", paths->prefix, from + 4, ".au");
        else if(eq(from, "app/", 4))
            snprintf(to, size, "%s/%s/%s/%s/%s%s", paths->prefix, paths->lang, paths->voice, paths->app, from + 4, ".au");
        else if(strchr(from, '/'))
            snprintf(to, size, "%s/%s%s", paths->prompts, from, ".au");
        else
            snprintf(to, size, "%s/%s/%s/%s%s", paths->prefix, paths->lang, paths->voice, from, ".au");
        while(to && *to) {
            *to = static_cast<char>(tolower(*to));
            ++to;
        }
        return true;
    }
};
#endif
