/*
 * Copyright (C) 2020 Tycho Softworks.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "drivers.hpp"
#include "runtime.hpp"
#include "utils.hpp"

#include <moderncli/filesystem.hpp>
#include <moderncli/scan.hpp>

using namespace server;

auto methods::scr_data() noexcept -> bool {
    skip();
    return true;
}

auto methods::scr_reset() noexcept -> bool {
    reset();
    skip();
    return true;
}

auto methods::scr_pause() noexcept -> bool {
    clear_timeout();
    set_handler("pause", &methods::sleep_handler);
    return false;
}

auto methods::scr_exit() noexcept -> bool {
    line = nullptr;
    return true;
}

auto methods::scr_switch() noexcept -> bool {
    if(index >= line->argc) {
        skip();
        return true;
    }

    if(conditional()) {
        skip();
        return false;
    }

    return goto_end();
}

auto methods::scr_case() noexcept -> bool {
    if(conditional()) {
        while(line && eq(line->cmd, "case"))    // cascade fall-through...
            skip();
        return false;
    }

    index = 0;

    auto top = line->top;
    while(line) {
        line = line->next;
        if(!line)
            return false;

        if(line->top == top && eq(line->cmd, "otherwise")) {
            skip();
            return false;
        }
        if(line->top == top && eq(line->cmd, "end")) return false;
    }

    return false;
}

auto methods::scr_otherwise() noexcept -> bool {
    return goto_end();
}

auto methods::scr_break() noexcept -> bool {
    if(!conditional()) {
        skip();
        return false;
    }
    goto_end();
    return false;
}

auto methods::scr_end() noexcept -> bool {
    if(index >= line->argc) {
        skip();
        return true;
    }

    return scr_repeat();
}

auto methods::scr_repeat() noexcept -> bool {
    if(!conditional()) {
        skip();
        return false;
    }

    if(line->top) {
        line = line->top;
        index = 0;
    }
    else
        entry();
    return false;
}

auto methods::scr_event() noexcept -> bool {
    while(auto signal = value()) {
        if(event(signal)) return false;
    }
    return error("trap not found");
}

auto methods::scr_do() noexcept -> bool {
    while(auto next = value()) {
        if(label(next)) return false;
    }
    return error("label not found");
}

auto methods::scr_set() noexcept -> bool {
    auto sym = symbol();
    if(!sym) return error("var missing");
    try {
        char buffer[symbols::var_size + 1]{0};
        buffer[0] = 0;
        while(auto v = value())
            mem_append(buffer, sizeof(buffer), v);

        if(!syms.set_value(sym, buffer)) return error("cannot set value");
    } catch(const std::exception& e) {
        return error(e.what());
    }

    skip();
    return true;
}

auto methods::scr_store() noexcept -> bool {
    try {
        auto store = syms.get_value("KEYSTORE");
        if(!store || !*store) {
            if(getppid() == 1)
                logger.error(":sid({}): cannot get keystore", sid());
            return error("cannot get keystore");
        }
        put_store(store);
    }
    catch(...) {}
    skip();
    return true;
}

auto methods::scr_keystore() noexcept -> bool {
    try {
        auto store = value();
        if(!store || !*store) {
            if(getppid() == 1)
                logger.error(":sid({}): cannot get keystore", sid());
            return error("cannot get keystore");
        }
        syms.make_const("KEYSTORE", store);
        get_store(store);
    } catch(...) {}
    skip();
    return true;
}

auto methods::scr_here() noexcept -> bool {
    try {
        const char *lang{};
        const char *val{};
        const char *voice{};
        while(auto opt = option()) {
            val = value();
            if(!val) return error("missing value");
            if(eq(opt, "voice"))
                voice = val;
            else if(eq(opt, "language") || eq(opt, "lang"))
                lang = val;
            else
                return error("invalid option");
        }
        val = value();
        if(val && *val)
            str_copy(var_location, 64, val);
        if(voice && *voice)
            str_copy(var_voice, 64, voice);
        if(lang && *lang)
            set_lang(lang);
    } catch(...) {}
    skip();
    return true;
}

auto methods::scr_speak() noexcept -> bool {
    paths = sys_audio;
    pause = 200U;
    set_timed(1, "-");

    while(auto opt = option()) {
        auto val = value();
        if(!val) return error("missing value");
        if(eq(opt, "voice"))
            paths.voice = val;
        else if(eq(opt, "mailbox"))
            paths.mailbox = val;
        else if(eq(opt, "pause"))
            pause = get_seconds_or(val);
        else
            return error("invalid option");
    }

    auto handle = get_phrase();
    if(handle == audiofile::invalid) return error("cannot play");
    clear_timeout();   // if no timer set...
    set_handler("speak", &methods::speak_handler);
    return false;
}

auto methods::scr_debug() noexcept -> bool {
    try {
        auto delim=" ";
        char buffer[256]{0};
        while(auto opt = option()) {
            auto val = value();
            if(!val) return error("missing value");
            if(eq(opt, "delim"))
                delim = val;
            else
                return error("invalid option");
        }

        auto level = value()[0] - '0';
        buffer[0] = 0;
        while(auto v = value()) {
            if(buffer[0] && delim && delim[0])
                mem_append(buffer, sizeof(buffer), delim);
            mem_append(buffer, sizeof(buffer), v);
        }
        logger.debug(level, "sid({}): {}", sid(), buffer);
    } catch(...) {}
    skip();
    return true;
}

auto methods::scr_const() noexcept -> bool {
    if(!eq(header->id, "_init_")) return error("can only be initializer");
    try {
        auto sym = symbol();
        if(!sym) return error("const missing");
        char buffer[symbols::var_size + 1]{0};
        buffer[0] = 0;
        while(auto v = value())
            mem_append(buffer, sizeof(buffer), v);

        // make sure defined once only...
        if(!syms.exists(sym))
            syms.make_const(sym, buffer);
        skip();
        return true;
    }
    catch(const std::exception& e) {
        return error(e.what());
    }
}

auto methods::scr_clear() noexcept -> bool {
    if(line->argc == 0)
        digits[0] = 0;
    else while(auto sym = symbol())
        syms.clear_symbol(sym);

    if(value()) return error("invalid arguments");
    skip();
    return true;
}

auto methods::scr_input() noexcept -> bool {
    digits[0] = 0;
    return scr_collect();
}

auto methods::scr_collect() noexcept -> bool {
    state.collect.max_digits = 3;
    state.collect.min_digits = 1;
    state.collect.wait_until = 10000L;
    state.collect.interdigit = 1000L;
    state.collect.digits = digits;

    auto max_size = max_digits;

    while(auto opt = option()) {
        auto val = value();
        if(!val) return error("missing value");
        if(eq(opt, "interdigit"))
            state.collect.interdigit = get_timeout_or(val);
        else if(eq(opt, "timeout"))
            state.collect.wait_until = get_timeout_or(val);
        else if(eq(opt, "digits"))
            state.collect.max_digits = get_unsigned_or(val);
        else if(eq(opt, "requires"))
            state.collect.min_digits = get_unsigned_or(val);
        else
            return error("invalid option");
    }

    auto sym = symbol();
    if(sym) {
        state.collect.digits = syms.make_storage(sym, static_cast<unsigned short>(state.collect.max_digits));
        if(!state.collect.digits) {
            state.collect.digits = syms.get_storage(sym);
            if(!state.collect.digits) return error("symbol not defined");
        }
        max_size = syms.get_size(sym);
        syms.set_value(sym, digits);       // copy and clear digits buffer
        digits[0] = 0;
    }

    if(state.collect.max_digits > max_size) return error("too many digits");
    if(state.collect.min_digits > state.collect.max_digits) return error("too many required");

    if(!state.collect.max_digits || !state.collect.min_digits || !state.collect.interdigit || !state.collect.wait_until) return error("invalid value");
    set_handler("collect", &methods::collect_handler);
    return true;
}

auto methods::scr_rand() noexcept -> bool {
    auto size = 3U;
    auto min = 1, max = 100, sum = 1;

    while(auto opt = option()) {
        auto val = value();
        if(!val) return error("missing value");
        if(eq(opt, "size")) {
            size = get_unsigned_or<unsigned short>(val);
            if(size < 1 || size > symbols::max_array) return error("invalid size");
        }
        else if(eq(opt, "min"))
            min = get_integer_or(val);
        else if(eq(opt, "max"))
            max = get_integer_or(val);
        else if(eq(opt, "sum"))
            sum = get_integer_or(val);
        else
            return error("invalid option");
    }

    while(auto sym = symbol()) {
        syms.make_value(sym, size);
        syms.set_value(sym, std::to_string(Session::randomize(min, max, sum)).c_str());
    }

    if(value()) return error("invalid arguments");
    skip();
    return true;
}

auto methods::scr_read() noexcept -> bool {
    auto size = symbols::var_size;

    while(auto opt = option()) {
        auto val = value();
        if(!val) return error("missing value");
        if(eq(opt, "key")) {
            auto found = false;
            reset();
            while(data && !found) {
                if(eq(data->cmd, "key")) {
                    for(unsigned pos = 0; pos < data->argc; ++pos) {
                        if(eq(data->argv[pos], val)) {
                            found = true;
                            break;
                        }
                    }
                }
                data = data->next;
            }
        }
        else if(eq(opt, "size")) {
            size = get_unsigned_or<unsigned short>(val);
            if(size < 1 || size > symbols::max_array) return error("invalid size");
        }
        else
            return error("invalid option");
    }

    if(!data) {
        if(event("end")) return false;
        return error("end of data");
    }

    unsigned index = 0;
    while(auto sym = symbol()) {
        syms.make_value(sym, size);
        if(index < data->argc)
            syms.set_value(sym, data->argv[index]);
        ++index;
    }

    if(value()) return error("invalid arguments");
    skip();
    return true;
}

auto methods::scr_var() noexcept -> bool {
    auto size = symbols::var_size;
    while(auto opt = option()) {
        auto val = value();
        if(!val) return error("missing value");
        if(eq(opt, "size")) {
            size = get_unsigned_or<unsigned short>(val);
            if(size < 1 || size > symbols::max_array) return error("invalid size");
        }
        else
            return error("invalid option");
    }

    while(auto sym = symbol())
        syms.make_value(sym, size);

    if(value()) return error("invalid arguments");
    skip();
    return true;
}

auto methods::scr_answer() noexcept -> bool {
    if(connected) {
        skip();
        return true;
    }

    Driver::instance()->answer(this);
    auto until = get_timeout_or("30s");
    set_timeout(sync_clock(until));
    set_handler("answer", &methods::answer_handler);
    return false;
}

auto methods::scr_info() noexcept -> bool {
    paths = sys_audio;
    set_timed(1, "-");

    while(auto opt = option()) {
        auto val = value();
        if(!val) return error("missing value");
        if(eq(opt, "prefix"))
            paths.record = val;
        else if(eq(opt, "mailbox"))
            paths.mailbox = val;
        else
            return error("invalid option");
    }

    auto cp = value();
    if(!cp) return error("no prompt file");
    char path[128];
    if(!audio_paths::savefile(cp, path, sizeof(path), &paths)) return error("invalid prompt path");
    if(!playrec.play(path)) return error("prompt file not found");
    auto sample_size = playrec.bytes_per_sample() ? playrec.bytes_per_sample() : 1;
    auto samples_msec = playrec.sample_rate() / 1000L;
    auto samples = (playrec.size() / sample_size);
    set_timed(samples / samples_msec, playrec.annotation());
    playrec.close();
    skip();
    return false;
}

auto methods::scr_copy() noexcept -> bool {
    paths = sys_audio;

    while(auto opt = option()) {
        auto val = value();
        if(!val) return error("missing value");
        if(eq(opt, "prefix"))
            paths.record = val;
        else if(eq(opt, "mailbox"))
            paths.mailbox = val;
        else
            return error("invalid option");
    }

    auto cp1 = value();
    if(!cp1) return error("no source file");

    auto cp2 = value();
    if(!cp2) return error("no target file");
    char path1[128], path2[128];
    if(!audio_paths::savefile(cp1, path1, sizeof(path1), &paths)) return error("invalid source path");
    if(!audio_paths::savefile(cp2, path2, sizeof(path2), &paths)) return error("invalid target path");
    try {
        fsys::copy_file(path1, path2);
    } catch(fsys::filesystem_error& err) {
        error("copy failed");
        return false;
    }
    skip();
    return false;
}

auto methods::scr_move() noexcept -> bool {
    paths = sys_audio;

    while(auto opt = option()) {
        auto val = value();
        if(!val) return error("missing value");
        if(eq(opt, "prefix"))
            paths.record = val;
        else if(eq(opt, "mailbox"))
            paths.mailbox = val;
        else
            return error("invalid option");
    }

    auto cp1 = value();
    if(!cp1) return error("no source file");
    auto cp2 = value();
    if(!cp2) return error("no target file");
    char path1[128], path2[128];
    if(!audio_paths::savefile(cp1, path1, sizeof(path1), &paths)) return error("invalid source path");
    if(!audio_paths::savefile(cp2, path2, sizeof(path2), &paths)) return error("invalid target path");
    try {
        fsys::copy_file(path1, path2);
    } catch(fsys::filesystem_error& err) {
        error("move failed");
        return false;
    }

    skip();
    return false;
}

auto methods::scr_erase() noexcept -> bool {
    paths = sys_audio;

    while(auto opt = option()) {
        auto val = value();
        if(!val) return error("missing value");
        if(eq(opt, "prefix"))
            paths.record = val;
        else if(eq(opt, "mailbox"))
            paths.mailbox = val;
        else
            return error("invalid option");
    }

    auto cp = value();
    if(!cp) return error("no prompt file");
    char path[128];
    if(!audio_paths::savefile(cp, path, sizeof(path), &paths)) return error("invalid erase path");
    fsys::remove(path);
    skip();
    return false;
}

auto methods::scr_record() noexcept -> bool {
    state.prompt.timeout = get_timeout_or("60s");
    state.prompt.pos = 0;
    paths = sys_audio;
    set_timed(1, "-");

    auto note = "record file";
    while(auto opt = option()) {
        auto val = value();
        if(!val) return error("missing value");
        if(eq(opt, "duration"))
            state.prompt.timeout = get_timeout_or(val);
        else if(eq(opt, "note"))
            note = val;
        else if(eq(opt, "prefix"))
            paths.record = val;
        else if(eq(opt, "mailbox"))
            paths.mailbox = val;
        else
            return error("invalid option");
    }

    auto cp = value();
    if(!cp) return error("no prompt file");
    char path[128];
    if(!audio_paths::savefile(cp, path, sizeof(path), &paths) || playrec.make(path, 1, 8000, 1, 0664, note)) return error("invalid record file");
    clear_timeout();
    playrec.pos(uint32_t(0));
    set_timed(1, note);
    set_handler("record", &methods::record_handler);
    return false;
}

auto methods::scr_append() noexcept -> bool {
    state.prompt.timeout = get_timeout_or("60s");
    paths = sys_audio;
    set_timed(1, "-");

    auto offset{0L};
    while(auto opt = option()) {
        auto val = value();
        if(!val) return error("missing value");
        if(eq(opt, "duration"))
            state.prompt.timeout = get_timeout_or(val);
        else if(eq(opt, "offset"))
            offset = get_timeout_or(val);
        else if(eq(opt, "prefix"))
            paths.record = val;
        else if(eq(opt, "mailbox"))
            paths.mailbox = val;
        else
            return error("invalid option");
    }

    auto cp = value();
    if(!cp) return error("no prompt file");
    char path[128];
    if(!audio_paths::savefile(cp, path, sizeof(path), &paths) || playrec.make(path)) return error("invalid record file");
    clear_timeout();
    if(offset == 0) {
        playrec.append();
        state.prompt.pos = playrec.pos();
    }
    else {
        state.prompt.pos = static_cast<uint32_t>(offset * ((playrec.bytes_per_sample() * playrec.sample_rate()) / 1000L));
        playrec.pos(state.prompt.pos);
    }
    set_timed(1, playrec.annotation());
    set_handler("record", &methods::record_handler);
    return false;
}

auto methods::scr_play() noexcept -> bool {
    state.prompt.timeout = 0;
    state.prompt.pos = 0;
    paths = sys_audio;
    set_timed(1, "-");

    auto offset = 0L;
    while(auto opt = option()) {
        auto val = value();
        if(!val) return error("missing value");
        if(eq(opt, "duration"))
            state.prompt.timeout = get_timeout_or(val);
        else if(eq(opt, "offset"))
            offset = get_timeout_or(val);
        else if(eq(opt, "prefix"))
            paths.record = val;
        else if(eq(opt, "mailbox"))
            paths.mailbox = val;
        else
            return error("invalid option");
    }

    auto cp = value();
    if(!cp) return error("no prompt file");
    char path[128];
    if(!audio_paths::savefile(cp, path, sizeof(path), &paths) || !playrec.play(path)) return error("missing prompt file");
    clear_timeout();
    state.prompt.pos = static_cast<uint32_t>(offset * ((playrec.bytes_per_sample() * playrec.sample_rate()) / 1000L));
    playrec.pos(state.prompt.pos);
    set_timed(1, playrec.annotation());
    set_handler("play", &methods::play_handler);
    return false;
}

auto methods::scr_tone() noexcept -> bool {
    state.tone.timeout = 0;
    state.tone.count = 1;
    paths = sys_audio;

    while(auto opt = option()) {
        auto val = value();
        if(!val) return error("missing value");
        if(eq(opt, "duration")) {
            state.tone.timeout = get_timeout_or(val);
            state.tone.count = 0;
        }
        else if(eq(opt, "voice"))
            paths.voice = val;
        else if(eq(opt, "count"))
            state.tone.count = get_unsigned_or<unsigned>(val, 1);
        else
            return error("invalid option");
    }

    auto cp = value();
    if(!cp)
        return error("missing tone argument");

    char path[128];
    if(!audio_paths::playfile(cp, path, sizeof(path), &paths) || !playrec.play(path)) return error("missing tone");
    clear_timeout();   // if no timer set...
    set_handler("tone", &methods::tone_handler);
    return false;
}

auto methods::scr_sleep() noexcept -> bool {
    const char *cp = value();
    if(!cp) return error("missing value");
    auto until = get_timeout_or(cp);
    if(until < 1) {
        skip();
        return true;
    }

    set_timeout(sync_clock(until));
    set_handler("sleep", &methods::sleep_handler);
    return false;
}

auto methods::scr_until() noexcept -> bool {
    const char *cp = value();
    if(!cp) return error("missing value");

    auto until = get_timeout_or(cp);
    if(until < 1) {
        skip();
        return true;
    }

    auto wait = started;
    wait += std::chrono::milliseconds(until);
    if(sync_remains(wait) < std::chrono::milliseconds(50)) return error("time expired");
    set_timeout(wait);
    set_handler("sleep", &methods::sleep_handler);
    return false;
}

auto checks::chk_data(const script::context_t& ctx) -> const char * {
    if(!ctx.last || !eq(ctx.last->id, "data")) return "can only be used in ^data segment";
    if(!ctx.line || ctx.line->argc < 1) return "data items required";
    return nullptr;
}

auto checks::chk_key(const script::context_t& ctx) -> const char * {
    if(!ctx.last || !eq(ctx.last->id, "data")) return "can only be used in ^data segment";
    if(!ctx.line || ctx.line->argc < 1) return "key item(s) required";
    return nullptr;
}

auto checks::chk_keyword(const script::context_t& ctx) -> const char * {
    if(ctx.header && eq(ctx.header->id, "_init_")) return "cannot be used to initialize";
    if(ctx.line && ctx.line->argc) return "arguments not used";
    return nullptr;
}

auto checks::chk_conditional(const script::context_t& ctx) -> const char * {
    if(ctx.header && eq(ctx.header->id, "_init_")) return "cannot be used to initialize";
    if(!ctx.line || !ctx.line->argc) return nullptr;
    // TODO: expression verification... chk_expression
    return nullptr;
}

auto checks::chk_otherwise(const script::context_t& ctx) -> const char * {
    if(ctx.header && eq(ctx.header->id, "_init_")) return "cannot be used to initialize";
    if(ctx.line && ctx.line->argc) return "arguments not used";
    if(ctx.line && !ctx.line->top) return "can only be in a block";
    // TODO: expression verification... chk_expression
    return nullptr;
}

auto checks::chk_case(const script::context_t& ctx) -> const char * {
    if(ctx.header && eq(ctx.header->id, "_init_")) return "cannot be used to initialize";
    if(!ctx.line || !ctx.line->argc) return "requires conditional arguments";
    auto top = ctx.line->top;
    if(!top) return "can only be in a block";
    top = top->next;
    if(top && !eq(top->cmd, "case")) return "can only be in cascading case block";
    // TODO: expression verification... chk_expression
    return nullptr;
}

auto checks::chk_const(const script::context_t& ctx) -> const char * {
    if(ctx.header && !eq(ctx.header->id, "_init_")) return "can only be initialize";
    if(!ctx.line || !ctx.line->argc) return "const symbol required";
    auto cp = ctx.line->argv[0];
    if(*cp == '%')
        ++cp;

    if(!script::is_constant(cp)) return "consts must be upper case";
    return nullptr;
}

auto checks::chk_clear(const script::context_t& ctx) -> const char * {
    if(!ctx.line || !ctx.line->argc) return nullptr; // by default clears %digits...
    unsigned index = 0;
    const char *cp = nullptr;
    while(nullptr != (cp = ctx.line->argv[index])) {
        if(*cp == '%')
            ++cp;

        if(!script::is_variable(cp)) return "vars must be lower case and have single scope";
        ++index;
    }
    return nullptr;
}

auto checks::chk_var(const script::context_t& ctx) -> const char * {
    if(!ctx.line || !ctx.line->argc) return "var symbol required";
    unsigned index = 0;
    const char *cp = nullptr;
    while(nullptr != (cp = ctx.line->argv[index])) {
        if(*cp != '=')
            break;
        cp = ctx.line->argv[++index];
        if(!cp) return "var option missing";
        ++index;
    }

    while(nullptr != (cp = ctx.line->argv[index])) {
        if(*cp == '%')
            ++cp;

        if(!script::is_variable(cp)) return "vars must be lower case and have single scope";
        ++index;
    }
    return nullptr;
}

auto checks::chk_collect(const script::context_t& ctx) -> const char * {
    if(ctx.header && eq(ctx.header->id, "_init_")) return "cannot be used to initialize";
    unsigned index = 0;
    const char *cp = nullptr;
    while(nullptr != (cp = ctx.line->argv[index])) {
        if(*cp != '=') break;
        cp = ctx.line->argv[++index];
        if(!cp) return "var option missing";
        ++index;
    }

    if(index < ctx.line->argc - 1) return "only one var allowed";
    cp = ctx.line->argv[index];
    if(!cp) return nullptr;
    if(*cp == '%')
        ++cp;

    if(!script::is_variable(cp)) return "vars must be lower case and have single scope";
    return nullptr;
}

auto checks::chk_keystore(const script::context_t& ctx) -> const char * {
    if(ctx.header && !eq(ctx.header->id, "_init_")) return "can only be initializer";
    if(ctx.level > 0) return "cannot be imported";
    if(!ctx.line || ctx.line->argc != 1) return "can only have one argument";
    if(ctx.keystore) return "keystore already defined";
    ctx.keystore = true;
    return nullptr;
}

auto checks::chk_here(const script::context_t& ctx) -> const char * {
    if(!ctx.line || !ctx.line->argc) return "here options and/or location required";
    unsigned index = 0;
    const char *cp = nullptr;
    while(nullptr != (cp = ctx.line->argv[index])) {
        if(*cp != '=') break;
        cp = ctx.line->argv[++index];
        if(!cp) return "here option missing";
        ++index;
    }

    if(!ctx.line || ctx.line->argc > index + 1) return "only one location";
    return nullptr;
}

auto checks::chk_speak(const script::context_t& ctx) -> const char * {
    if((ctx.header && eq(ctx.header->id, "_init_")) || !ctx.line) return "cannot be used to initialize";
    unsigned index = 0;
    const char *cp = nullptr;
    while(nullptr != (cp = ctx.line->argv[index])) {
        if(*cp != '=') break;
        cp = ctx.line->argv[++index];
        if(!cp) return "var option missing";
        ++index;
    }

    if(index >= (ctx.line->argc - 1) || !ctx.line->argv[index]) return "at least one phrase required";
    return nullptr;
}

auto checks::chk_debug(const script::context_t& ctx) -> const char * {
    if(!ctx.line || !ctx.line->argc) return "debug level required";
    unsigned index = 0;
    const char *cp = nullptr;
    while(nullptr != (cp = ctx.line->argv[index])) {
        if(*cp != '=') break;
        cp = ctx.line->argv[++index];
        if(!cp) return "var option missing";
        ++index;
    }

    if(!ctx.line || index >= ctx.line->argc - 1 || !ctx.line->argv[index]) return "at least one debug message required";
    auto sv = std::string_view(ctx.line->argv[index]);
    if(sv < "#0" || sv > "#9") return "debug level must be 0-9";
    return nullptr;
}

auto checks::chk_store(const script::context_t& ctx) -> const char * {
    if(!ctx.line || !ctx.line->argc) return "symbols required";
    auto index{0U};
    auto cp{""};
    while(nullptr != (cp = ctx.line->argv[index++])) {
        if(*cp == '%')
            ++cp;
        if(!script::is_variable(cp)) return "vars must be lower case and have single scope";
    }
    return nullptr;
}

auto checks::chk_set(const script::context_t& ctx) -> const char * {
    if(!ctx.line || !ctx.line->argc) return "set symbol required";
    auto cp = ctx.line->argv[0];
    if(*cp == '%')
        ++cp;

    if(!script::is_variable(cp)) return "vars must be lower case and have single scope";
    return nullptr;
}

auto checks::chk_do(const script::context_t& ctx) -> const char * {
    if(ctx.header && eq(ctx.header->id, "_init_")) return "cannot be used to initialize";
    if(ctx.line && !ctx.line->argc) return "at least one label required";
    return nullptr;
}

auto checks::chk_sleep(const script::context_t& ctx) -> const char * {
    if(ctx.header && eq(ctx.header->id, "_init_")) return "cannot be used to initialize";
    if(!ctx.line || ctx.line->argc == 0) return "no duration specified";
    if(ctx.line->argc > 1) return "too many arguments";
    return nullptr;
}

auto checks::chk_info(const script::context_t& ctx) -> const char * {
    if(ctx.header && eq(ctx.header->id, "_init_")) return "cannot be used to initialize";
    if(!ctx.line || !ctx.line->argc) return "prompt file required";
    unsigned index = 0;
    const char *cp = nullptr;
    while(nullptr != (cp = ctx.line->argv[index])) {
        if(*cp != '=') break;
        cp = ctx.line->argv[++index];
        if(!cp) return "copy option missing";
        ++index;
    }

    if(nullptr == ctx.line->argv[index++]) return "prompt file required";
    if(nullptr != ctx.line->argv[index]) return "too many prompt files";
    return nullptr;
}

auto checks::chk_copy(const script::context_t& ctx) -> const char * {
    if(ctx.header && eq(ctx.header->id, "_init_")) return "cannot be used to initialize";
    if(!ctx.line || !ctx.line->argc) return "prompt file required";
    unsigned index = 0;
    const char *cp = nullptr;
    while(nullptr != (cp = ctx.line->argv[index])) {
        if(*cp != '=') break;
        cp = ctx.line->argv[++index];
        if(!cp) return "copy option missing";
        ++index;
    }

    if(nullptr == ctx.line->argv[index++]) return "source prompt file required";
    if(nullptr == ctx.line->argv[index++]) return "target prompt file required";
    if(nullptr != ctx.line->argv[index]) return "too many prompt files";
    return nullptr;
}

auto checks::chk_move(const script::context_t& ctx) -> const char * {
    if(ctx.header && eq(ctx.header->id, "_init_")) return "cannot be used to initialize";
    if(!ctx.line || !ctx.line->argc) return "prompt file required";
    unsigned index = 0;
    const char *cp = nullptr;
    while(nullptr != (cp = ctx.line->argv[index])) {
        if(*cp != '=') break;
        cp = ctx.line->argv[++index];
        if(!cp) return "move option missing";
        ++index;
    }

    if(nullptr == ctx.line->argv[index++]) return "source prompt file required";
    if(nullptr == ctx.line->argv[index++]) return "target prompt file required";
    if(nullptr != ctx.line->argv[index]) return "too many prompt files";
    return nullptr;
}

auto checks::chk_erase(const script::context_t& ctx) -> const char * {
    if(ctx.header && eq(ctx.header->id, "_init_")) return "cannot be used to initialize";
    if(!ctx.line || !ctx.line->argc) return "prompt file required";
    unsigned index = 0;
    const char *cp = nullptr;
    while(nullptr != (cp = ctx.line->argv[index])) {
        if(*cp != '=') break;
        cp = ctx.line->argv[++index];
        if(!cp) return "erase option missing";
        ++index;
    }

    if(nullptr == ctx.line->argv[index++]) return "prompt file required";
    if(nullptr != ctx.line->argv[index]) return "too many prompt files";
    return nullptr;
}

auto checks::chk_record(const script::context_t& ctx) -> const char * {
    if(ctx.header && eq(ctx.header->id, "_init_")) return "cannot be used to initialize";
    if(!ctx.line || !ctx.line->argc) return "prompt file required";
    unsigned index = 0;
    const char *cp = nullptr;
    while(nullptr != (cp = ctx.line->argv[index])) {
        if(*cp != '=') break;
        cp = ctx.line->argv[++index];
        if(!cp) return "record option missing";
        ++index;
    }

    if(nullptr == ctx.line->argv[index++]) return "prompt file required";
    if(nullptr != ctx.line->argv[index]) return "too many prompt files";
    return nullptr;
}

auto checks::chk_append(const script::context_t& ctx) -> const char * {
    if(ctx.header && eq(ctx.header->id, "_init_")) return "cannot be used to initialize";
    if(!ctx.line || !ctx.line->argc) return "prompt file required";
    unsigned index = 0;
    const char *cp = nullptr;
    while(nullptr != (cp = ctx.line->argv[index])) {
        if(*cp != '=') break;
        cp = ctx.line->argv[++index];
        if(!cp) return "append option missing";
        ++index;
    }

    if(nullptr == ctx.line->argv[index++]) return "prompt file required";
    if(nullptr != ctx.line->argv[index]) return "too many prompt files";
    return nullptr;
}

auto checks::chk_play(const script::context_t& ctx) -> const char * {
    if(ctx.header && eq(ctx.header->id, "_init_")) return "cannot be used to initialize";
    if(!ctx.line || !ctx.line->argc) return "prompt file required";
    unsigned index = 0;
    const char *cp = nullptr;
    while(nullptr != (cp = ctx.line->argv[index])) {
        if(*cp != '=') break;
        cp = ctx.line->argv[++index];
        if(!cp) return "play option missing";
        ++index;
    }

    if(nullptr == ctx.line->argv[index++]) return "prompt file required";
    if(nullptr != ctx.line->argv[index]) return "too many prompt files";
    return nullptr;
}

auto checks::chk_tone(const script::context_t& ctx) -> const char * {
    if(ctx.header && eq(ctx.header->id, "_init_")) return "cannot be used to initialize";
    if(!ctx.line || !ctx.line->argc) return "tone required";
    unsigned index = 0;
    const char *cp = nullptr;
    while(nullptr != (cp = ctx.line->argv[index])) {
        if(*cp != '=') break;
        cp = ctx.line->argv[++index];
        if(!cp) return "tone option missing";
        ++index;
    }

    if(nullptr == ctx.line->argv[index++]) return "tone required";
    if(nullptr != ctx.line->argv[index]) return "too many arguments";
    return nullptr;
}

auto checks::chk_event(const script::context_t& ctx) -> const char * {
    if(ctx.header && eq(ctx.header->id, "_init_")) return "cannot be used to initialize";
    if(ctx.line && !ctx.line->argc) return "at least one event required";
    return nullptr;
}
