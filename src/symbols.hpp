/*
 * Copyright (C) 2020 Tycho Softworks.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SYMBOLS_HPP_
#define SYMBOLS_HPP_

#include "server.hpp"

#include <moderncli/memory.hpp>

#include <climits>

namespace server {
class symbols final : private mempager {
public:
    using init_t = std::initializer_list<std::pair<std::string,std::string>>;

    static constexpr unsigned hash_size = TUNE_SYMBOLS_INDEX;
    static constexpr unsigned max_pager = TUNE_SYMBOLS_PAGER;
    static constexpr unsigned max_array = TUNE_SYMBOLS_PAGER / 4;
    static constexpr unsigned short var_size = TUNE_SYMBOL_SIZE;

    symbols();
    ~symbols() final;

    auto exists(const std::string_view& id) const -> bool;
    auto get_value(const std::string_view& id) const -> const char *;              // op-after
    auto from_value(const std::string_view& id, short offset) const -> const char *;    // no-op
    auto get_count(const std::string_view& id) const -> unsigned;
    auto get_size(const std::string_view& id) const-> std::size_t;
    auto set_value(const std::string_view& id, const char *value) -> bool;
    auto set_exists(const std::string_view& id, std::string_view value) -> bool;

    auto get_storage(const std::string_view& id) -> char *;
    auto clear_symbol(const std::string_view& id) -> bool;
    auto make_const(const std::string_view& id, const char *value) -> bool;
    auto make_value(const std::string_view& id, unsigned short size = var_size) -> bool;
    auto make_counter(const std::string_view& id, unsigned short max) -> bool;
    auto make_storage(const std::string_view& id, unsigned short size = var_size, const char *cval = nullptr) -> char *;
    auto make_const(const init_t& list) -> unsigned;
    void purge();

    static auto is_number(const char *cp) -> bool;
    static auto is_number(const std::string& s) -> bool;
    static auto is_path(const char *cp) -> bool;
    static auto is_path(const std::string& s) -> bool;
    static auto is_symbol(const char *cp) -> bool;
    static auto is_symbol(const std::string& s) -> bool;

private:
    struct symbol_t {
        symbol_t *next;
        char *data; // const fixed, sequence source...
        enum { NORMAL, CONST, SEQUENCE, COUNT, ARRAY, FIFO, LIFO } type;
        unsigned short size, count; // size = total or entity, count = entities
        union {
            char buffer[4];
            struct {
                unsigned short head, tail;
            };
            struct {      // sequence or array positioning...
                unsigned short pos;
            };
        };
        char id[1];
    };

    symbol_t *index[hash_size] = {};

    auto find_symbol(const std::string_view& id) const -> symbol_t *;
    auto make_symbol(std::string_view id) -> symbol_t *;
};
} // end namespace
#endif
