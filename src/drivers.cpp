/*
 * Copyright (C) 2020 Tycho Softworks.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "drivers.hpp"
#include "runtime.hpp"

#include <moderncli/templates.hpp>
#include <moderncli/keyfile.hpp>
#include <moderncli/process.hpp>
#include <moderncli/memory.hpp>
#include <moderncli/scan.hpp>

#include <list>

#include <fcntl.h>

#ifdef  SYSTEMD_SERVICE
#include <systemd/sd-daemon.h>
#else
inline auto sd_notify(int, const char *) {  // NOLINT
    return 0;
}
#endif

using namespace server;

namespace  {
Session::event_t shutdown_event{event_type::shutdown};
const char *dtmf_group = "dtmf";

mempool<ipc_session> ipc_pool;
keyfile::keys timers;

unique_sync<std::unordered_map<std::string, script>> scripts;
unique_sync<std::unordered_map<std::string_view, session_t>> sessions;
} // end namespace

ipc_system *Driver::sys_mapped = nullptr;
std::atomic<bool> Driver::online = false;

Plugin *Plugin::plugins_ = nullptr;
Driver *Driver::driver_ = nullptr;
unsigned Driver::ring_timer_ = 4;
unsigned Driver::answer_timer_ = 30;

std::unordered_map<std::string_view, const char *> Driver::groups = {
    {"digits", dtmf_group},
    {"1", dtmf_group},
    {"2", dtmf_group},
    {"3", dtmf_group},
    {"4", dtmf_group},
    {"5", dtmf_group},
    {"6", dtmf_group},
    {"7", dtmf_group},
    {"8", dtmf_group},
    {"9", dtmf_group},
    {"*", dtmf_group},
    {"#", dtmf_group},
};

std::unordered_map<std::string_view, Driver::keyword_t> Driver::keywords = {
    {"here", {
        static_cast<method_t>(&methods::scr_here),
        static_cast<check_t>(&checks::chk_here)}},
    {"debug", {
        static_cast<method_t>(&methods::scr_debug),
        static_cast<check_t>(&checks::chk_debug)}},
    {"pause", {
        static_cast<method_t>(&methods::scr_pause),
        static_cast<check_t>(&checks::chk_keyword)}},
    {"exit", {
        static_cast<method_t>(&methods::scr_exit),
        static_cast<check_t>(&checks::chk_keyword)}},
    {"event", {
        static_cast<method_t>(&methods::scr_event),
        static_cast<check_t>(&checks::chk_event)}},
    {"do", {
        static_cast<method_t>(&methods::scr_do),
        static_cast<check_t>(&checks::chk_do)}},
    {"repeat", {
        static_cast<method_t>(&methods::scr_exit),
        static_cast<check_t>(&checks::chk_conditional)}},
    {"break", {
        static_cast<method_t>(&methods::scr_break),
        static_cast<check_t>(&checks::chk_conditional)}},
    {"end", {
        static_cast<method_t>(&methods::scr_end),
        static_cast<check_t>(&checks::chk_conditional)}},
    {"switch", {
        static_cast<method_t>(&methods::scr_switch),
        static_cast<check_t>(&checks::chk_conditional)}},
    {"case", {
        static_cast<method_t>(&methods::scr_case),
        static_cast<check_t>(&checks::chk_case)}},
    {"otherwise", {
        static_cast<method_t>(&methods::scr_otherwise),
        static_cast<check_t>(&checks::chk_otherwise)}},
    {"var", {
        static_cast<method_t>(&methods::scr_var),
        static_cast<check_t>(&checks::chk_var)}},
    {"set", {
        static_cast<method_t>(&methods::scr_set),
        static_cast<check_t>(&checks::chk_set)}},
    {"tone", {
        static_cast<method_t>(&methods::scr_tone),
        static_cast<check_t>(&checks::chk_tone)}},
    {"play", {
        static_cast<method_t>(&methods::scr_play),
        static_cast<check_t>(&checks::chk_play)}},
    {"erase", {
        static_cast<method_t>(&methods::scr_erase),
        static_cast<check_t>(&checks::chk_erase)}},
    {"record", {
        static_cast<method_t>(&methods::scr_record),
        static_cast<check_t>(&checks::chk_record)}},
    {"append", {
        static_cast<method_t>(&methods::scr_append),
        static_cast<check_t>(&checks::chk_append)}},
    {"info", {
        static_cast<method_t>(&methods::scr_info),
        static_cast<check_t>(&checks::chk_info)}},
    {"copy", {
        static_cast<method_t>(&methods::scr_copy),
        static_cast<check_t>(&checks::chk_copy)}},
    {"move", {
        static_cast<method_t>(&methods::scr_move),
        static_cast<check_t>(&checks::chk_move)}},
    {"const", {
        static_cast<method_t>(&methods::scr_const),
        static_cast<check_t>(&checks::chk_const)}},
    {"sleep", {
        static_cast<method_t>(&methods::scr_sleep),
        static_cast<check_t>(&checks::chk_sleep)}},
    {"speak", {
        static_cast<method_t>(&methods::scr_speak),
        static_cast<check_t>(&checks::chk_speak)}},
    {"until", {
        static_cast<method_t>(&methods::scr_until),
        static_cast<check_t>(&checks::chk_sleep)}},
    {"clear", {
        static_cast<method_t>(&methods::scr_clear),
        static_cast<check_t>(&checks::chk_clear)}},
    {"collect", {
        static_cast<method_t>(&methods::scr_collect),
        static_cast<check_t>(&checks::chk_collect)}},
    {"answer", {
        static_cast<method_t>(&methods::scr_answer),
        static_cast<check_t>(&checks::chk_keyword)}},
    {"input", {
        static_cast<method_t>(&methods::scr_input),
        static_cast<check_t>(&checks::chk_collect)}},
    {"keystore", {
        static_cast<method_t>(&methods::scr_keystore),
        static_cast<check_t>(&checks::chk_keystore)}},
    {"store", {
        static_cast<method_t>(&methods::scr_store),
        static_cast<check_t>(&checks::chk_store)}},
    {"key", {
        static_cast<method_t>(&methods::scr_data),
        static_cast<check_t>(&checks::chk_key)}},
    {"data", {
        static_cast<method_t>(&methods::scr_data),
        static_cast<check_t>(&checks::chk_data)}},
    {"reset", {
        static_cast<method_t>(&methods::scr_reset),
        static_cast<check_t>(&checks::chk_keyword)}},
    {"read", {
        static_cast<method_t>(&methods::scr_read),
        static_cast<check_t>(&checks::chk_var)}},
    {"rand", {
        static_cast<method_t>(&methods::scr_rand),
        static_cast<check_t>(&checks::chk_var)}},
};

Driver::Driver(const std::string& id) noexcept {
    assert(driver_ == nullptr);
    driver_ = this;
    name_ = id;
}

Driver::~Driver() = default;

void Driver::configure(keyfile& config, unsigned verbose) {
    using namespace process;

    if(!limit_) {
        const auto& server = config.at("server");
        limit_ = get_unsigned_or(server.at("limit"), 20U, 1U, 1000U);
    }

    auto size = sizeof(ipc_system) + (limit_ * sizeof(ipc_session));
    auto shm = shm_open(BORDEAUX_IPCPATH, O_RDWR | O_CREAT, 0660);
    if(shm < 0 || ftruncate(shm, static_cast<off_t>(size)) < 0)
        logger.crit(97, "failed to allocate shared memory");
    auto mem = map_t(shm, size, true).detach();
    ::close(shm);
    if(mem == MAP_FAILED)
        logger.crit(97, "failed to map shared memory");

    memset(mem, 0, size);
    sys_mapped = static_cast<ipc_system*>(mem);
    ipc_pool = mempool<ipc_session>(void_ptr(mem, sizeof(ipc_system)), limit_);

    time(&sys_mapped->started);
    sys_mapped->level = verbose;
    sys_mapped->limit = limit_;
    sys_mapped->active = false;

    timers = config.at("timers");
    ring_timer_ = get_unsigned_or(timers.at("ring"), 4U, 4U, 6U);
    answer_timer_ = get_unsigned_or(timers.at("answer"), 20U, 4U, 30U);
}

void Driver::restart(const keyfile& config) {
    scan_directory(config["paths"].at("scripts"), [config](const auto& entry) {
        if(!entry.is_regular_file()) return false;
        const auto filename = entry.path().filename().string();
        if(filename[0] == '.' || filename[0] == '_') return false;
        if(ends_with(filename, SCRIPT_EXT)) {
            guard_ptr scr(scripts);
            scr->insert_or_assign(filename, script(entry.path(), config));
            return true;
        }
        return false;
    });
}

void Driver::shutdown() {
}

void Driver::disconnect() {
    guard_ptr list(sessions);
    for([[maybe_unused]] auto const& [sid, session] : *list) {
        Session::send(session, shutdown_event);
    }
    list->clear();
}

void Driver::drop(Session *session) {
}

void Driver::answer(Session *session) {
}

auto Driver::config_instance(keyfile& cfg, unsigned verbose) -> keyfile {
    assert(driver_ != nullptr);
    driver_->configure(cfg, verbose);
    return cfg;
}

void Driver::start_instance(const keyfile& ini) {
    assert(driver_ != nullptr);
    logger.info("starting {} driver with {} sessions", driver_->name(), driver_->limit_);
    driver_->startup(ini);
}

void Driver::reload_instance(const keyfile& cfg) {
    assert(driver_ != nullptr);
    driver_->restart(cfg);
}

void Driver::stop_instance() {
    assert(driver_ != nullptr);
    logger.info("stopping {} driver", driver_->name());
    driver_->shutdown();
}

void Driver::drop_instance() {
    assert(driver_ != nullptr);
    logger.info("disconnect {} driver", driver_->name());
    driver_->disconnect();
}

void Driver::notify(bool set) {
    online = set;
    if(online)
        sd_notify(0, "READY=1\nSTATUS=online");
    else
        sd_notify(0, "READY=1\nSTATUS=offline");
}

auto Driver::attach(const session_t& s) -> bool {
    logger.debug(3, "sid({}): attaching to driver", s->sid());
    guard_ptr list(sessions);
    if(ipc_pool.empty()) return false;
    list->insert_or_assign(s->sid(), s);
    s->shm = ipc_pool.get_or();
    s->slot_ = s->shm - ipc_pool.begin();
    time(&s->shm->created);
    str_copy(s->shm->sid, 10, s->sid());
    mem_copy(s->shm->status, sizeof(s->shm->status), "init");
    s->shm->state = ipc_session::ISC_UNKNOWN;
    s->shm->type = ipc_session::ISC_ANY;
    s->shm->lines = s->shm->steps = s->shm->audio = s->shm->video = 0;
    ++sys_mapped->used;
    return true;
}

void Driver::release(const std::string_view& sid) {
    logger.debug(3, "sid({}): releasing from driver", sid);
    Session::sync_store();
    guard_ptr list(sessions);
    try {
        auto& session = list->at(sid);
        driver_->drop(session.get());
        if(session->shm) {
            memset(session->shm, 0, sizeof(ipc_session));
            ipc_pool.release(session->shm);
            --sys_mapped->used;
            session->shm = nullptr;
        }
        list->erase(sid);
    }
    catch(...) {
        logger.warn("release: {}: unknown session", sid);
    }
}

auto Driver::control(const std::vector<std::string_view>& list) -> bool {
    if(list.size() < 2) return false;
    try {
        get(list[1]);
    }
    catch(...) {
        return false;
    }
    return true;
}

Plugin::Plugin(const std::string& id) noexcept : name_(std::move(id)), next_(plugins_) {
    plugins_ = this;
}

Plugin::~Plugin() = default;

void Plugin::startup([[maybe_unused]]const keyfile& config) {}
void Plugin::shutdown() {}
void Plugin::restart([[maybe_unused]]const keyfile& config) {}
void Plugin::configure([[maybe_unused]]keyfile& config) {}
auto Plugin::format([[maybe_unused]]Session *s, [[maybe_unused]]const char *keyword) -> const char * {return nullptr;}
auto Plugin::speak([[maybe_unused]]char *out, [[maybe_unused]]std::size_t size, [[maybe_unused]]const char *request, [[maybe_unused]]const char *value) -> bool {return false;}


void Plugin::config_all(keyfile& cfg) {
    auto plugin = Plugin::plugins_;
    while(plugin) {
        plugin->configure(cfg);
        plugin = plugin->next_;
    }
}

void Plugin::start_all(const keyfile& cfg) {
    auto plugin = Plugin::plugins_;
    while(plugin) {
        plugin->startup(cfg);
        plugin = plugin->next_;
    }
}

void Plugin::reload_all(const keyfile& cfg) {
    auto plugin = Plugin::plugins_;
    while(plugin) {
        plugin->restart(cfg);
        plugin = plugin->next_;
    }
}

void Plugin::stop_all() {
    auto plugin = Plugin::plugins_;
    while(plugin) {
        plugin->shutdown();
        plugin = plugin->next_;
    }
}

auto Plugin::find(const std::string_view& from) -> Plugin * {
    auto plugin = Plugin::plugins_;
    while(plugin) {
        if(plugin->name() == from)
            return plugin;
        plugin = plugin->next_;
    }
    return nullptr;
}

auto Plugin::find_locale(std::string_view from) -> Plugin * {
    auto plugin = Plugin::plugins_;
    while(plugin) {
        if(plugin->name() == from)
            return plugin;
        plugin = plugin->next_;
    }

    auto pos = from.find('_');
    if(pos == std::string::npos || pos < 2) return nullptr;
    from.remove_suffix(from.size() - pos);
    plugin = Plugin::plugins_;
    while(plugin) {
        if(plugin->name() == from) return plugin;
        plugin = plugin->next_;
    }
    return nullptr;
}

auto Driver::count() -> std::size_t {
    guard_ptr list(sessions);
    return list->size();
}

auto Driver::get(std::string_view sid) -> session_t& {
    guard_ptr list(sessions);
    return list->at(sid);
}

auto Driver::get_script(const std::string& name) -> script& {
    guard_ptr scr(scripts);
    return scr->at(name);
}

auto Plugin::get_formatted(Session *s, const char *keyword) -> const char * {
    auto plugin = Plugin::plugins_;
    while(plugin) {
        if(plugin->name() == keyword) return plugin->format(s, keyword);
        plugin = plugin->next_;
    }
    return nullptr;
}

