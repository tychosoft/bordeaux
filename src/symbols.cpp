/*
 * Copyright (C) 2020 Tycho Softworks.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "symbols.hpp"

#include <moderncli/strings.hpp>
#include <moderncli/scan.hpp>

using namespace server;

namespace {
auto from_slice(const char *data, short offset) {
    if(!data) return "";
    auto size = static_cast<short>(mem_size(data, TUNE_SYMBOLS_PAGER));
    if(offset < 0 && offset <= -size) return "";
    if(offset > size) return "";
    if(offset >= 0) return data + offset;
    size += offset; // NOLINT
    ++size;
    return data + size;
}
}

symbols::symbols() :
mempager(max_pager) {
    memset(&index[0], 0, sizeof(index));    // NOLINT
}

symbols::~symbols() = default;

void symbols::purge() {
    mempager::clear();
    memset(&index[0], 0, sizeof(index));    // NOLINT
}

auto symbols::find_symbol(const std::string_view& id) const -> symbol_t * {
    const auto path = mem_index(id) % hash_size;
    auto sym = index[path];
    while(sym) {
        if(id == sym->id) break;
        sym = sym->next;
    }
    return sym;
}

auto symbols::make_symbol(std::string_view id) -> symbol_t * {
    auto path = mem_index(id) % hash_size;
    auto id_size =id.size();
    auto sym = static_cast<symbol_t*>(alloc(sizeof(symbol_t) + id_size));
    if(!sym) return nullptr;
    mem_view(&sym->id[0], id_size + 1, id);
    sym->id[id_size] = 0;
    sym->next = index[path];
    index[path] = sym;
    return sym;
}

auto symbols::exists(const std::string_view& id) const -> bool {
    const auto *sym = find_symbol(id);
    return sym != nullptr;
}

auto symbols::get_size(const std::string_view& id) const -> std::size_t {
    const auto *sym = find_symbol(id);
    if(!sym || !sym->data) return 0;
    return sym->size;
}

auto symbols::get_count(const std::string_view& id) const -> unsigned {
    const auto *sym = find_symbol(id);
    if(!sym || !sym->data) return 0;
    if(sym->type == symbol_t::NORMAL || sym->type == symbol_t::CONST) return static_cast<unsigned>(mem_size(sym->data, TUNE_SYMBOLS_PAGER));
    return sym->count;
}

auto symbols::set_exists(const std::string_view& id, std::string_view value) -> bool {
    auto sym = find_symbol(id);
    if(!sym || sym->type != symbol_t::NORMAL) return false;
    auto target = sym->data;
    auto count = value.size();
    auto from = value.data();

    if(count >= sym->size) return false;
    while(count--)
        *(target++) = *(from++);

    *target = 0;
    return true;
}

auto symbols::set_value(const std::string_view& id, const char *value) -> bool {
    auto sym = find_symbol(id);

    // create generic symbol if none found...
    if(!sym) {
        sym = make_symbol(id);
        sym->data = static_cast<char *>(alloc(var_size + std::size_t(1)));
        sym->type = symbol_t::NORMAL;
        sym->size = var_size + 1;
        sym->count = 0;
    }

    switch(sym->type) {
    case symbol_t::COUNT:
        if(get_integer_or(value) < 0) return false;
        if(get_integer_or(value) > sym->size) return false;
        sym->count = get_unsigned_or<unsigned short>(value);
        mem_value(sym->buffer, sizeof(symbol_t::buffer), sym->count);
        return true;
    case symbol_t::CONST:
    case symbol_t::SEQUENCE:
        return false;
    case symbol_t::NORMAL:
        mem_copy(sym->data, sym->size, value);
        return true;
    case symbol_t::LIFO:
    case symbol_t::FIFO:
        if(++sym->tail >= sym->count)
          sym->tail = 0;
        mem_copy(&sym->data[static_cast<std::ptrdiff_t>(sym->tail) * sym->size], sym->size, value);
        return true;
    case symbol_t::ARRAY:
        mem_copy(&sym->data[static_cast<std::ptrdiff_t>(sym->pos) * sym->size], sym->size, value);
        return true;
    }
    return false;
}

auto symbols::clear_symbol(const std::string_view& id) -> bool {
    auto sym = find_symbol(id);
    if(!sym) return false;
    switch(sym->type) {
    case symbol_t::SEQUENCE:
        sym->pos = 0;
        return true;
    case symbol_t::CONST:
        return false;
    case symbol_t::COUNT:
        sym->count = 0;
        sym->buffer[0] = '0';
        sym->buffer[1] = 0;
        return true;
    case symbol_t::NORMAL:
        sym->data[0] = 0;
        return true;
    case symbol_t::LIFO:
    case symbol_t::FIFO:
        sym->head = sym->tail = 0;
        return true;
    case symbol_t::ARRAY:
        for(auto offset = 0; offset < sym->count; ++offset)
            sym->data[static_cast<std::ptrdiff_t>(sym->size) * offset] = 0;
        sym->pos = 0;
        return true;
    }
    return false;
}

auto symbols::get_storage(const std::string_view& id) -> char * {
    const auto *sym = find_symbol(id);
    if(!sym || sym->type != symbol_t::NORMAL) return nullptr;
    return sym->data;
}

auto symbols::get_value(const std::string_view& id) const -> const char * {
    auto sym = find_symbol(id);
    if(!sym) return "";
    const char *result{""};
    switch(sym->type) {
    case symbol_t::NORMAL:
    case symbol_t::CONST:
        return sym->data;
    case symbol_t::COUNT:
        if(sym->count < sym->size)
            mem_value(sym->buffer, sizeof(symbol_t::buffer), ++sym->count);
        return sym->buffer;
    case symbol_t::SEQUENCE:
        result = &sym->data[sym->pos];
        sym->pos += mem_size(result, TUNE_SYMBOLS_PAGER) + 1;
        if(sym->pos >= sym->size)
            sym->pos = 0;
        return result;
    case symbol_t::LIFO:
        if(sym->head == sym->tail) return "";
        result = &sym->data[static_cast<std::ptrdiff_t>(sym->size) * sym->tail];
        if(sym->tail == 0)
            sym->tail = sym->count;
        --sym->tail;
        return result;
    case symbol_t::FIFO:
        if(sym->head == sym->tail) return "";
        result = &sym->data[static_cast<std::ptrdiff_t>(sym->size) * sym->head];
        if(++sym->head >= sym->count)
            sym->head = 0;
        return result;
    case symbol_t::ARRAY:
        result = &sym->data[static_cast<std::ptrdiff_t>(sym->size) * sym->pos];
        if(++sym->pos >= sym->count)
            sym->pos = 0;
        return result;
    }
    return "";
}

auto symbols::from_value(const std::string_view& id, short offset) const -> const char * {
    const auto sym = find_symbol(id);
    if(!sym) return "";
    const char *result{""};
    short pos{};

    switch(sym->type) {
    case symbol_t::NORMAL:
    case symbol_t::CONST:
        return from_slice(sym->data, offset);
    case symbol_t::COUNT:
        return from_slice(sym->buffer, offset);
    case symbol_t::SEQUENCE:
        pos = static_cast<short>(sym->count);
        if(offset > pos) return "";
        if(offset <= -pos) return "";
        if(offset < 0)
            offset += pos; // NOLINT
        result = sym->data;
        while(offset--)
            result += mem_size(sym->data, TUNE_SYMBOLS_PAGER) + 1;
        return result;
    case symbol_t::LIFO:
    case symbol_t::FIFO:
        if(sym->head == sym->tail) return "";
        if(offset < 0) {
            pos = static_cast<short>(sym->tail);
            while(++offset < 0) {
                if(--pos < 0)
                    pos = static_cast<short>(sym->count - 1);
                if(pos == sym->tail) return "";
            }
        }
        else {
            pos = static_cast<short>(sym->head);
            while(offset--) {
                if(++pos >= sym->count)
                    pos = 0;
                if(pos == sym->head) return "";
            }
        }
        return &sym->data[static_cast<std::ptrdiff_t>(sym->size) * pos];
    case symbol_t::ARRAY:
        pos = static_cast<short>(sym->count);
        if(offset >= pos) return "";
        if(offset <= -pos) return "";
        if(offset > 0)
            pos = offset;
        else
            pos += offset; // NOLINT
        return &sym->data[static_cast<std::ptrdiff_t>(sym->size) * pos];
    }
    return "";
}

auto symbols::make_const(const std::string_view& id, const char *value) -> bool {
    auto sym = find_symbol(id);
    if(sym) {
        if(sym->type != symbol_t::CONST) return false;
        return strcmp(value, sym->data) == 0;
    }

    sym = make_symbol(id);
    sym->data = mempager::dup(value);
    sym->type = symbol_t::CONST;
    sym->size = static_cast<unsigned short>(mem_size(value, TUNE_SYMBOLS_PAGER) + 1);
    sym->count = 0;
    return true;
}

auto symbols::make_counter(const std::string_view& id, unsigned short max) -> bool {
    auto sym = find_symbol(id);
    if(sym) return sym->type == symbol_t::COUNT;
    sym = make_symbol(id);
    sym->data = nullptr;
    sym->type = symbol_t::COUNT;
    sym->size = max;
    sym->count = 0;
    sym->buffer[0] = 0;
    return true;
}

auto symbols::make_value(const std::string_view& id, unsigned short size) -> bool {
    auto sym = find_symbol(id);
    if(sym) return sym->size == size && sym->type == symbol_t::NORMAL;
    sym = make_symbol(id);
    sym->data = static_cast<char *>(alloc(size));
    sym->type = symbol_t::NORMAL;
    sym->size = size;
    sym->count = 0;
    sym->data[0] = 0;
    return true;
}

auto symbols::make_storage(const std::string_view& id, unsigned short size, const char *cval) -> char * {
    auto sym = find_symbol(id);
    if(sym) return nullptr;
    sym  = make_symbol(id);
    sym->data = static_cast<char *>(alloc(size));
    sym->size = size;
    sym->count = 0;
    sym->data[0] = 0;

    if(cval) {
        sym->type = symbol_t::CONST;
        auto target = sym->data;
        auto count = str_size(cval, size);
        auto from = cval;

        if(count >= sym->size)
            count = sym->size - std::size_t(1);

        while(count--)
            *(target++) = *(from++);

        *target = 0;
    }
    else
        sym->type = symbol_t::NORMAL;

    return sym->data;
}

auto symbols::make_const(const init_t& list) -> unsigned {
    unsigned count = 0;
    for(auto const& [key, value] : list) {
        if(make_const(key, value.c_str()))
            ++count;
    }
    return count;
}

auto symbols::is_path(const char *cp) -> bool {
    if(!cp || !*cp || !strchr(cp, '/')) return false;
    if(!isalpha(*cp)) return false;
    while(*cp) {
        if(!isalnum(*cp) && *cp != '/')
            return false;
        ++cp;
    }
    return true;
}

auto symbols::is_symbol(const char *cp) -> bool {
    bool scope = false;

    if(!cp) return false;
    if(!isalpha(*cp)) return false;
    while(*cp) {
        if(*cp == ':' && scope) return false;
        if(*cp == ':') {
            scope = true;
            ++cp;
            continue;
        }
        if(!isalnum(*cp)) return false;
        ++cp;
    }
    return true;
}

auto symbols::is_path(const std::string& str) -> bool {
    return is_path(str.c_str());
}

auto symbols::is_symbol(const std::string& str) -> bool {
    return is_symbol(str.c_str());
}

auto symbols::is_number(const char *cp) -> bool {
    bool dot = false;

    if(!cp) return false;
    if(*cp == '-')
        ++cp;

    if(!*cp) return false;
    while(*cp) {
        switch(*(cp++)) {
        case '.':
            if(dot) return false;
            dot = true;
            break;
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            break;
        default:
            return false;
        }
    }
    return true;
}

auto symbols::is_number(const std::string& s) -> bool {
    return is_number(s.c_str());
}

