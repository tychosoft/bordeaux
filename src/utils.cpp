/*
 * Copyright (C) 2020 Tycho Softworks.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "drivers.hpp"
#include "utils.hpp"

#include <moderncli/strings.hpp>
#include <moderncli/socket.hpp>
#include <moderncli/scan.hpp>

using namespace server;

auto util::uri_schema(const std::string& id) -> std::string {
    if(begins_with(id, "sip:")) return "sip:";
    if(begins_with(id, "sips:")) return "sips:";
    if(begins_with(id, "h323:")) return "h323:";
    return {};
}

auto util::uri_userid(const std::string& id) -> std::string {
    auto user(id);
    if(begins_with(id, "sip:"))
        user = id.substr(4);
    else if(begins_with(id, "sips:") || begins_with(id, "h323:"))
        user = id.substr(5);

    auto pos = user.find_last_of('@');
    if(pos != std::string::npos) return user.substr(0, pos);
    return user;
}

auto util::uri_server(const std::string& id) -> std::string {
    auto server(id);
    if(begins_with(id, "sip:"))
        server = id.substr(4);
    else if(begins_with(id, "sips:") || begins_with(id, "h323:"))
        server = id.substr(5);

    auto pos = server.find_last_of('@');
    if(pos != std::string::npos) return server.substr(pos + 1);
    return server;
}

auto util::uri_host(const std::string& id) -> std::string {
    auto server(id);
    auto pos = id.find_last_of('@');
    if(pos != std::string::npos)
        server = id.substr(pos + 1);
    else {
        if(begins_with(id, "sip:"))
            server = id.substr(4);
        else if(begins_with(id, "sips:") || begins_with(id, "h323:"))
            server = id.substr(5);
    }

    if(begins_with(id, "h323:")) return server;
    auto inet6 = server.find_last_of(']');
    pos = server.find_last_of(':');

    if((pos != std::string::npos) && (inet6 == std::string::npos || inet6 < pos)) return server.substr(0, pos);
    return server;
}

auto util::uri_address(const std::string& id) -> std::string {
    auto host = uri_host(id);
    auto addr = inet_find(host, "", ipv6 ? AF_INET6 : AF_INET);

    if(host.empty() || addr.empty()) return host;
    return addr.host();
}

auto util::uri_port(const std::string& id) -> uint16_t {
    auto server(id);
    auto port = uint16_t(5060);

    if(begins_with(id, "sip:"))
        server = id.substr(4);
    else if(begins_with(id, "h323:")) return 1720;
    else if(begins_with(id, "sips:")) {
        server = id.substr(5);
        ++port;
    }

    auto inet6 = server.find_last_of(']');
    auto pos = server.find_last_of(':');

    if(pos == std::string::npos) return port;
    if(inet6 != std::string::npos && inet6 > pos) return port;
    return get_unsigned_or<uint16_t>(server.substr(pos + 1), port, 1);
}

auto util::uri_setuser(const std::string& uri, const std::string& user) -> std::string {
    auto schema = uri_schema(uri);
    auto server = uri_server(uri);

    auto inet6 = server.find_last_of(']');
    auto pos = server.find_last_of(':');

    if(schema == "h323:")   // we never use ports in h323
        inet6 = pos;

    if((pos != std::string::npos) && (inet6 == std::string::npos || inet6 < pos)) {
        auto port = get_unsigned<uint16_t>(server.substr(pos + 1), 5060, 1);
        return make_uri(schema, user, server.substr(0, pos), port);               }
    return make_uri(schema, user, server);
}

auto util::make_uri(const std::string& schema, const std::string& host, uint16_t port) -> std::string {
    if(schema == "h323:" || !port) return schema + host;
    return schema + host + ":" + std::to_string(port);
}

auto util::make_uri(const std::string& schema, const std::string& user, const std::string& host, uint16_t port) -> std::string {
    if(schema == "h323:" || !port) return schema + user + "@" + host;
    return schema + user + "@" + host + ":" + std::to_string(port);
}

