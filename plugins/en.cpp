/*
 * Copyright (C) 2022 Tycho Softworks.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "drivers.hpp"

#include <moderncli/strings.hpp>
#include <moderncli/memory.hpp>

using namespace server;
using namespace std::literals::chrono_literals;

namespace {
const char *order[] = { "th",
    "1st", "2nd", "3rd", "4th", "5th",
    "6th", "7th", "8th", "9th", "10th",
    "11th", "12th", "13th", "14th", "15th",
    "16th", "17th", "18th", "19th",
    // extra entries for days of month...
    "20,th", "20,1st", "20,2nd", "20,3rd", "20,4th",
    "20,5th", "20,6th", "20,7th", "20,8th", "20,9th",
    "30,th", "30,1st"};

[[maybe_unused]] const char *weekdays[] = {"sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"};

auto low_order(char *out, std::size_t size, int num) {
    char buf[10]{0};
    *out = 0;

    if(num >= 100) {
        buf[0] = static_cast<char>('0' + (num / 100));
        buf[1] = 0;
        if(!str_append(out, size, buf, ",hundred,", nullptr)) return false;
        num %= 100;
        if(!num) return str_append(out, size, "th,", nullptr);
    }

    if(num < 20) return str_append(out, size, order[num], ",", nullptr);
    buf[0] = static_cast<char>('0' + num / 10);
    buf[1] = '0';
    buf[2] = ',';
    buf[3] = 0;
    return str_append(out, size, buf, order[num % 10], ",", nullptr);
}

auto low_number(char *out, std::size_t size, int num) {
    char buf[6]{0};
    if(num >= 100) {
        buf[0] = static_cast<char>('0' + (num / 100));
        buf[1] = 0;
        if(!str_append(out, size, buf, ",hundred,", nullptr)) return false;
        num %= 100;
        if(!num) return true;
    }

    if(num < 10) {
        buf[0] = static_cast<char>('0' + num);
        buf[1] = ',';
        buf[2] = 0;
    }
    else if(num < 20) {
        buf[0] = '1';
        buf[1] = static_cast<char>('0' + num / 10);
        buf[2] = ',';
        buf[3] = 0;
    } else {
        buf[0] = static_cast<char>('0' + num / 10);
        buf[1] = '0';
        buf[2] = ',';
        buf[3] = 0;
        if(num % 10) {
            buf[3] = static_cast<char>('0' + (num % 10));
            buf[4] = ',';
            buf[5] = 0;
        }
    }
    return str_append(out, size, buf, nullptr);
}

auto speak_spell(char *out, std::size_t size, const char *v) {
    if(!v || strchr(v, ',') || mem_size(v) > (size / 2 - 2)) return false;
    while(*v) {
        auto ch = tolower(*(v++));
        if(!isdigit(ch) && (ch < 'a' || ch > 'z')) continue;
        *(out++) = static_cast<char>(ch);
        *(out++) = ',';
    }
    *out = 0;
    return true;
}

auto speak_order(char *out, std::size_t size, const char *v) {
    if(!v || !*v || strchr(v, ',') || strchr(v, '.')) return false;
    auto num = atol(v); // NOLINT
    if(num < 0 || num > 999) return false;
    return low_order(out, size, static_cast<int>(num));
}

auto speak_number(char *out, std::size_t size, const char *v) {
    if(!v || !*v || strchr(v, ',')) return false;
    *out = 0;
    auto zero = false;
    if(*v == '-') {
        str_append(out, size, "negative,", nullptr);
        ++v;
    }

    auto num = atol(v); // NOLINT
    if(num < 0) return false;
    if(num > 999999999) {
        zero = false;
        if(!low_number(out, size, static_cast<int>(num / 1000000000)) || !str_append(out, size, "billion,", nullptr)) return false;
        num %= 1000000000;
    }
    if(num > 999999) {
        zero = false;
        if(!low_number(out, size, static_cast<int>(num / 1000000)) || !str_append(out, size, "million,", nullptr)) return false;
        num %= 1000000;
    }
    if(num > 999) {
        zero = false;
        if(!low_number(out, size, static_cast<int>(num / 1000)) || !str_append(out, size, "thousand,", nullptr)) return false;

        num %= 1000;
    }

    if(num || zero)
        if(!low_number(out, size, static_cast<int>(num))) return false;

    v = strchr(v, '.');
    if(!v || !v[1]) return true;
    ++v;
    if(!str_append(out, size, "point,", nullptr)) return false;
    auto len = mem_size(out);
    return speak_spell(out + len, size - len, v);
}
} // end anon namespace

namespace server {
class en_plugin final : private Plugin {
public:
    en_plugin() : Plugin("en") {}

    auto speak(char *out, std::size_t size, const char *request, const char *value) -> bool final {
        if(eq(request, "&spell")) return speak_spell(out, size, value);
        if(eq(request, "&number") || eq(request, "&decimal") || eq(request, "&count")) return speak_number(out, size, value);
        if(eq(request, "&order")) return speak_order(out, size, value);
        return false;
    }

    static en_plugin instance;
};
} // end namespace

NO_EXPORT en_plugin en_plugin::instance;
