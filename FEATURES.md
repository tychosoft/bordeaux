# Features

Bordeaux provides ip telephony services automation and integration as an
extendable media processing application server who's underlying operations are
controlled by a script driven state-event engine. Bordeaux operates as a posix
service while a typical deployment also includes a runtime driver, a set of
media resources, and a collection of Bordeaux application scripts. Typically,
Bordeaux will be used in conjunction with a local ip telephony server, or to
provide intelligent agents to cloud hosted services. This offers a high level
feature overview for Bordeaux.

## 1. Runtime Drivers

Bordeaux operates in conjunction with a runtime loadable driver which defines
specific behaviors and integration protocols. Runtime drivers can provide
different protocol stacks and can also extend the Bordeaux scripting language,
such as adding database support for Bordeaux scripting. Runtime drivers can
also be built as separate stand-alone projects, such as Bordeaux Call Manager
that will provide h323 support for use with gnu gatekeeper, as well as
eventually offering a pure rtp/webrtc drivers with http or websocket support
for use with cloud and carrier scale voice application services, and a calypso
driver for secure application services.

The core project comes bundled with a generic driver and a driver for
integrating with coventry using eXosip. The generic driver offers consistent
application services to interoperate with many existing SIP based IP-PBX
systems such as Vodia, Asterisk/FreePBX, 3CX, and Freeswitch Fusion, as well as
RTP media support and SIP messaging. The generic runtime driver may also
include MQTT support for facility integration. This feature description will
mostly cover the generic driver.

Bordeaux is a server that exports its own symbol space to runtime drivers. The
server's headers can be exported to independently compile and link a new driver
plugin in another package. This is used in Bordeaux to create a cloud call
center, and for building gnu gatekeeper support. This also allows special
plugin modules and scripting language extensions to be runtime loadable.

## 2. Scripted Automation

Bordeaux scripting, enhanced by the runtime driver, provides an abstract and
consistent way to define application call flow regardless of the deployment
environment or driver being used. There is done with ccscript, which was used
in Bayonne, and which may have extensions thru module plugins.

As a declarative language, ccscript represents call flows and telephony events.
The current script step being executed also represents the executing state of
the session driver.  To support scalable parallel execution ccscript operates
by asynchronous dispatch with a thread lock that manipulates a session state
machine. This also means asynchronous events can be collected and executed from
other threads efficiently.

To improve performance further ccscript are compiled into an internal
representation that is stored in memory and can also be reloaded on demand.
Script symbols are managed in pre-allocated spaces to reduce heap activity.
Currently active sessions reference an existing memory image of the script, and
new calls can get the latest version of a script. This also allows for live
updating of running services.

## 3. Database Integration

Bordeaux may need to do simple and trivial database operations for call flow
operations. However, Bordeaux itself has no built-in database support. Instead,
these operations might be performed through plugins or by json rpc requests to
a supporting web application or MQTT service which can then map database
queries into script engine consumable results. This keeps the Bordeaux server
simple and makes what database integration is created entirely generic.

Some drivers may also integrate database services as they connect with servers
that use well defined back-end schemas. The sipcraft driver, for example, may
connect to the common sipcraft postgres back-end we use. A freepbx driver may
connected to mysql in the future. There had been consideration to having a
generic odbc database driver interface and generic scripting support instead.
This may happen if there is sufficient need, but Bordeaux scripting is really
not designed for large generic database processing.

## 4. Continues Operation and Controls

Bordeaux operates as a continually up server. This means that if calls are
active on currently running scripts, and the server is reloaded, any active
scripts continue running until the last call session using them ends, and then
the old scripts are purged. Server reload is achieved with a sighup.

There are other config properties that may be updated from the config file on a
server reload. Some may not, such as current network binding, and hence can
only be changed by a full server restart.

In addition to sighup and live reload, Bordeaux supports ipc services. Basic
controls are offered thru a unix domain udp control socket and each active
session has an associated shared memory block. With some servers it may be
possible to exchange control events and alarms over SIP subscriptions.

## 5. Message Automation

Messaging includes in-dialog "session" oriented messaging flows managed by
script automation, and out of dialog "generated" messages. The latter are
called notify messages and may also be used to solicit and send system
notifications to user devices, typically as out of dialog "instant" messages or
sms.

Out of band messaging events can also be used to trigger Bordeaux script
sessions and call processing, and support activations from key events or
display menus, along with IPC integrations. This enables support for Bordeaux
scripted chat and call agent automation whether for telephony endpoints or web
sites.

## 6. Media Processing

Audio processing is primarily in basic u-law telephony audio. U-law is used to
map phrases into memory and to assemble audio for rapid generation of audio
streams. The system to do this is defined in the ccscript call flow language
and a system first introduced in Bayonne called phrasebook. Phrasebook
assembles phrases from multi-lingual specification rules, and is aware of how
to present things like numbers, currencies, time and date, etc. Phrasebook uses
plugins to process rules for each language. Text-To-Speech will also be
available, to produce phrasebook source material.

Bordeaux will include an integrated RTP stack that operates in its own thread
per call session. This may also offer rtcp and will be compatible with webrtc
operations, turn proxies, etc, depending on the driver. This stack will be
optimized around the media types Bordeaux supports, and will allocate all
buffering at startup to avoid heap usage.

Audio will generally be sampled and recorded as u-law for convenience. It may
also be fed into a speech recognition engine, much like we experimented with
early in Bayonne. Recognized words and phrases can then be described as events
in ccscript and associated with specific call flow states.

Video sessions may also be possible to generate in Bordeaux and assembled from
frame segments. These can be synchronized with phrasebook and tts generated
audio to give a true artificial agent experience under application control.

## 7. Facility Integration

Facility integration includes control of lighting, hvac, security, and premise
sensors. All of these can and potentially do relate to telephony and
automation. From the perspective of Bordeaux, we can make telephony instruments
user interfaces to manage and control facility services. This includes the
ability to do script-driven management of a facility, as well as the ability to
do so interactively.

Facility management is somewhat tied to drivers. This is because in Coventry we
are supporting facility automation over sip, while often MQTT and HTTP are also
used. Bordeaux may also include some automation utilities based on use of other
integration services, such as MQTT, for integrating different kinds of IoT
device infrastructure.

## 8. Generic Utilities

Bordeaux will include a number of supporting utilities. These are simple tools
to integrate sip, zmq, web, or h323 infrastructure better, when paired with
drivers and external servers. Some of these utilities may target features
specific to our other servers, and some may prove useful generic and
stand-alone.

Some tools to include with Bordeaux will be for preparation of media files.
This would be similar to things like Bayonne's original audiotool and tonetool
utilities. A new video tool will help produce aligned video frames for proper
assembly of glitch-free video agent sessions. Other bordeaux specific tools may
talk to the Bordeaux embedded web interface.

Other utilities may offer further interconnect. I may introduce sip and mqtt to
X10 power control endpoints daemon in Bordeaux. A headless sip camera server
for sip and scan-to-sip fax utility may also be introduced in Bordeaux.
